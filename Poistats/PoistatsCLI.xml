<?xml version="1.0" encoding="utf-8"?>

<executable>
	<category>
		Diffusion Tensor
	</category>
	
	<title>
		Path of interest
	</title> 
	
	<description>
		Finds most probable path between two regions of interest.
	</description>	
	
	<version>
		0.1 Beta
	</version>
	
	<contributor>
		Dennis Jen
	</contributor>
	
	<acknowledgements>
	    Acknowledgements for funding agency, employer, colleague, (e.g. This work is part of the National Alliance for Medical Image Computing NAMIC), funded by the National Institutes of Health through the NIH Roadmap for Medical Research, Grant U54 EB005149). for CLP, appears as part of --help for GUI, may show up in the Help of About section. 
	</acknowledgements>
	
	<parameters advanced="false">

		<label>
			Required images
		</label> 
		
		<description>
			Input images for poistats.
		</description>
		
		<file>
			
			<name>
				diffusionTensorImage
			</name>
			
			<description>
				Diffusion tensor image input.
			</description>
			
			<label>
				Diffusion Tensor Image
			</label>
			
			<flag>
				i
			</flag>
			
			<channel>
				input
			</channel>
			
		</file>
		
		<boolean>
		  
		  <name>
		    isSymmetricTensorData
		  </name>
		  
		  <description>
		    Is the data stored as a symmetric matrix (6 components)?
		  </description>
		  
		  <label>
		    Symmetric Data Storage
		  </label>
		  
		  <longflag>
		    symmetric
		  </longflag>
		  
		  <default>
		    false
		  </default>
		  
		</boolean>
		
		<boolean>
		  
		  <name>
		    isOutputNii
		  </name>
		  
		  <description>
		    Should the output be in nifti?  This overrides the default.
		  </description>
		  
		  <label>
		    Nifit Output
		  </label>
		  
		  <longflag>
		    nii
		  </longflag>
		  
		  <default>
		    false
		  </default>
		  
		</boolean>		
		
		<image>
			
			<name>
				seedRegions
			</name>
			
			<description>
				Volume containing numerical labels to use as seed regions.
			</description>
			
			<label>
				Seed Regions Image
			</label>
			
			<longflag>
				seeds
			</longflag>

			<channel>
				input
			</channel>
			
		</image>

		<image>
			
			<name>
				sample
			</name>
			
			<description>
				Instem for volume to sample. For example: fa, trace.
			</description>
			
			<label>
				Sample Volume
			</label>
			
			<longflag>
				sample
			</longflag>

			<channel>
				input
			</channel>
			
		</image>
		
	</parameters>
	
	<parameters advanced="false">
	
		<label>
			Required Values
		</label> 
		
		<description>
			Input value parameters for poistats.
		</description>
		
		<directory>

			<name>
				outputDirectory
			</name>
			
			<description>
				Output Directory
			</description>
			
			<label>
				Output Directory
			</label>
			
			<longflag>
				out
			</longflag>

		</directory>
		
		<integer>
		
			<name>
				sigma
			</name>
			
			<description>
				Search distance for path control points in units voxels. The search distance should be approximately the distance between the midpoint of the seed path and the target location.
			</description>
			
			<label>
				Sigma
			</label>
			
			<default>
				10
			</default>
			
			<longflag>
				sigma
			</longflag>
			
			<constraints>
			
				<minimum>
					1
				</minimum>
			
			</constraints>
		
		</integer>
		
		<integer>
		
			<name>
				numberOfControlPoints
			</name>
			
			<description>
				Number of control points used to describe path. Number should be approximately the number of 'turns' in the path.
			</description>
			
			<label>
				Number of control points
			</label>
			
			<default>
				2
			</default>
			
			<longflag>
				nc
			</longflag>
			
			<constraints>
			
				<minimum>
					1
				</minimum>
			
			</constraints>
		
		</integer>

	</parameters>
	
	<parameters advanced="true">
				
		<label>
			Optimal parameters
		</label>

		<description>
			Optional parameters
		</description>
					
		<image>
		
			<name>
				mask
			</name>
			
			<description>
				Instem for mask. The path will not be allowed to contact the mask. The mask can be used to specify invalid regions, e.g., CSF.
			</description>
			
			<label>
				Mask
			</label>
			
			<longflag>
				mask
			</longflag>
			
			<channel>
				input
			</channel>			

		</image>
		
		<integer-vector>
		
			<name>
				seedRegionsToUse
			</name>
		
			<description>
				 Define seed region to use as defined by numerical labels.
			</description>
			
			<label>
				Seed regions to use
			</label>
			
			<longflag>
				seednums
			</longflag>
		
		</integer-vector>

		<integer>
		
			<name>
				numberOfReplicas
			</name>
			
			<description>
				Specifies the number of replicas.
			</description>
			
			<label>
				Number of replicas
			</label>
			
			<default>
				100
			</default>
			
			<longflag>
				nreplicas
			</longflag>
			
			<constraints>

				<minimum>
					1
				</minimum>
			
			</constraints>
		
		</integer>

		<integer>
		
			<name>
				numberOfSamplePoints
			</name>
			
			<description>
				Number of points to sample along path from sample volume.
			</description>
			
			<label>
				Number of sample points
			</label>
			
			<default>
				100
			</default>
			
			<longflag>
				ns
			</longflag>
			
			<constraints>

				<minimum>
					1
				</minimum>
			
			</constraints>
		
		</integer>
		
		<double>

			<name>
				exchangeProbability
			</name>
			
			<description>
				Replica exchange probability.
			</description>
			
			<label>
				Exchange probability
			</label>

			<default>
				0.05
			</default>
			
			<longflag>
				exchangeprob
			</longflag>
			
		</double>		
		
		<double>
		
			<name>
				timeConstant
			</name>
			
			<description>
				Sigma time constant.
			</description>
			
			<label>
				Sigma time constant
			</label>

			<default>
				200
			</default>
			
			<longflag>
				timeconst
			</longflag>
			
		</double>		

		<double>
		
			<name>
				gamma
			</name>
			
			<description>
				Points to image gamma.
			</description>
			
			<label>
				Points to image gamma
			</label>

			<default>
				0.5
			</default>
			
			<longflag>
				gamma
			</longflag>
			
		</double>		
	
	</parameters>

</executable>
