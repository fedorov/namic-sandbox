/*=========================================================================

  Program:   Simple Robot Simulator / Server Thread class
  Module:    $RCSfile: $
  Language:  C++
  Date:      $Date: $
  Version:   $Revision: $

  Copyright (c) Brigham and Women's Hospital. All rights reserved.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef __Workphase_h
#define __Workphase_h


// Workphases
#define WP_STARTUP     0
#define WP_PLANNING    1
#define WP_CALIBRATION 2
#define WP_TARGETING   3
#define WP_MANUAL      4
#define WP_EMERGENCY   5



#endif // __Workphase_h



