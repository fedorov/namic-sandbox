// Version 1.0: Jul 17, 2007 12:08PM

#include "BRPtprStatus.h"


char *BRPtprStatusMessage[BRPtprStatus_Size] =
{ "Invalid",
  "Initializing",
  "Ready",
  "Moving",
  "Manual",
  "Error"
};
