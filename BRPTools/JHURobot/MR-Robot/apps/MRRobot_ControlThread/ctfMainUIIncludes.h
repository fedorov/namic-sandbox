/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-    */
/* ex: set filetype=cpp softtabstop=4 shiftwidth=4 tabstop=4 cindent expandtab: */

//
// $Id: ctfMainUIIncludes.h,v 1.3 2005/09/26 15:41:46 anton Exp $
//

#ifndef _ctfMainUIIncludes_h
#define _ctfMainUIIncludes_h


// system includes
#include <stdio.h>
#include <iostream>

// cisst includes
#include <cisstCommon.h>
#include <cisstMultiTask.h>
#include <cisstDevices.h>


#endif // _ctfMainUIIncludes_h
