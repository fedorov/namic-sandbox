This directory contains an experimental version of an Open Source 
library for supporting the execution of Workflow-Based applications.

This version is intended to be a "build-to-throw" version, that will
be superseded by a redesigned version that can be used from ITK, VTK
and particularly from IGSTK.
