<?xml version="1.0" encoding="utf-8"?>
<executable>
  <category>
    Segmentation
  </category>
  <title>
    Localized Region-Based Segmentation
  </title>
  <description>
    Active contour segmentation using robust statistic as features.
  </description>
  <version>1.0</version>
  <documentation-url>
    http://wiki.na-mic.org/Wiki/index.php
  </documentation-url>
  <license></license>
  <contributor>
    Yi Gao, Allen Tannenbaum
  </contributor>
  <acknowledgements>
    This work is part of the National Alliance for Medical Image Computing (NAMIC), funded by the National Institutes of Health
  </acknowledgements>

  <parameters>
    <label>Segmentation Parameters</label>
    <description>Parameters for robust statistics segmentation</description>

    <double>
      <name>expectedVolume</name>
      <longflag>expectedVolume</longflag>
      <flag>v</flag>
      <description>The approximate volume of the object, in mL.</description>
      <label>Approximate volume(mL)</label>
      <default>1</default>
      <constraints>
        <minimum>0.0</minimum>
        <maximum>10000</maximum>
        <step>10</step>
      </constraints>
    </double>

    <point coordinateSystem="ras" multiple="true">
      <name>seed</name>
      <label>Seeds</label>
      <longflag>--seed</longflag>
      <description>Seed point(s) for initial position</description>
      <default>100,100,10</default>
    </point>

<!--     <integer> -->
<!--       <name>cx</name> -->
<!--       <longflag>centerX</longflag> -->
<!--       <flag>x</flag> -->
<!--       <description>x-coord of a point in object</description> -->
<!--       <label>center X</label> -->
<!--       <default>100</default> -->
<!--     </integer> -->

<!--     <integer> -->
<!--       <name>cy</name> -->
<!--       <longflag>centerY</longflag> -->
<!--       <flag>y</flag> -->
<!--       <description>y-coord of a point in object</description> -->
<!--       <label>center Y</label> -->
<!--       <default>100</default> -->
<!--     </integer> -->

<!--     <integer> -->
<!--       <name>cz</name> -->
<!--       <longflag>centerZ</longflag> -->
<!--       <flag>z</flag> -->
<!--       <description>z-coord of a point in object</description> -->
<!--       <label>center Z</label> -->
<!--       <default>10</default> -->
<!--     </integer> -->

<!--       <boolean> -->
<!--         <name>globalSwitch</name> -->
<!--         <longflag>global</longflag> -->
<!--         <flag>g</flag> -->
<!--         <description>true: Use local statistics, false: Use global statistics.</description> -->
<!--         <label>Use local statistics?</label> -->
<!--         <default>false</default> -->
<!--       </boolean> -->

<!--     <integer> -->
<!--       <name>globalSwitch</name> -->
<!--       <longflag>globalSwitch</longflag> -->
<!--       <flag>g</flag> -->
<!--       <description>1: Use global statistics, 0: Use local statistics.</description> -->
<!--       <label>global or local?</label> -->
<!--       <default>1</default> -->
<!--       <constraints> -->
<!--         <minimum>0</minimum> -->
<!--         <maximum>1</maximum> -->
<!--         <step>1</step> -->
<!--       </constraints> -->
<!--     </integer> -->

  </parameters>

  <parameters>
    <label>Auxiliary Parameters</label>
    <description>Some auxillary parameters to control the stop criteria.</description>

    <double>
      <name>intensityHomogeneity</name>
      <longflag>intensityHomogeneity</longflag>
      <description>What is the homogeneity of intensity within the
      object? Given constant intensity at 1.0 score and extreme
      fluctuating intensity at 0.</description>
      <label>Intensity Homogeneity[0-1.0]</label>
      <default>0.0</default>
      <constraints>
        <minimum>0.0</minimum>
        <maximum>1.0</maximum>
        <step>0.1</step>
      </constraints>
    </double>

    <double>
      <name>curvatureWeight</name>
      <longflag>curvatureWeight</longflag>
      <flag>c</flag>
      <description>Given sphere 1.0 score and extreme rough
      bounday/surface 0 score,
      what is the expected smoothness of the object?</description>
      <label>Boundary Smoothness[0-1]</label>
      <default>0</default>
      <constraints>
        <minimum>0.0</minimum>
        <maximum>1.0</maximum>
        <step>0.1</step>
      </constraints>
    </double>

    <integer>
      <name>labelValue</name>
      <longflag>labelValue</longflag>
      <description>Label value of the output image</description>
      <label>Output Label Value</label>
      <default>1</default>
      <constraints>
        <minimum>1</minimum>
        <maximum>255</maximum>
        <step>1</step>
      </constraints>
    </integer>


    <double>
      <name>maxRunningTime</name>
      <longflag>maxRunningTime</longflag>
      <description>The program will stop if this time is reached.</description>
      <label>Max running time(min)</label>
      <default>10</default>
      <constraints>
        <minimum>0</minimum>
        <maximum>60</maximum>
        <step>1</step>
      </constraints>
    </double>

<!--     <integer> -->
<!--       <name>numOfIteration</name> -->
<!--       <longflag>numOfIteration</longflag> -->
<!--       <flag>n</flag> -->
<!--       <description>Max number of iteration</description> -->
<!--       <label>Number of iteration</label> -->
<!--       <default>1000000</default> -->
<!--       <constraints> -->
<!--         <minimum>1</minimum> -->
<!--         <maximum>1000000</maximum> -->
<!--         <step>100</step> -->
<!--       </constraints> -->
<!--     </integer> -->

  </parameters>


  <parameters>
    <label>IO</label>
    <description>Input/output parameters</description>
    <image>
      <name>originalImageFileName</name>
      <label>Original Image</label>
      <channel>input</channel>
      <index>0</index>
      <description>Original image to be segmented</description>
    </image>
    <image type="label">
      <name>segmentedImageFileName</name>
      <label>Output Volume</label>
      <channel>output</channel>
      <index>1</index>
      <description>Segmented image</description>
    </image>
  </parameters>

</executable>
