10. Run the featureMap on the input image. There are two hard
coded parameters, alpha and beta on line 49 of featureMap.cxx
that could be tuned to change the behavior of the sigmoid
function.

20. The result of the featureMap should be like bright inside the
target and dark outside.

30. Run gacSeg3 on the feature map generated in the previous
step. center position (x, y, z) of the object is needed.
