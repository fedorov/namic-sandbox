This directory is intended to contain prototype code for the segmenting cells
from confocal microscopy datasets.

Details are available at the Wiki page:

http://wiki.na-mic.org/Wiki/index.php/NA-MIC_NCBC_Collaboration:3D%2Bt_Cells_Lineage:GoFigure


