PROJECT(NDICAPI)
INCLUDE_REGULAR_EXPRESSION("^(ndicapi|polaris).*$")

INCLUDE_DIRECTORIES(${NDICAPI_SOURCE_DIR})
INCLUDE_DIRECTORIES(${NDICAPI_BINARY_DIR})

SET(common_SRCS
ndicapi.c ndicapi_math.c ndicapi_serial.c ndicapi_thread.c)

IF (WIN32)
  IF (NOT BORLAND)
    IF (NOT CYGWIN)
      SET(common_SRCS ${common_SRCS} ndicapi.def)
    ENDIF (NOT CYGWIN)
  ENDIF (NOT BORLAND)
ENDIF (WIN32)

ADD_LIBRARY(ndicapi STATIC ${common_SRCS})
TARGET_LINK_LIBRARIES( ndicapi )

INSTALL_TARGETS(/lib ndicapi)
