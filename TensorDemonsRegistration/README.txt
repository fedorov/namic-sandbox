This code has been taken from Slicer 2.6, from the module "AG" and DTIMRI that
were intended for performing deformable registration of DTMRI data.

The code was working with VTK 4.3.

-----

This code is being converted to ITK.

If you have questions regarding this directory,
please send email to westin at bwh dot harvard dot edu.


