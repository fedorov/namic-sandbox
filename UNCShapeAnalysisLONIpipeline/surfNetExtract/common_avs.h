/******************************************************************************
 *
 *                            U T I L I T I E S             
 *
 ******************************************************************************
 *
 *  File         : common_avs.h
 *
 *  Type         : Declarations, AVS Interface
 * 
 *  Purpose      : Declarations of external avs functions
 *
 ******************************************************************************
 *
 *  Author       : Markus Naef
 *
 *  started      : 14 Feb 94
 *  last change  : 14 Feb 94
 *
 *****************************************************************************/

#ifndef __cplusplus
#define __cplusplus 1
#define NeedFunctionPrototypes 0
#endif

#include <avs/avs.h>
#include <avs/field.h>
#include <avs/geom.h>
#include <avs/colormap.h>

