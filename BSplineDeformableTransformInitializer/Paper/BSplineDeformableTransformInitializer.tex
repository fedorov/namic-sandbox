\documentclass{InsightArticle}

\usepackage[dvips]{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  hyperref should be the last package to be loaded.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[dvips,
bookmarks,
bookmarksopen,
backref,
colorlinks,linkcolor={blue},citecolor={blue},urlcolor={blue},
]{hyperref}


\title{Helper class for initializing the grid parameters of a BSpline
deformable transform by using an image as reference}

\release{2.00}

\author{Luis Ibanez$^{1}$, Matt Turek$^{1}$, Michel Audette$^{1}$, Stephen Aylward$^{1}$}
\authoraddress{$^{1}$Kitware Inc., Clifton Park, NY}

\begin{document}

%
% Add hyperlink to the web location and license of the paper.
% The argument of this command is the handler identifier given
% by the Insight Journal to this paper.
%
\IJhandlefooter{216}

\ifpdf
\else
   %
   % Commands for including Graphics when using latex
   % 
   \DeclareGraphicsExtensions{.eps,.jpg,.gif,.tiff,.bmp,.png}
   \DeclareGraphicsRule{.jpg}{eps}{.jpg.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.gif}{eps}{.gif.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.tiff}{eps}{.tiff.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.bmp}{eps}{.bmp.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.png}{eps}{.png.bb}{`convert #1 eps:-}
\fi


\maketitle

\ifhtml
\chapter*{Front Matter\label{front}}
\fi


\begin{abstract}
\noindent
This document describes a simple helper class intended for making easy to
initialize the grid parameters of a \doxygen{BSplineDeformableTranform}.

This paper is accompanied with the source code, input data, parameters and
output data that we used for validating the algorithm described in this paper.
This adheres to the fundamental principle that scientific publications must
facilitate \textbf{reproducibility} of the reported results.
\end{abstract}

\tableofcontents

\section{Introduction}

When using a BSplineDeformableTranform for the purpose of performing deformable
registration or for the purpose of resampling an image, it is customary to
initialize the parameters of the BSpline grid based on the parameters of the
fixed image or based on the parameters of image that is the output space of the
resampling process. This initialization, however, requires the software
developer to remember to set several critical details. Forgetting any of these
details can easily lead to a failed registration or a failed resampling. The
process of finding the source of the problem can be a very frustrating
experience.

The class described in this report is intended to simplify the initialization
of a BSplineDeformableTranform and to ensure that no small detail is missed in
the process.


\section{How to use this class}

The two files in the ''Testing'' directory of this report provide typical
examples of how to use this initializer class. Please refer to that code for a
fully functional example.

Just for convenience, a minimal code snipped is shown below 

\subsection{Usage Case 1}
\label{sec:Case1}

In this example we illustrate the use of the method \code{SetGridSizeInsideTheImage}.

\small
\begin{verbatim}
  typedef itk::BSplineDeformableTransformInitializer<
                  TransformType, 
                  FixedImageType >      InitializerType;

  InitializerType::Pointer transformInitilizer = InitializerType::New();


  transformInitilizer->SetTransform( bsplineTransform );
  transformInitilizer->SetImage( fixedImage );

  typedef TransformType::RegionType RegionType;
  RegionType::SizeType   size;

  const unsigned int numberOfGridNodesInsideTheImageSupport = 5;

  size.Fill( numberOfGridNodesInsideTheImageSupport );

  transformInitilizer->SetGridSizeInsideTheImage( size );

  transformInitilizer->InitializeTransform();
 
\end{verbatim}
\normalsize


\subsection{Usage Case 2}
\label{sec:Case2}

In this case we don't use the method \code{SetGridSizeInsideTheImage}. Instead,
we invoke the equivalent method \code{SetNumberOfGridNodesInsideTheImage}.

\small
\begin{verbatim}
  typedef itk::BSplineDeformableTransformInitializer<
                  TransformType, 
                  FixedImageType >      InitializerType;

  InitializerType::Pointer transformInitilizer = InitializerType::New();


  transformInitilizer->SetTransform( bsplineTransform );
  transformInitilizer->SetImage( fixedImage );
  transformInitilizer->SetNumberOfGridNodesInsideTheImage( 5 );

  transformInitilizer->InitializeTransform();
 
\end{verbatim}
\normalsize


\section{Software Requirements}

In order to reproduce the results described in this report you need to have the
following software installed:

\begin{itemize}
  \item  Insight Toolkit 3.12.
  \item  CMake 2.4.8
\end{itemize}

If you want to generate this PDF document itself then you will also need a
functional LaTeX installation. This should be trivial in GNU/Linux.

\appendix

\section{Results}

The results described in this section can be reproduced by running the Tests in
the \code{Testing} subdirectory of this report. To build this paper and its
associated tests, please do the following:

\begin{itemize}
\item ccmake .
\item make
\item ctest
\end{itemize}

The first command will configure the build tree by using CMake. The second
command will build the executables and will build this PDF document. The last
command will run the tests that resample an image with a
BSplineDeformableTranform. 

An additional round of "make" commands may be needed if you are building the
PDF document of the paper. This is due to a cycle of progressive dependencies
in LaTeX.

Figure \ref{fig:Case1} illustrates the input and output of the first test
described in \ref{sec:Case1}.

\begin{figure}
\center
\includegraphics[width=0.4\textwidth]{../Testing/Temporary/BrainProtonDensitySliceBorder20.png}
\includegraphics[width=0.4\textwidth]{../Testing/Temporary/BSplineWarping2Test.png}
\itkcaption{Left: Input to the resampling filter. Right: Output of the
resampling filter. The image has been deformed by introducing deformations in
five grid nodes located towards the center of the image. This image was
generated with the source code illustrated in \ref{sec:Case1}.}
\label{fig:Case1}
\end{figure}

Figure \ref{fig:Case2} illustrates the input and output of the second test
described in \ref{sec:Case2}.

\begin{figure}
\center
\includegraphics[width=0.4\textwidth]{../Testing/Temporary/BrainProtonDensitySliceBorder20.png}
\includegraphics[width=0.4\textwidth]{../Testing/Temporary/BSplineWarping2Test.png}
\itkcaption{Left: Input to the resampling filter. Right: Output of the 
resampling filter. The image has been deformed by introducing deformations in
five grid nodes located towards the center of the image. This image was
generated with the source code illustrated in \ref{sec:Case2}.}
\label{fig:Case2}
\end{figure}





\end{document}

