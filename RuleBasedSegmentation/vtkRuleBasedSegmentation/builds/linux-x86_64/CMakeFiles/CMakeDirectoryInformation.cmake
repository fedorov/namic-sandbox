# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.4

# Force unix paths in dependencies.
SET(CMAKE_FORCE_UNIX_PATHS 1)

# The C and CXX include file search paths:
SET(CMAKE_C_INCLUDE_PATH
  "/software/slicer2/Modules/vtkRuleBasedSegmentation"
  "/software/slicer2/Lib/linux-x86_64/VTK-build"
  "/software/slicer2/Lib/linux-x86_64/VTK/Hybrid"
  "/software/slicer2/Lib/linux-x86_64/VTK/Patented"
  "/software/slicer2/Lib/linux-x86_64/VTK/Rendering"
  "/software/slicer2/Lib/linux-x86_64/VTK/IO"
  "/software/slicer2/Lib/linux-x86_64/VTK/Imaging"
  "/software/slicer2/Lib/linux-x86_64/VTK/Graphics"
  "/software/slicer2/Lib/linux-x86_64/VTK/Filtering"
  "/software/slicer2/Lib/linux-x86_64/VTK/Common"
  "/software/slicer2/Lib/linux-x86_64/VTK/Common/Testing/Cxx"
  "/software/slicer2/Lib/linux-x86_64/VTK-build/Utilities/zlib"
  "/software/slicer2/Lib/linux-x86_64/VTK/Utilities/zlib"
  "/software/slicer2/Lib/linux-x86_64/VTK-build/Utilities/jpeg"
  "/software/slicer2/Lib/linux-x86_64/VTK/Utilities/jpeg"
  "/software/slicer2/Lib/linux-x86_64/VTK-build/Utilities/png"
  "/software/slicer2/Lib/linux-x86_64/VTK/Utilities/png"
  "/software/slicer2/Lib/linux-x86_64/VTK-build/Utilities/tiff"
  "/software/slicer2/Lib/linux-x86_64/VTK/Utilities/tiff"
  "/software/slicer2/Lib/linux-x86_64/VTK-build/Utilities/expat"
  "/software/slicer2/Lib/linux-x86_64/VTK/Utilities/expat"
  "/software/slicer2/Lib/linux-x86_64/VTK-build/Utilities/DICOMParser"
  "/software/slicer2/Lib/linux-x86_64/VTK/Utilities/DICOMParser"
  "/software/slicer2/Lib/linux-x86_64/tcl-build/include"
  "/software/slicer2/Modules/vtkRuleBasedSegmentation/cxx"
  "."
  )
SET(CMAKE_CXX_INCLUDE_PATH ${CMAKE_C_INCLUDE_PATH})

# The C and CXX include file regular expressions for this directory.
SET(CMAKE_C_INCLUDE_REGEX_SCAN "^.*$")
SET(CMAKE_C_INCLUDE_REGEX_COMPLAIN "^$")
SET(CMAKE_CXX_INCLUDE_REGEX_SCAN ${CMAKE_C_INCLUDE_REGEX_SCAN})
SET(CMAKE_CXX_INCLUDE_REGEX_COMPLAIN ${CMAKE_C_INCLUDE_REGEX_COMPLAIN})

# The set of files generated by rules in this directory:
SET(CMAKE_GENERATED_FILES
)
