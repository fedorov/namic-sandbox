<?xml version="1.0" encoding = "Windows-1252"?>
<VisualStudioProject
  ProjectType="Visual C++"
  Version="7.10"
  Name="vtkRuleBasedSegmentationTCL"
  SccProjectName=""
  SccLocalPath=""
  Keyword="Win32Proj">
  <Platforms>
    <Platform
      Name="Win32"/>
  </Platforms>
  <Configurations>
    <Configuration
      Name="Debug|Win32"
      OutputDirectory="Debug"
      IntermediateDirectory="vtkRuleBasedSegmentationTCL.dir\Debug"
      ConfigurationType="2"
      UseOfMFC="0"
      ATLMinimizesCRunTimeLibraryUsage="FALSE"
      CharacterSet="2">
      <Tool
        Name="VCCLCompilerTool"
        AdditionalOptions=" /DWIN32 /D_WINDOWS  /Zm1000     -DNOMINMAX /D_DEBUG         -DCMAKE_INTDIR=\&quot;Debug\&quot;"
        AdditionalIncludeDirectories="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\gdcm;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\znzlib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\niftilib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\NrrdIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\MetaIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\SpatialObject;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\NeuralNetworks;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\Statistics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\FEM;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\IO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Common;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\BasicFilters;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Algorithms;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Hybrid;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Patented;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Rendering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\IO;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Imaging;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Graphics;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Filtering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common\Testing\Cxx;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\DICOMParser;D:\research\EXTERN~1\slicer2\Lib\win32\tcl-build\include;d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32;D:\research\EXTERN~1\slicer2\Modules\vtkITK\cxx;D:\research\EXTERN~1\slicer2\Modules\vtkITK\builds\win32;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation;"
        BasicRuntimeChecks="1"
        CompileAs="2"
        DebugInformationFormat="3"
        ExceptionHandling="TRUE"
        InlineFunctionExpansion="0"
        Optimization="0"
        RuntimeLibrary="3"
        RuntimeTypeInfo="TRUE"
        WarningLevel="3"
        PreprocessorDefinitions=",vtkRuleBasedSegmentationTCL_EXPORTS"
        AssemblerListingLocation="Debug"
        ObjectFile="$(IntDir)\"
        ProgramDatabaseFileName="d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/bin/$(OutDir)/vtkRuleBasedSegmentationTCL.pdb"
/>
      <Tool
        Name="VCCustomBuildTool"/>
      <Tool
        Name="VCResourceCompilerTool"
        AdditionalIncludeDirectories="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\gdcm;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\znzlib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\niftilib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\NrrdIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\MetaIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\SpatialObject;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\NeuralNetworks;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\Statistics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\FEM;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\IO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Common;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\BasicFilters;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Algorithms;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Hybrid;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Patented;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Rendering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\IO;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Imaging;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Graphics;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Filtering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common\Testing\Cxx;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\DICOMParser;D:\research\EXTERN~1\slicer2\Lib\win32\tcl-build\include;d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32;D:\research\EXTERN~1\slicer2\Modules\vtkITK\cxx;D:\research\EXTERN~1\slicer2\Modules\vtkITK\builds\win32;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation;"
        PreprocessorDefinitions="" />
      <Tool
        Name="VCMIDLTool"
        PreprocessorDefinitions=""
        MkTypLibCompatible="FALSE"
        TargetEnvironment="1"
        GenerateStublessProxies="TRUE"
        TypeLibraryName="$(InputName).tlb"
        OutputDirectory="$(IntDir)"
        HeaderFileName="$(InputName).h"
        DLLDataFileName=""
        InterfaceIdentifierFileName="$(InputName)_i.c"
        ProxyFileName="$(InputName)_p.c"/>
      <Tool
        Name="VCPreBuildEventTool"/>
      <Tool
        Name="VCPreLinkEventTool"/>
      <Tool
        Name="VCPostBuildEventTool"/>
      <Tool
        Name="VCLinkerTool"
        AdditionalOptions="/MACHINE:I386 /STACK:10000000 /machine:I386   /debug"
        AdditionalDependencies="$(NOINHERIT) kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib vtkITK.lib vtkITKTCL.lib vtkRuleBasedSegmentation.lib vtkIOTCL.lib vtkGraphicsTCL.lib vtkImagingTCL.lib vtkFilteringTCL.lib vtkCommonTCL.lib tcl84.lib vtkHybridTCL.lib vtkPatentedTCL.lib vtkRenderingTCL.lib ITKAlgorithms.lib ITKNumerics.lib ITKStatistics.lib ITKBasicFilters.lib ITKCommon.lib itkvnl_inst.lib itkvnl_algo.lib itkvnl.lib itkvcl.lib itknetlib.lib itksys.lib vtkITK.lib vtkHybrid.lib vtkPatented.lib vtkIOTCL.lib vtkRendering.lib vtkIO.lib vtkDICOMParser.lib vtkpng.lib vtktiff.lib vtkzlib.lib vtkjpeg.lib vtkexpat.lib vtkftgl.lib vtkfreetype.lib glu32.lib opengl32.lib vtkGraphicsTCL.lib vtkGraphics.lib vtkImagingTCL.lib vtkImaging.lib vtkFiltering.lib vtkCommon.lib tcl84.lib tk84.lib "
        OutputFile="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\Debug\vtkRuleBasedSegmentationTCL.dll"
        LinkIncremental="2"
        AdditionalLibraryDirectories="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin,d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\bin\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\bin,d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build,d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build,d:\research\ExternalPackages\slicer2\Modules\vtkITK\builds\win32\bin\debug\$(OutDir),d:\research\ExternalPackages\slicer2\Modules\vtkITK\builds\win32\bin\debug,..\bin\$(OutDir),..\bin,..\bin\$(OutDir),..\bin,d:\research\ExternalPackages\slicer2\Lib\win32\tcl-build\lib\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\tcl-build\lib"
        ProgramDatabaseFile="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\$(OutDir)\vtkRuleBasedSegmentationTCL.pdb"
        GenerateDebugInformation="TRUE"
        StackReserveSize="10000000"
        ImportLibrary="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\Debug\vtkRuleBasedSegmentationTCL.lib"/>
    </Configuration>
    <Configuration
      Name="RelWithDebInfo|Win32"
      OutputDirectory="RelWithDebInfo"
      IntermediateDirectory="vtkRuleBasedSegmentationTCL.dir\RelWithDebInfo"
      ConfigurationType="2"
      UseOfMFC="0"
      ATLMinimizesCRunTimeLibraryUsage="FALSE"
      CharacterSet="2">
      <Tool
        Name="VCCLCompilerTool"
        AdditionalOptions=" /DWIN32 /D_WINDOWS  /Zm1000     -DNOMINMAX     /D NDEBUG   -DCMAKE_INTDIR=\&quot;RelWithDebInfo\&quot;"
        AdditionalIncludeDirectories="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\gdcm;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\znzlib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\niftilib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\NrrdIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\MetaIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\SpatialObject;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\NeuralNetworks;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\Statistics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\FEM;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\IO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Common;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\BasicFilters;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Algorithms;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Hybrid;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Patented;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Rendering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\IO;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Imaging;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Graphics;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Filtering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common\Testing\Cxx;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\DICOMParser;D:\research\EXTERN~1\slicer2\Lib\win32\tcl-build\include;d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32;D:\research\EXTERN~1\slicer2\Modules\vtkITK\cxx;D:\research\EXTERN~1\slicer2\Modules\vtkITK\builds\win32;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation;"
        CompileAs="2"
        DebugInformationFormat="3"
        ExceptionHandling="TRUE"
        InlineFunctionExpansion="1"
        Optimization="2"
        RuntimeLibrary="2"
        RuntimeTypeInfo="TRUE"
        WarningLevel="3"
        PreprocessorDefinitions=",vtkRuleBasedSegmentationTCL_EXPORTS"
        AssemblerListingLocation="RelWithDebInfo"
        ObjectFile="$(IntDir)\"
        ProgramDatabaseFileName="d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/bin/$(OutDir)/vtkRuleBasedSegmentationTCL.pdb"
/>
      <Tool
        Name="VCCustomBuildTool"/>
      <Tool
        Name="VCResourceCompilerTool"
        AdditionalIncludeDirectories="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\gdcm;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\znzlib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\niftilib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\NrrdIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\MetaIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\SpatialObject;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\NeuralNetworks;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\Statistics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\FEM;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\IO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Common;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\BasicFilters;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Algorithms;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Hybrid;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Patented;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Rendering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\IO;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Imaging;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Graphics;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Filtering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common\Testing\Cxx;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\DICOMParser;D:\research\EXTERN~1\slicer2\Lib\win32\tcl-build\include;d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32;D:\research\EXTERN~1\slicer2\Modules\vtkITK\cxx;D:\research\EXTERN~1\slicer2\Modules\vtkITK\builds\win32;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation;"
        PreprocessorDefinitions="" />
      <Tool
        Name="VCMIDLTool"
        PreprocessorDefinitions=""
        MkTypLibCompatible="FALSE"
        TargetEnvironment="1"
        GenerateStublessProxies="TRUE"
        TypeLibraryName="$(InputName).tlb"
        OutputDirectory="$(IntDir)"
        HeaderFileName="$(InputName).h"
        DLLDataFileName=""
        InterfaceIdentifierFileName="$(InputName)_i.c"
        ProxyFileName="$(InputName)_p.c"/>
      <Tool
        Name="VCPreBuildEventTool"/>
      <Tool
        Name="VCPreLinkEventTool"/>
      <Tool
        Name="VCPostBuildEventTool"/>
      <Tool
        Name="VCLinkerTool"
        AdditionalOptions="/MACHINE:I386 /STACK:10000000 /machine:I386   /debug"
        AdditionalDependencies="$(NOINHERIT) kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib vtkITK.lib vtkITKTCL.lib vtkRuleBasedSegmentation.lib vtkIOTCL.lib vtkGraphicsTCL.lib vtkImagingTCL.lib vtkFilteringTCL.lib vtkCommonTCL.lib tcl84.lib vtkHybridTCL.lib vtkPatentedTCL.lib vtkRenderingTCL.lib ITKAlgorithms.lib ITKNumerics.lib ITKStatistics.lib ITKBasicFilters.lib ITKCommon.lib itkvnl_inst.lib itkvnl_algo.lib itkvnl.lib itkvcl.lib itknetlib.lib itksys.lib vtkITK.lib vtkHybrid.lib vtkPatented.lib vtkIOTCL.lib vtkRendering.lib vtkIO.lib vtkDICOMParser.lib vtkpng.lib vtktiff.lib vtkzlib.lib vtkjpeg.lib vtkexpat.lib vtkftgl.lib vtkfreetype.lib glu32.lib opengl32.lib vtkGraphicsTCL.lib vtkGraphics.lib vtkImagingTCL.lib vtkImaging.lib vtkFiltering.lib vtkCommon.lib tcl84.lib tk84.lib "
        OutputFile="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\RelWithDebInfo\vtkRuleBasedSegmentationTCL.dll"
        LinkIncremental="2"
        AdditionalLibraryDirectories="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin,d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\bin\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\bin,d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build,d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build,d:\research\ExternalPackages\slicer2\Modules\vtkITK\builds\win32\bin\debug\$(OutDir),d:\research\ExternalPackages\slicer2\Modules\vtkITK\builds\win32\bin\debug,..\bin\$(OutDir),..\bin,..\bin\$(OutDir),..\bin,d:\research\ExternalPackages\slicer2\Lib\win32\tcl-build\lib\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\tcl-build\lib"
        ProgramDatabaseFile="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\$(OutDir)\vtkRuleBasedSegmentationTCL.pdb"
        GenerateDebugInformation="TRUE"
        StackReserveSize="10000000"
        ImportLibrary="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\RelWithDebInfo\vtkRuleBasedSegmentationTCL.lib"/>
    </Configuration>
    <Configuration
      Name="Release|Win32"
      OutputDirectory="Release"
      IntermediateDirectory="vtkRuleBasedSegmentationTCL.dir\Release"
      ConfigurationType="2"
      UseOfMFC="0"
      ATLMinimizesCRunTimeLibraryUsage="FALSE"
      CharacterSet="2">
      <Tool
        Name="VCCLCompilerTool"
        AdditionalOptions=" /DWIN32 /D_WINDOWS  /Zm1000     -DNOMINMAX    /D NDEBUG   -DCMAKE_INTDIR=\&quot;Release\&quot;"
        AdditionalIncludeDirectories="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\gdcm;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\znzlib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\niftilib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\NrrdIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\MetaIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\SpatialObject;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\NeuralNetworks;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\Statistics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\FEM;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\IO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Common;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\BasicFilters;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Algorithms;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Hybrid;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Patented;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Rendering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\IO;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Imaging;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Graphics;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Filtering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common\Testing\Cxx;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\DICOMParser;D:\research\EXTERN~1\slicer2\Lib\win32\tcl-build\include;d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32;D:\research\EXTERN~1\slicer2\Modules\vtkITK\cxx;D:\research\EXTERN~1\slicer2\Modules\vtkITK\builds\win32;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation;"
        CompileAs="2"
        ExceptionHandling="TRUE"
        InlineFunctionExpansion="2"
        Optimization="2"
        RuntimeLibrary="2"
        RuntimeTypeInfo="TRUE"
        WarningLevel="3"
        PreprocessorDefinitions=",vtkRuleBasedSegmentationTCL_EXPORTS"
        AssemblerListingLocation="Release"
        ObjectFile="$(IntDir)\"
/>
      <Tool
        Name="VCCustomBuildTool"/>
      <Tool
        Name="VCResourceCompilerTool"
        AdditionalIncludeDirectories="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\core;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\vxl\vcl;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\gdcm;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\znzlib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\nifti\niftilib;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\NrrdIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Utilities\MetaIO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\SpatialObject;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\NeuralNetworks;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\Statistics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics\FEM;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\IO;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Numerics;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Common;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\BasicFilters;d:\research\ExternalPackages\slicer2\Lib\win32\Insight\Code\Algorithms;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Hybrid;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Patented;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Rendering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\IO;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Imaging;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Graphics;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Filtering;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Common\Testing\Cxx;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\zlib;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\jpeg;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\png;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\tiff;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\expat;d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\Utilities\DICOMParser;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Utilities\DICOMParser;D:\research\EXTERN~1\slicer2\Lib\win32\tcl-build\include;d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32;D:\research\EXTERN~1\slicer2\Modules\vtkITK\cxx;D:\research\EXTERN~1\slicer2\Modules\vtkITK\builds\win32;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation;"
        PreprocessorDefinitions="" />
      <Tool
        Name="VCMIDLTool"
        PreprocessorDefinitions=""
        MkTypLibCompatible="FALSE"
        TargetEnvironment="1"
        GenerateStublessProxies="TRUE"
        TypeLibraryName="$(InputName).tlb"
        OutputDirectory="$(IntDir)"
        HeaderFileName="$(InputName).h"
        DLLDataFileName=""
        InterfaceIdentifierFileName="$(InputName)_i.c"
        ProxyFileName="$(InputName)_p.c"/>
      <Tool
        Name="VCPreBuildEventTool"/>
      <Tool
        Name="VCPreLinkEventTool"/>
      <Tool
        Name="VCPostBuildEventTool"/>
      <Tool
        Name="VCLinkerTool"
        AdditionalOptions="/MACHINE:I386 /STACK:10000000 /machine:I386   "
        AdditionalDependencies="$(NOINHERIT) kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib vtkITK.lib vtkITKTCL.lib vtkRuleBasedSegmentation.lib vtkIOTCL.lib vtkGraphicsTCL.lib vtkImagingTCL.lib vtkFilteringTCL.lib vtkCommonTCL.lib tcl84.lib vtkHybridTCL.lib vtkPatentedTCL.lib vtkRenderingTCL.lib ITKAlgorithms.lib ITKNumerics.lib ITKStatistics.lib ITKBasicFilters.lib ITKCommon.lib itkvnl_inst.lib itkvnl_algo.lib itkvnl.lib itkvcl.lib itknetlib.lib itksys.lib vtkITK.lib vtkHybrid.lib vtkPatented.lib vtkIOTCL.lib vtkRendering.lib vtkIO.lib vtkDICOMParser.lib vtkpng.lib vtktiff.lib vtkzlib.lib vtkjpeg.lib vtkexpat.lib vtkftgl.lib vtkfreetype.lib glu32.lib opengl32.lib vtkGraphicsTCL.lib vtkGraphics.lib vtkImagingTCL.lib vtkImaging.lib vtkFiltering.lib vtkCommon.lib tcl84.lib tk84.lib "
        OutputFile="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\Release\vtkRuleBasedSegmentationTCL.dll"
        LinkIncremental="2"
        AdditionalLibraryDirectories="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin,d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\bin\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\bin,d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build,d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build,d:\research\ExternalPackages\slicer2\Modules\vtkITK\builds\win32\bin\debug\$(OutDir),d:\research\ExternalPackages\slicer2\Modules\vtkITK\builds\win32\bin\debug,..\bin\$(OutDir),..\bin,..\bin\$(OutDir),..\bin,d:\research\ExternalPackages\slicer2\Lib\win32\tcl-build\lib\$(OutDir),d:\research\ExternalPackages\slicer2\Lib\win32\tcl-build\lib"
        ProgramDatabaseFile="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\$(OutDir)\vtkRuleBasedSegmentationTCL.pdb"
        StackReserveSize="10000000"
        ImportLibrary="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\bin\Release\vtkRuleBasedSegmentationTCL.lib"/>
    </Configuration>
  </Configurations>
  <Files>
      <File
        RelativePath="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeLists.txt">
        <FileConfiguration
          Name="Debug|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Building Custom Rule D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/CMakeLists.txt"
          CommandLine="D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\bin\cmake.exe -HD:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation -Bd:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32&#x0D;&#x0A;"
          AdditionalDependencies="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeLists.txt;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeListsLocal.txt;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Modules\FindITK.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKConfig.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKLibraryDepends.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\UseITK.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Modules\CMakeImportBuildSettings.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKBuildSettings.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Templates\CMakeWindowsSystemConfig.cmake;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeLists.txt;"
          Outputs="vtkRuleBasedSegmentationTCL.vcproj.cmake"/>
        </FileConfiguration>
        <FileConfiguration
          Name="RelWithDebInfo|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Building Custom Rule D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/CMakeLists.txt"
          CommandLine="D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\bin\cmake.exe -HD:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation -Bd:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32&#x0D;&#x0A;"
          AdditionalDependencies="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeLists.txt;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeListsLocal.txt;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Modules\FindITK.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKConfig.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKLibraryDepends.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\UseITK.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Modules\CMakeImportBuildSettings.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKBuildSettings.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Templates\CMakeWindowsSystemConfig.cmake;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeLists.txt;"
          Outputs="vtkRuleBasedSegmentationTCL.vcproj.cmake"/>
        </FileConfiguration>
        <FileConfiguration
          Name="Release|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Building Custom Rule D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/CMakeLists.txt"
          CommandLine="D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\bin\cmake.exe -HD:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation -Bd:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32&#x0D;&#x0A;"
          AdditionalDependencies="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeLists.txt;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeListsLocal.txt;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Modules\FindITK.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKConfig.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKLibraryDepends.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\UseITK.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Modules\CMakeImportBuildSettings.cmake;d:\research\ExternalPackages\slicer2\Lib\win32\Insight-build\ITKBuildSettings.cmake;D:\research\ExternalPackages\slicer2\Lib\win32\CMake-build\share\CMake\Templates\CMakeWindowsSystemConfig.cmake;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\CMakeLists.txt;"
          Outputs="vtkRuleBasedSegmentationTCL.vcproj.cmake"/>
        </FileConfiguration>
      </File>
    <Filter
      Name="Source Files"
      Filter="">
      <File
        RelativePath="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkRuleBasedSegmentationTCLInit.cxx">
      </File>
      <File
        RelativePath="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKBayesianClassificationImageFilterTcl.cxx">
      </File>
      <File
        RelativePath="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKImageToImageFilterULULTcl.cxx">
      </File>
    </Filter>
    <Filter
      Name="Header Files"
      Filter="">
      <File
        RelativePath="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKBayesianClassificationImageFilter.h">
        <FileConfiguration
          Name="Debug|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Generating vtkITKBayesianClassificationImageFilterTcl.cxx"
          CommandLine="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/vtkITKBayesianClassificationImageFilter.h d:/research/ExternalPackages/slicer2/Lib/win32/VTK/Wrapping/hints 1 d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/cxx/vtkITKBayesianClassificationImageFilterTcl.cxx&#x0D;&#x0A;"
          AdditionalDependencies="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Wrapping\hints;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKBayesianClassificationImageFilter.h;"
          Outputs="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKBayesianClassificationImageFilterTcl.cxx"/>
        </FileConfiguration>
        <FileConfiguration
          Name="RelWithDebInfo|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Generating vtkITKBayesianClassificationImageFilterTcl.cxx"
          CommandLine="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/vtkITKBayesianClassificationImageFilter.h d:/research/ExternalPackages/slicer2/Lib/win32/VTK/Wrapping/hints 1 d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/cxx/vtkITKBayesianClassificationImageFilterTcl.cxx&#x0D;&#x0A;"
          AdditionalDependencies="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Wrapping\hints;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKBayesianClassificationImageFilter.h;"
          Outputs="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKBayesianClassificationImageFilterTcl.cxx"/>
        </FileConfiguration>
        <FileConfiguration
          Name="Release|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Generating vtkITKBayesianClassificationImageFilterTcl.cxx"
          CommandLine="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/vtkITKBayesianClassificationImageFilter.h d:/research/ExternalPackages/slicer2/Lib/win32/VTK/Wrapping/hints 1 d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/cxx/vtkITKBayesianClassificationImageFilterTcl.cxx&#x0D;&#x0A;"
          AdditionalDependencies="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Wrapping\hints;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKBayesianClassificationImageFilter.h;"
          Outputs="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKBayesianClassificationImageFilterTcl.cxx"/>
        </FileConfiguration>
      </File>
      <File
        RelativePath="D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKImageToImageFilterULUL.h">
        <FileConfiguration
          Name="Debug|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Generating vtkITKImageToImageFilterULULTcl.cxx"
          CommandLine="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/vtkITKImageToImageFilterULUL.h d:/research/ExternalPackages/slicer2/Lib/win32/VTK/Wrapping/hints 1 d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/cxx/vtkITKImageToImageFilterULULTcl.cxx&#x0D;&#x0A;"
          AdditionalDependencies="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Wrapping\hints;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKImageToImageFilterULUL.h;"
          Outputs="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKImageToImageFilterULULTcl.cxx"/>
        </FileConfiguration>
        <FileConfiguration
          Name="RelWithDebInfo|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Generating vtkITKImageToImageFilterULULTcl.cxx"
          CommandLine="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/vtkITKImageToImageFilterULUL.h d:/research/ExternalPackages/slicer2/Lib/win32/VTK/Wrapping/hints 1 d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/cxx/vtkITKImageToImageFilterULULTcl.cxx&#x0D;&#x0A;"
          AdditionalDependencies="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Wrapping\hints;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKImageToImageFilterULUL.h;"
          Outputs="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKImageToImageFilterULULTcl.cxx"/>
        </FileConfiguration>
        <FileConfiguration
          Name="Release|Win32">
          <Tool
          Name="VCCustomBuildTool"
          Description="Generating vtkITKImageToImageFilterULULTcl.cxx"
          CommandLine="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe D:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/cxx/vtkITKImageToImageFilterULUL.h d:/research/ExternalPackages/slicer2/Lib/win32/VTK/Wrapping/hints 1 d:/research/ExternalPackages/slicer2/Modules/vtkRuleBasedSegmentation/builds/win32/cxx/vtkITKImageToImageFilterULULTcl.cxx&#x0D;&#x0A;"
          AdditionalDependencies="d:\research\ExternalPackages\slicer2\Lib\win32\VTK-build\bin\$(OutDir)\vtkWrapTcl.exe;d:\research\ExternalPackages\slicer2\Lib\win32\VTK\Wrapping\hints;D:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\cxx\vtkITKImageToImageFilterULUL.h;"
          Outputs="d:\research\ExternalPackages\slicer2\Modules\vtkRuleBasedSegmentation\builds\win32\cxx\vtkITKImageToImageFilterULULTcl.cxx"/>
        </FileConfiguration>
      </File>
    </Filter>
  </Files>
  <Globals>
  </Globals>
</VisualStudioProject>
