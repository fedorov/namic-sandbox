
SET(ShapeIO_SRCS
SphericalHarmonicCoefficientFileReader.cxx
SphericalHarmonicCoefficientFileWriter.cxx
)

ADD_LIBRARY(ShapeIO ${ShapeIO_SRCS})
