// VisualAttributeMeshProperty.h: interface for the VisualAttributeMeshProperty class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VISUALATTRIBUTEMESHPROPERTY_H__7EE14766_BE3A_44D7_A6BD_E00BACC85882__INCLUDED_)
#define AFX_VISUALATTRIBUTEMESHPROPERTY_H__7EE14766_BE3A_44D7_A6BD_E00BACC85882__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class VisualAttributeMeshProperty  
{
public:
  VisualAttributeMeshProperty();
  virtual ~VisualAttributeMeshProperty();

};

#endif // !defined(AFX_VISUALATTRIBUTEMESHPROPERTY_H__7EE14766_BE3A_44D7_A6BD_E00BACC85882__INCLUDED_)
