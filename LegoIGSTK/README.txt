
This is an example of how to use the code from ../LegoTutorial
for managing a robot suitable for testing the calibration of
an optical or electromagnetic tracker under the control of IGSTK.

Thanks to 

        * Danielle F. Pace, B. CmpH
        * Ron Kikinis, M.D.
        * Nobuhiko Hata, Ph.D.

for creating and making available the material of the LegoTutorial
in a format truly intended for facilitating others to reuse their 
work.

