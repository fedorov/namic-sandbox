/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkSlicerFixedPointVolumeRayCastHelper2.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkSlicerFixedPointVolumeRayCastHelper2.h"
#include "vtkObjectFactory.h"

#include <math.h>

vtkCxxRevisionMacro(vtkSlicerFixedPointVolumeRayCastHelper2, "$Revision: 1.2 $");
vtkStandardNewMacro(vtkSlicerFixedPointVolumeRayCastHelper2);

vtkSlicerFixedPointVolumeRayCastHelper2::vtkSlicerFixedPointVolumeRayCastHelper2()
{
}

vtkSlicerFixedPointVolumeRayCastHelper2::~vtkSlicerFixedPointVolumeRayCastHelper2()
{
}

void vtkSlicerFixedPointVolumeRayCastHelper2::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}


