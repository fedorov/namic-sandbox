
#ifndef __MachineInformation_h__
#define __MachineInformation_h__

#include <iostream>

class MachineInformation
{
public:
  static void DescribeMachine( std::ostream& );
  static void DescribeMachineLatex( std::ostream& );

protected:

};

#endif // __MachineInformation_h__
