
These file were taken from the distribution of MetaKit, version 2.4.9.5. 

Only the minimal set of files was conserved. They correspond to the original
directories {src, include}.

CMakeFiles were added in order to build this minimal library.

The license of MetaKit allow us to take pieces, as long as we keep the
copyright notice of the files.


