
This is a review of the ITK statistics framework

Detail information can be found at  
http://www.itk.org/Wiki/Proposals:Refactoring_Statistics_Framework_2007

This directory contains two subdirectories

   Source
   Testing

The content of Source is intended to become the new content of
Insight/Code/Numerics/Statistics

The content of Testing is intended to become the new content of
Insight/Testing/Code/Numerics/Statistics



