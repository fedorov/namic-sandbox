
This directory is intended to collaborate in a solution for the DIADEM Challenge.

The purpose is to provide an Open Source implementation for this challenge.


===========================================================================


 The DIADEM Challenge

 

Neuroscientists map the tree-like structure of nerve cells to better understand
how networks of neurons assemble into circuits to enable complex behavior must
now build those maps by hand. Despite the advent of computer technology that
enables mapping in three dimensions, neuronal reconstructions are still largely
performed by hand and reconstructing a single cell may take months. The vast
majority of axons (the long neuronal projections that transmit information to
neighboring cells) and dendrites (the branches on nerve cells that receive
information from neighboring cells) must be traced manually. The lack of
powerful – and effective – computational tools to automatically reconstruct
neuronal arbors has emerged as a major technical bottleneck in neuroscience
research.

 

Organizers of a new competition hope to provide incentives for the development
of new computer algorithms to advance the field – including a cash prize of up
to $75,000 for the qualifying winner. The DIADEM Competition – short for
Digital Reconstruction of Axonal and Dendritic Morphology – will bring together
computational and experimental scientists to test the most promising new
approaches against the latest data in a real-world environment.

 

The competition is open to individuals and teams from the private sector and
academic laboratories. Competitors will have a year to design an algorithm and
to test it against the manual gold standard. Up to five finalists will compete
in the final phase at the Janelia Farm Research Campus in August 2010.

 

The DIADEM Competition is organized by the Allen Institute for Brain Science
(AIBS), the Janelia Farm Research Campus of the Howard Hughes Medical Institute
(HHMI) and the Krasnow Institute of George Mason University (Krasnow
Institute). The cash prize has been established by the AIBS and HHMI.

 

Five Institutes of the National Institutes of Health (NIH), including the
National Institute of Neurological Disorders and Stroke (NINDS), the National
Institute of Mental Health (NIMH),the National Institute on Drug Abuse (NIDA),
the National Institute of Biomedical Engineering and Bioimaging (NIBIB), and
the National Institute of General Medical Sciences (NIGMS) are providing
partial support for a scientific conference that is independent of— but held in
conjunction with—the final phase of the DIADEM challenge.*

 

For data download, the rules and more details, please visit
http://DIADEMchallenge.org

 

*The co-sponsorship of the conference by these NIH institutes does not
represent an endorsement of the DIADEM Challenge, specific software products
that may emerge from the competition or the activities of the AIBS, HHMI, or
the Krasnow Institute.

 
