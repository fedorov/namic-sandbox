This project is implemented by

+ Serdar K Balci (serdar at csail.mit.edu)

and supervised by

+ Polina Golland
+ William M. Wells


The goal of the project is to provide an open source 
implementation of a non-rigid groupwise registration method.

   
#
#
#  DOCUMENTATION
#
#        
    
    For documentation check SourceGuide.txt   

#
#
# DATA
#
#    
    
Obtain test data at

http://public.kitware.com/pub/itk/Data/BrainWeb/BrainPart1.tgz
http://public.kitware.com/pub/itk/Data/BrainWeb/BrainPart2.tgz
http://public.kitware.com/pub/itk/Data/BrainWeb/BrainPart3.tgz
http://public.kitware.com/pub/itk/Data/DTI/Reconstructed/FAImage.tar.gz    
    
Extract files and point the path of the folders to cmake for testing.
Type ctest or "make test" to run the tests    
 
