Tests from 001 to 018 are groupwise registrations of PD, T1, T2 images.
Tests from 101 to 106 are groupwise registration of FA images.
    
For more information about the individual tests, see README files in subdirectories.

#
# HOW TO OBTAIN TEST DATA
#
All test data are from ITK's data folders.
    
Download and extract the following files.
Supply the path of the output folders to CMAKE.(avoid spaces in path names)
    
http://public.kitware.com/pub/itk/Data/BrainWeb/BrainPart1.tgz
http://public.kitware.com/pub/itk/Data/BrainWeb/BrainPart2.tgz
http://public.kitware.com/pub/itk/Data/BrainWeb/BrainPart3.tgz
    
http://public.kitware.com/pub/itk/Data/DTI/Reconstructed/FAImage.tar.gz

