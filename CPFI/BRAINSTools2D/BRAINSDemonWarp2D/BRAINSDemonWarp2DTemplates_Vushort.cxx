#include "BRAINSDemonWarp2DTemplates.h"

void VectorProcessOutputType_ushort(struct BRAINSDemonWarp2DAppParameters & command)
{
  VectorProcessOutputType< unsigned short >(command);
}
