#include <iostream>
#include "BRAINSDemonWarp2DTemplates.h"

// main function built in BRAINSDemonWarp2DPrimary.cxx so that testing only builds
// templates once.

int main(int argc, char *argv[])
{
  std::cout << "DEPRECATED:" << argv[0] << std::endl;
  std::cout << "DEPRECATED:" << argv[0] << std::endl;
  std::cout << "DEPRECATED:" << argv[0] << std::endl;
  std::cout << "DEPRECATED:  You should use BRAINSDemonWarp2D instead." << std::endl;
  std::cout << "DEPRECATED:  BRAINSDemonWarp2D has the same command line." << std::endl;
  std::cout << "DEPRECATED:" << argv[0] << std::endl;
  std::cout << "DEPRECATED:" << argv[0] << std::endl;
  std::cout << "DEPRECATED:" << argv[0] << std::endl;

  return BRAINSDemonWarp2DPrimary(argc, argv);
}
