#include "BRAINSDemonWarp2DTemplates.h"

void VectorProcessOutputType_uchar(struct BRAINSDemonWarp2DAppParameters & command)
{
  VectorProcessOutputType< unsigned char >(command);
}
