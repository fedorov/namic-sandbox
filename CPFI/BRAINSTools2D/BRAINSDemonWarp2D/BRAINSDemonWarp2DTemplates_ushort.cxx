#include "BRAINSDemonWarp2DTemplates.h"

void ProcessOutputType_ushort(struct BRAINSDemonWarp2DAppParameters & command)
{
  ProcessOutputType< unsigned short >(command);
}
