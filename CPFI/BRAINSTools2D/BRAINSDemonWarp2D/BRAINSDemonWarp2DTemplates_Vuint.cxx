#include "BRAINSDemonWarp2DTemplates.h"

void VectorProcessOutputType_uint(struct BRAINSDemonWarp2DAppParameters & command)
{
  VectorProcessOutputType< unsigned int >(command);
}
