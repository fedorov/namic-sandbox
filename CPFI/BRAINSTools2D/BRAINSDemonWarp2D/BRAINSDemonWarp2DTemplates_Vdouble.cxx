#include "BRAINSDemonWarp2DTemplates.h"

void VectorProcessOutputType_double(struct BRAINSDemonWarp2DAppParameters & command)
{
  VectorProcessOutputType< double >(command);
}
