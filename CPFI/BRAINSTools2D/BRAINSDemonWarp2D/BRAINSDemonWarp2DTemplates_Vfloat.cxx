#include "BRAINSDemonWarp2DTemplates.h"

void VectorProcessOutputType_float(struct BRAINSDemonWarp2DAppParameters & command)
{
  VectorProcessOutputType< float >(command);
}
