#include "vtkSlicerLoadSaveDialog.h"
#include "vtkObjectFactory.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkSlicerLoadSaveDialog );
vtkCxxRevisionMacro(vtkSlicerLoadSaveDialog, "$Revision: 1.0 $");

//----------------------------------------------------------------------------
void vtkSlicerLoadSaveDialog::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

