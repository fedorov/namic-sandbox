/* 
 * Resource generated for file:
 *    SlicerBlank.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerBlank_width          = 21;
static const unsigned int  image_SlicerBlank_height         = 21;
static const unsigned int  image_SlicerBlank_pixel_size     = 3;
static const unsigned long image_SlicerBlank_length         = 28;
static const unsigned long image_SlicerBlank_decoded_length = 1323;

static const unsigned char image_SlicerBlank[] = 
  "eNr7/38UjIJRMApGAQEAAKGpJiE=";

/* 
 * Resource generated for file:
 *    SlicerCancel.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerCancel_width          = 21;
static const unsigned int  image_SlicerCancel_height         = 21;
static const unsigned int  image_SlicerCancel_pixel_size     = 3;
static const unsigned long image_SlicerCancel_length         = 820;
static const unsigned long image_SlicerCancel_decoded_length = 1323;

static const unsigned char image_SlicerCancel[] = 
  "eNq9lPtLU3EYxndh9jeEdDNTGjJxWpq2nMq8lOQgKUSsMC9F9ZPtBwnrh0rCilkpFkEYSx"
  "ZprcsmzljacnRBS8QaWghS7Zzdum2uLebenq02rSPdoB6+vLw8z+eBwzl8D483J35EAoFA"
  "KBSKRKJFUcXFxcERRMTjiP5W/6T+8T2ZjXSuhRprqU4ZntjhwP9lfWyETjRRhYIqiqgyer"
  "DDgY/0J/XRYWqoo7INtL2UmhupV0eT1vDEDgc+UjAL1t+9pcMHqCiLqsvpjoGcdnI55uaA"
  "kWq2hVMwICPCZ4rVZ/U6kkupLJ/6bpPDTh1qqlJS8TqqryTLIPl8ZOoLp3JpmOTU3U0qZ7"
  "bkk2pfyM5Q20nKy6C8dCrNpQtnKRAg892Qg0UKBiT34a3KQuta8YcebdBhd20pduakugpz"
  "vB2nQwE/pruqHD5SMCC59Xvpqx9Ik73jY59Z5nlWymRBpqNdjS4mdjjwkYIBya33l8h7JY"
  "mvrlz229mRrZun21tnA/7pM6ceyTIsacmPN+XDRwoGZKzOj9bNDfu14hVDe3b5WIYdNM36"
  "/RNtamO2VC9ZZUhNGm856mNsSMGA/Pbq5tWfdWvPJy3VrJFM6W+GgsHXAyaDsqQrZeUNhW"
  "z4+JEZlnlxvRspGJDc+ozbpdtdrU5cckmRO3FL52FsXpbx2N58ndZrVzsL1iMFA5Jbh6Ys"
  "9y8qNx5LiG9NE/fU7nii6bSNPsXEDgc+UjAx/oc69HLIrKnZeXDZ4kPLvztw4COdD/MXun"
  "Eel+uhtkuzt75ZlqlKiMfEDgf+/7jvf1if+9f93pmvLwvVV9s=";

/* 
 * Resource generated for file:
 *    SlicerCancelDisabled.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerCancelDisabled_width          = 21;
static const unsigned int  image_SlicerCancelDisabled_height         = 21;
static const unsigned int  image_SlicerCancelDisabled_pixel_size     = 3;
static const unsigned long image_SlicerCancelDisabled_length         = 656;
static const unsigned long image_SlicerCancelDisabled_decoded_length = 1323;

static const unsigned char image_SlicerCancelDisabled[] = 
  "eNqllFuLQWEUhv85U0rJhRTCBQpRYoRxzDFnCTmHchpyyIULbtTMk1079hhTY12svt71vH"
  "ut74DT+Wp8/Tce2i+Xy/l8Ph6Ph8Nhv9+TWaOg/2kH2+12/X6/XC6nUqlIJEJmjYJO9Yn9"
  "dDpNJpNCoRAKhT7uAwWdKsxDO18ej8f0CgQC4XA4n893Op3pdEpmjYJOFUacQbSzr+12m8"
  "vl/H5/NBrt9Xqf98HwsViMKgykcA6inQ+2222fzxcMBrvdLny1WmXj8IlEYjgcbjYbvkkV"
  "BlIYQLRztslk0uVykZfLZaVSeb8GA9dqNdoNBgO+KTLwt3Zux+1222y2er2OHSOYx+MplU"
  "r0JdMXnSoMJPytnfs1mUxms3k0Gi0WCxiHw8GJ4SWzRkGnCgMJL+lusVh0Oh1XDEbrbDaL"
  "l4yOxW63o1OFQZF0Zy9er1ej0TDwfD5vtVp40+k0jXTXiMfj6FRhICV75yR5FSqVSqvVcu"
  "Y8sGazabVagWnNFeClNVUYSMnJc4/r9Zo9KpVKg8EAyYOZzWZiLhaLer2eKgyk5N6FAeho"
  "NBoVCoVarWazbJy7JrNGQacK8/PViW++0WhwyHK5/O0+UNCp/vbmxRmYLZPJCB1lMpkwCc"
  "pqtXr+i3v99/6Pf5tX4hsMRCuY";

/* 
 * Resource generated for file:
 *    SlicerCancelRequested.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerCancelRequested_width          = 21;
static const unsigned int  image_SlicerCancelRequested_height         = 21;
static const unsigned int  image_SlicerCancelRequested_pixel_size     = 3;
static const unsigned long image_SlicerCancelRequested_length         = 968;
static const unsigned long image_SlicerCancelRequested_decoded_length = 1323;

static const unsigned char image_SlicerCancelRequested[] = 
  "eNqtlM1PE2EQxv8Yox4kGC2mKnH5Cp+KkejRC/8Adw+aeDApGlJoJR5qKhrtiRZQBDxKoq"
  "W0CtKodBsKLWV3u7WVtgjdfq0/u6ZpIDHGuIfJZOaZeWee93lX139/lUqlWCzmcrlwOOz1"
  "ehcWFiYnJ10uFxafCHGyYEDqRz5N07a2tpaXlxcXF1dWViKRiKIo6XQai0+EOFkwhULhaH"
  "kmk/H7/Wtrazs7O4lEQpKkWCwGGItPhDjZQCCQzWb1upnpZkQy1S8UCrnd7uHh4aGhocHB"
  "QSw+EeLAmB8klipj32g0Sk8jzqhOp9Nut09PTzPq+vo6Fp/IxMSEqqpGLXiqqKWhz+cLBo"
  "M4Rja+va3K8l61f7lcxuKr1XXIJpNJkOCpwoFPOGEvZqM/mOL+fklRklbb547LviYBm3zs"
  "LHxPl0olsmBEUQRPlXFHsMq5bMeESUWh9vudu5UbA/rVTr37fOXm9eLLmdTs3A8WkyQwIM"
  "FTRe38/PzGxgZtLRYLO/6a0zqmDVzTW5v0NpN+pUV32LnUPdeLzfsju7u7U1NTIMFTRS2q"
  "oCs8wDAswUawoy/V2ZJsPv2tq3nv0VhFy2Pl/u5Aey/aAAMSPFWGrlKpFPfLHcEzd7FkEr"
  "6aG8QOszpupRaL/8Xc4DUJZMGABE+Vocn60+F5ta3vU39PdHy0rOWjthHfpaZ3Z05+FC4Q"
  "Z7ZDpx/aPZfNSk+fS25POZ8PPbS+uXh2tvH4XOOJ1c6e+MgYzICp372eeZvNJsXjuUQi4p"
  "kRnzx71Sq4Th17bT631N69eet2QZZVRTnEfP29ozd6IgYeyYd7lrdC13uT4G/tjT0Y1WTZ"
  "UBQYkLV7r6kOtTMS3EJOWBTpf3BwUFMdelCqHWRZBllTXU3zBMnS1uFwWK1Wj8dzVPMo1n"
  "hZNc1zF5yIHupfHJTCzx9eHDCq6v8bdPjL9250+L9/m3/71/0EbZkobA==";

/* 
 * Resource generated for file:
 *    SlicerCancelled.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerCancelled_width          = 21;
static const unsigned int  image_SlicerCancelled_height         = 21;
static const unsigned int  image_SlicerCancelled_pixel_size     = 3;
static const unsigned long image_SlicerCancelled_length         = 740;
static const unsigned long image_SlicerCancelled_decoded_length = 1323;

static const unsigned char image_SlicerCancelled[] = 
  "eNrdlOtLUwEYxv+OkG5mSkMmTkvTllOZl5IcJIWIFeYlqT7ZPkhYHyoJK2alWARhLFmktS"
  "6buGJp01GGlog1tBCk2jnbzrptri22PT0jIuKIfvfl8PDye37vt3MOsLrmxzc4bLjWgdYG"
  "NOoSyZ2EfMWZnsSFNlRrUV2Kmr8PdxJytsvM1ARaGlG5Ewcq0N6KQTPmXInkTkLOls6S8/"
  "ULTp9AaS7qqvDUCp8HkvdfDttQvz/R0qEpm5jFDI0KlUUYegyvBz0G1OpQth1NNXCOIBSC"
  "fSjRalQJUzb+Nr0vT/lTfyzuEdB1EYXZKMxCRQFuXEUkAsezuFdkS4em/NylK3FtU3wfME"
  "W9HmlvmS8/QyrJD/ZcjkfCTH9tFTlbOjTl58+ztrxQpQVnpn+Jwrvc9LniHG+3gbdM7iTk"
  "bOnQlJ8/KdcMKlM+3rkd9oiT+/YsdHfGIuGFK5fG1dnOzLRXu4vI2dKhKT93tBw3KTaONR"
  "8OiYI4Yo+Fw7NdBlueyqLcbM1Inek4GxLcbOnQlJ+/7TddT11n3KqctzyMR6Ofhu1WXXlf"
  "+qYHWvXE+TOLovD+fj9bOjTl54t+yXykzpCy9pa2YPaROSC4g6IQcH/+k657d3uLd7ClQ3"
  "PJF2feOXpTt+tcclJnpmKg4eBrY6976g2TOwk5WzrLvLYfxhzG+kMn1685teG/h4Sc7Yof"
  "TUCSXpr6jEeb2tU5+uQkJncS8lX2a/kNhRlGUw==";

/* 
 * Resource generated for file:
 *    SlicerCleanUp.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerCleanUp_width          = 21;
static const unsigned int  image_SlicerCleanUp_height         = 21;
static const unsigned int  image_SlicerCleanUp_pixel_size     = 3;
static const unsigned long image_SlicerCleanUp_length         = 620;
static const unsigned long image_SlicerCleanUp_decoded_length = 1323;

static const unsigned char image_SlicerCleanUp[] = 
  "eNqdVE1LAlEU1RHtDwVtWrWKDFq0iSByUasCoS9a1cZMIUxoYSBRqFAZZSRuqhGhqKZyUZ"
  "CLIYyKqLEvJaEW40xHH/N8OpbZ5XK5c945c9+7980YDGUzlozjOJPJZDabmzSzWCxAuJIZ"
  "dKb+12rKxfRjaFtw+PixOd7uTiAiX4sKdw+ZuvJw7NwbFCLx60vx5TX3KWUVRORAgO/sJX"
  "+RL4WPwrvivfRBHvNfao9HRiQGHKuhyHFNOepiVVHKLx/yy10uGZEiWAUnup+skuO82But"
  "S0p3OIpyRLoBsgdv8IT0AWMicvQKp6McKavaFmTnZuEgpSAiB0INzPXoKStHb9EfqoVkIi"
  "DfZJT3vIqIHAh9A5jORZ7dPKaDDpPV1cPCsF8OJIqlb58VRORAgBMCmOCzcsy3oHVtZEVG"
  "OddWhQMBTghggv9Tdev008C8NOitcCDA9dWNurP3z6baR8+s4xUOBDg9O/jF1mlytvMzy6"
  "lW24begdPOg8/K2blfpXO9k7GWbg/rQIBrcxfAZ+VVt+5CfLO74219vubOKUTkQOitA5NI"
  "WLn+zlcZufPgUMRY74vDjNgvjtZt9HsHQs5bV97o36b8r/ubs/YNve6Ygg==";

/* 
 * Resource generated for file:
 *    SlicerColors.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerColors_width          = 21;
static const unsigned int  image_SlicerColors_height         = 21;
static const unsigned int  image_SlicerColors_pixel_size     = 4;
static const unsigned long image_SlicerColors_length         = 284;
static const unsigned long image_SlicerColors_decoded_length = 1764;

static const unsigned char image_SlicerColors[] = 
  "eNpjYmL4z0QDTEVwhQ5m/oHSb8nEGGb++fMHjOXk5EjG+Mz8+/cvRA0DAxiD2DW308AYxJ"
  "5x5SoYg9ifPm0E44Ewk5Z+3++5D4xB7Ib+LWAMYmcA3ZgBdeejW0lgjM1MRkbqm0lFv0/D"
  "Zua/f//AarrVW8EYxPbqmgHGIHbs/nlgDGI/a0kB44EwkxZ+BwGQmij1LjAGu61uPxiD2A"
  "z1nGAMYj+wjQXjgTCTQr9foXJZ95aG5ecFqJl/sch9BeJfSPgPEfgf1Mx/VK6HwOYBAFie"
  "ew4=";

/* 
 * Resource generated for file:
 *    SlicerCycle.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerCycle_width          = 21;
static const unsigned int  image_SlicerCycle_height         = 21;
static const unsigned int  image_SlicerCycle_pixel_size     = 3;
static const unsigned long image_SlicerCycle_length         = 256;
static const unsigned long image_SlicerCycle_decoded_length = 1323;

static const unsigned char image_SlicerCycle[] = 
  "eNq9lMEJxCAUREX0YCfW4tkObMEubEGwgtRjBYI1iLA7IIRF0JX9ZMVDmJ8Xne8Yzhmnzd"
  "ev4yvOGCPimy+s8Ou6nHNa64ELIaCc4LVW7721NsaYcx44nqFAR3WP450QQu998g4FOqob"
  "vJSCVW52GtBR/XQx4Skl7HPTSVTRkxVujIHfDY4q+rnClVKttQ2OqpTyodWJ3omdJ577lL"
  "oROfg9T92deeR84OgV/B5m/vy6/eG+n/xtKPMNQkCuXQ==";

/* 
 * Resource generated for file:
 *    SlicerDecrement.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerDecrement_width          = 21;
static const unsigned int  image_SlicerDecrement_height         = 21;
static const unsigned int  image_SlicerDecrement_pixel_size     = 3;
static const unsigned long image_SlicerDecrement_length         = 48;
static const unsigned long image_SlicerDecrement_decoded_length = 1323;

static const unsigned char image_SlicerDecrement[] = 
  "eNpjYmJgogz9JxeMah/VDgcMuMFo0I1qp5Z2ShAA9hgdXQ==";

/* 
 * Resource generated for file:
 *    SlicerDelete.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerDelete_width          = 21;
static const unsigned int  image_SlicerDelete_height         = 21;
static const unsigned int  image_SlicerDelete_pixel_size     = 3;
static const unsigned long image_SlicerDelete_length         = 368;
static const unsigned long image_SlicerDelete_decoded_length = 1323;

static const unsigned char image_SlicerDelete[] = 
  "eNqlVEsOgkAMTfj/RAmgHtDEnefgBCaAa/caFxoXcAluAMcAfUmTCXaQCE6aZqb08Tqdto"
  "qiKz3RNNO2Pc9buq5vmg4shmE7zgJHXbfwVVUN2C3LpT0cXnMXMY5/HXdAbLK9bVvo8zEX"
  "enABi5vKdOMi/JETBmf/r+v6eX+QzrM8SZK+DzLcD34qO+BIPstG13VlWWIP3TRNmqakq6"
  "pi4QHOLHQUcIQNIGkZvlqFMvvv8DBco3hmw6Now959EjwIogF2kbqiGIf7fvBP6sCO9pnN"
  "jruj9Wazx/F2AP4zO95dTl35uW6XK+lTmslVx8oG7XnY7b8Ja15Q/z9taMRhlOEVUEW0QU"
  "4gOJJFTDYmb7DJTuA=";

/* 
 * Resource generated for file:
 *    SlicerDeleteDisabled.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerDeleteDisabled_width          = 21;
static const unsigned int  image_SlicerDeleteDisabled_height         = 21;
static const unsigned int  image_SlicerDeleteDisabled_pixel_size     = 3;
static const unsigned long image_SlicerDeleteDisabled_length         = 228;
static const unsigned long image_SlicerDeleteDisabled_decoded_length = 1323;

static const unsigned char image_SlicerDeleteDisabled[] = 
  "eNrNVLsOwyAQ+/8h/QaeH5EO3dl6LF0SpQorGzst6kkoJcmlCa1U62QJhLERHFovQylFDK"
  "d4HAUtb5oTFiFPqebzMcbEt6vN/Ik829FFyIv9Qwj3YUC2YI0x0zXqhW+55wDOOeTkCwDI"
  "3vsiXqUcH8NhuZTyf+Xj+NOzCyFq3DnnNe6MsRr3Rbl7R991yBZgM3xqz0t7XquieecXtw"
  "v5B8PW0/vxBFwcZXA=";



/* 
 * Resource generated for file:
 *    SlicerDone.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerDone_width          = 21;
static const unsigned int  image_SlicerDone_height         = 21;
static const unsigned int  image_SlicerDone_pixel_size     = 3;
static const unsigned long image_SlicerDone_length         = 840;
static const unsigned long image_SlicerDone_decoded_length = 1323;

static const unsigned char image_SlicerDone[] = 
  "eNr7/39YgW8/v1x9fHbXxTVLD0+eubtlyeFJOy6suvzo9NefnwnqffDq1sbTCydtr5myo3"
  "7azkYgAjKA3N4tZetOzL338gYevfdeXl9woLdrU3HflvIJW6sgqG9LRfemkrl7uuft7Zm5"
  "u/XOi6tY9X758Qno2sbVGS1rc9rW57VvyAeh9fmt63Jrl6Q9f/P42ZvHxXMj5x/o+fz9I6"
  "b2E7f3li2JqlgWW708sWZFEgwlVi1LaF9W+ub9q9Byx7A6+9x5/kdv7MTUPndfF1Aqb35Q"
  "wYLggoUhULQgOGO634u3z46c3e9fapk02TV5hsv0Xc2Y2suXRCdNdwbKpsxwTZ3pBkRARv"
  "I0l5bFhX///fXJsQxuNoueZBU10TJ/fjCm9uiJVhETLCL6LSqXxqdMd4uaYAlUGdxu8vr9"
  "y11HNjtmawa2GgO5QW0moe1mmNqzpvn7NRgev7ofyP77909av29Ak1HX0qrff347xRu6FW"
  "r71hn61Bh4V+un9vpgau9aWe5WpGMZq6LjIXPv8Z2Hz+45Zmm+//Ru8561FtHKroXa7sW6"
  "QAVA1La4FFP7jpNrnXO1bZPVjIMVrAL0/vz5ffTswd+/fxn5KlsnqjrlaDllawINBJLbjq"
  "/B1P7h87vamTmOmZq2SWoGvnJT5/cDBRetnmsYIG+bom6Xpm6XCkJVU7Pff36LNeWcvX48"
  "vS0U6ACzCCUNR8nihhwdVzkg2yZJzSYRhFKbQ85cP4Yn3Z65dqxiUqZVnIpxiIKet6xRkI"
  "JFjIpVvCoQVUzMBMoSzDXvP73dfHBVzZS8oAIHm3i10BKnumkFQBGg+PAqWv4DAHyUBkQ=";

/* 
 * Resource generated for file:
 *    SlicerError.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerError_width          = 21;
static const unsigned int  image_SlicerError_height         = 21;
static const unsigned int  image_SlicerError_pixel_size     = 3;
static const unsigned long image_SlicerError_length         = 664;
static const unsigned long image_SlicerError_decoded_length = 1323;

static const unsigned char image_SlicerError[] = 
  "eNrdlM9Lk3Ecx/+OWMv1OB97dD7Npzl/lGukS5YHh7fVobQyJfWiHqSsU2UTiSQiIaLSMa"
  "FgFDMqL7oadjAYgXkRL3WRZlniYYf4+PoKHeIRd9+Hhw9vXu/X5zk9m0hxzZ9NSb+TRzEZ"
  "vixdEbXJEHjB+fJZxkYkGpJoi5z795AhcNp9JrskA13SFpD2k9IelP7zMnFHes6qDIHT4u"
  "w5v37KzUFpqZNwvbQ2SOtxeftKcTYZAqfFwbTN31RSgpac8kmTT5r9EvLLbFIVbDIEThu0"
  "lGmbjZGhHw3m1omj+YBXAtXqVZMTqmCTA9VwWhxM+/lK5PSKv2KttvJ7nWe9vkq96vognE"
  "2GwGlxMO3nC8eMRat8yTqS9RnLNQbat96LcDYZAqfFwbSfvw8H35juObNs3qunvXrGqy9a"
  "+teeDnZml8BpcTDt5+mBvkTF4ZeVWtKjvfZoqSp3ynTPmmqTIXBaHEz7+fKLxKR+6HF5yV"
  "PD9dxwTRuu7Pho/vcme3qXwGlxMO3n2xu5ZHfnPbfzfpnzge58iDYTV6+diZMhcFoczD0/"
  "nLXMhyeRM7c0x6jmuFvqiJUefNYWZpMhcFqcfT7b1Y/pqUsXrpUcuOH674HAaQv+aLZyuU"
  "+J+NTVK7cba4c0B5sMgRfZX8sOp5MA4g==";

/* 
 * Resource generated for file:
 *    SlicerGlyph.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGlyph_width          = 21;
static const unsigned int  image_SlicerGlyph_height         = 21;
static const unsigned int  image_SlicerGlyph_pixel_size     = 3;
static const unsigned long image_SlicerGlyph_length         = 376;
static const unsigned long image_SlicerGlyph_decoded_length = 1323;

static const unsigned char image_SlicerGlyph[] = 
  "eNrNlF0KgzAQhK2iPUgv0jP1AH3ukXogEYqIICKCSPvVwTUkFkLpQ5ewrJOd7Gx+TJLNDo"
  "ulaZplWZ7nx9WKogBJF0sCe35r/0yf5/lyu+Pj6S5lHEdi/KelQvo0TX3fn85XxjAMXdfh"
  "9QnOrJvMMYV0qlg+sa1GHENv27YsS/JVGk8MAu7RPfFqkEFm0zTSjycG0ZTbvtFFtDY1wA"
  "HxLkiOLWJ0VEmnSjwWk2a8PjWljtTF+56v1bVCXddkIhUWMbLxxCDgxOKq+mGlW+/SJp0S"
  "o63TzjPr9u7RbeeNbjsvAf7B7dGpUlUVOu3ciUF2zn2P3i/GXVUjeGKBIT2887YzsFBud9"
  "7w+BcXUn7+3rd/Xdxw7QV0tcbp";

/* 
 * Resource generated for file:
 *    SlicerGoToEnd.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoToEnd_width          = 21;
static const unsigned int  image_SlicerGoToEnd_height         = 21;
static const unsigned int  image_SlicerGoToEnd_pixel_size     = 3;
static const unsigned long image_SlicerGoToEnd_length         = 156;
static const unsigned long image_SlicerGoToEnd_decoded_length = 1323;

static const unsigned char image_SlicerGoToEnd[] = 
  "eNpjYKAU/CcXDD/te3bt2rdnDxBBuHAGkdqNjIxiwQDO3bl9O0na586ejawdyN6xbRvZ2u"
  "eAuZgmEKkdyJ2LzQSStEMEgVyybUfWS5J2INfY2JjskEezlyTtoaGhJMU7BEC4lpaWIyfH"
  "kaqdEgAAi3O8Aw==";

/* 
 * Resource generated for file:
 *    SlicerGoToFirst.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoToFirst_width          = 21;
static const unsigned int  image_SlicerGoToFirst_height         = 21;
static const unsigned int  image_SlicerGoToFirst_pixel_size     = 3;
static const unsigned long image_SlicerGoToFirst_length         = 160;
static const unsigned long image_SlicerGoToFirst_decoded_length = 1323;

static const unsigned char image_SlicerGoToFirst[] = 
  "eNpjYmJgogz9JxfQU/uXL1+I0c7AgMXAU6dOMYABfu1ABU+ePEFTBhHBFCdGO9DNQO706d"
  "PJ0P7nz5/i4uKGhoYnYECq9ra2NogIGdqBDkbWS5L2X79+oemlp+2U+53ykKcw3klNdRSm"
  "ecpzHJ2LC0oQALdvjEA=";

/* 
 * Resource generated for file:
 *    SlicerGoToLast.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoToLast_width          = 21;
static const unsigned int  image_SlicerGoToLast_height         = 21;
static const unsigned int  image_SlicerGoToLast_pixel_size     = 3;
static const unsigned long image_SlicerGoToLast_length         = 152;
static const unsigned long image_SlicerGoToLast_decoded_length = 1323;

static const unsigned char image_SlicerGoToLast[] = 
  "eNpjYmJgogz9JxfQQvuXL1/I1s4ABqdOncJUDBQnRvuTJ08gJFZxYrRPnz4dyED2BUnaga"
  "ChoaG4uPjPnz/kaYcobmtrI1s7RD3QI2Rrh2j59esX/W2nxO+UhDyF8U5JqqMkzVOY4+hf"
  "XFCCANRnjEA=";

/* 
 * Resource generated for file:
 *    SlicerGoToStart.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoToStart_width          = 21;
static const unsigned int  image_SlicerGoToStart_height         = 21;
static const unsigned int  image_SlicerGoToStart_pixel_size     = 3;
static const unsigned long image_SlicerGoToStart_length         = 164;
static const unsigned long image_SlicerGoToStart_decoded_length = 1323;

static const unsigned char image_SlicerGoToStart[] = 
  "eNrlkzEOgCAMRTkTTByEDU+GXAOUEK+mjU2axsVSNn3Tb/IflAFjZjm1fFg/esfQW4NMo0"
  "Tfa7XWYl5uaHzVt1J4H3JOSaiju7K+XEcXynlchwK5WXU7P0H3dufcoz+k0w5qHQghUD/G"
  "OKoD3ntaBvnPjxPqM1wzgrwA";

/* 
 * Resource generated for file:
 *    SlicerGoing0.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing0_width          = 21;
static const unsigned int  image_SlicerGoing0_height         = 21;
static const unsigned int  image_SlicerGoing0_pixel_size     = 3;
static const unsigned long image_SlicerGoing0_length         = 584;
static const unsigned long image_SlicerGoing0_decoded_length = 1323;

static const unsigned char image_SlicerGoing0[] = 
  "eNr7/3+Qgjlz5mhqak6ePJk87ebm5uv2PldWViZS/YYNG0JCQlauXAnhLliwwMDAYNq0aU"
  "Rqj4+P33fqjb+/P3mu3bx5c2Rk5OrVq58+e7502aqyirrouNTyqoaly9c8efqcSEOAeju7"
  "+kuq+6csOLBq10MgWVo7sbt3CpEmLFm2srCyd8WOR6t3PwOiVbueLdv+sKhq4pLla3BpOX"
  "LkSH19/b59+4DsotLqnll7lmx7AkeLtz7unrmntKIBl/b29vZz1z9WV1cD2cFhsbPX3pm3"
  "8dHcDQ9mr7s3c/Xt6SuvT1p8ISwqCZf2Y8eONTc3HzhwAMguKCqvaFnaOnVf44QddX1ba3"
  "s21/VuKaydk19UTozfFy5eGplQkF8zL6t8RnrJlPTiyRml08Jjc+cvXEpUyD991tTcFhie"
  "EhpXEpFYGxxd6Bec0NjU9vjxU/war1+/DkxyV65cefLk6bz5izKzcj28/IDk3PkLHz1+Qt"
  "De9evX33/6dfny5eSlups3b65Zs+bq1asQ7sOHD4FRee/ePfJMO3z48PPXP/bu3Uue9keP"
  "HgGj8v79+4O1QPoPAIN5mzs=";

/* 
 * Resource generated for file:
 *    SlicerGoing1.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing1_width          = 21;
static const unsigned int  image_SlicerGoing1_height         = 21;
static const unsigned int  image_SlicerGoing1_pixel_size     = 3;
static const unsigned long image_SlicerGoing1_length         = 588;
static const unsigned long image_SlicerGoing1_decoded_length = 1323;

static const unsigned char image_SlicerGoing1[] = 
  "eNr7/3+Qgg0bNoSEhKxcuZI87fHx8ftOvfH39ydS/ZEjR+rr6/ft2wfhbt68OTIycvXq1R"
  "DunDlzNDU1J0+ejEt7e3v7uesfq6urscqam5uv2/tcWVkZl/Zjx441NzcfOHDg8ZNnCxat"
  "LCqtiYhJKS6rn79o1cPHzxYsWGBgYDBt2jT8XgDqbW3vyy/vnTB3/8qdD4FkQWV/a+ekh4"
  "+eERMC8xetyCntXr790erdz4Bo1a5nS7c9zCnrn7toNS4t169fB0bQlStXgOy8osr2abuX"
  "bHsCR4u3Pm6buju/uA6X9vXr199/+nX58uVAdkBwzKw1d+ZtfDR3w4PZ6+7NXH17+srrEx"
  "ddCAxLwKX95s2ba9asuXr1KpCdnVdaVL+odeq+xgk76vq21vZsruvdkl0+IzOnhBi/z563"
  "KCAiO696blb5jPSSKenFk9NLpvqGpE+fuYAY7Y8ePamubXLzjfULzwtPqPEJyXLxiqysar"
  "h//xF+jQ8fPgQmuXv37j14+Gj6jDlJyRnOLp6JyelTps26e+8BQXsPHz78/PWPvXv3kpdH"
  "Hj16BExy9+/f/z8KiAAAKtqYng==";

/* 
 * Resource generated for file:
 *    SlicerGoing2.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing2_width          = 21;
static const unsigned int  image_SlicerGoing2_height         = 21;
static const unsigned int  image_SlicerGoing2_pixel_size     = 3;
static const unsigned long image_SlicerGoing2_length         = 584;
static const unsigned long image_SlicerGoing2_decoded_length = 1323;

static const unsigned char image_SlicerGoing2[] = 
  "eNr7/3+QgiNHjtTX1+/bt4887e3t7eeuf6yuriZS/fXr11euXHnlyhUI99ixY83NzQcOHI"
  "BwN2zYEBISAlSAS/v69evvP/26fPlyrLLx8fH7Tr3x9/fHpf3mzZtr1qy5evXq02fPly5b"
  "VVZRFx2XWl7VsHT5midPn2/evDkyMnL16tX4vQDU29nVX1LdP2XBgVW7HgLJ0tqJ3b1TgC"
  "YQEwJLlq0srOxdsePR6t3PgGjVrmfLtj8sqpq4ZPkaXFoePnwIjKB79+4B2UWl1T2z9izZ"
  "9gSOFm993D1zT2lFA1B2zpw5mpqakydPRtZ++PDh569/7N27F8gODoudvfbOvI2P5m54MH"
  "vdvZmrb09feX3S4gthUUlAWXNz83V7nysrKyNrf/ToETCC7t+/D2QXFJVXtCxtnbqvccKO"
  "ur6ttT2b63q3FNbOyS8qB8ouWLDAwMBg2rRpuDyycPHSyISC/Jp5WeUz0kumpBdPziidFh"
  "6bO3/hUmKC7unTZ03NbYHhKaFxJRGJtcHRhX7BCY1NbY8fPyUy+T158nTe/EWZWbkeXn5A"
  "cu78hY8eP/k/CgYTAAATXJs7";

/* 
 * Resource generated for file:
 *    SlicerGoing3.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing3_width          = 21;
static const unsigned int  image_SlicerGoing3_height         = 21;
static const unsigned int  image_SlicerGoing3_pixel_size     = 3;
static const unsigned long image_SlicerGoing3_length         = 584;
static const unsigned long image_SlicerGoing3_decoded_length = 1323;

static const unsigned char image_SlicerGoing3[] = 
  "eNr7/3+QguvXr69cufLKlSvkaV+/fv39p1+XL19OpPqHDx/u27fv3r17EO7NmzfXrFlz9e"
  "pVCPfIkSP19fVABbi0Hz58+PnrH3v37sUq297efu76x+rqalzaHz16dODAgfv37z999nzp"
  "slVlFXXRcanlVQ1Ll6958vT5sWPHmpubgQrwewGot7Orv6S6f8qCA6t2PQSSpbUTu3unAE"
  "0gJgSWLFtZWNm7Ysej1bufAdGqXc+WbX9YVDVxyfI1xGgvKq3umbVnybYncLR46+PumXtK"
  "KxqAshs2bAgJCQFGKC7twWGxs9fembfx0dwND2avuzdz9e3pK69PWnwhLCoJKBsfH7/v1B"
  "t/f39c2guKyitalrZO3dc4YUdd39bans11vVsKa+fkF5UDZTdv3hwZGbl69Wpc2hcuXhqZ"
  "UJBfMy+rfEZ6yZT04skZpdPCY3PnL1xKjN+fPn3W1NwWGJ4SGlcSkVgbHF3oF5zQ2NT2+P"
  "FTIpPfkydP581flJmV6+HlByTnzl/46PGTOXPmaGpqTp48mbwsYG5uvm7vc2VlZfK0L1iw"
  "wMDAYNq0af9HARgAAK8amzs=";

/* 
 * Resource generated for file:
 *    SlicerGoing4.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing4_width          = 21;
static const unsigned int  image_SlicerGoing4_height         = 21;
static const unsigned int  image_SlicerGoing4_pixel_size     = 3;
static const unsigned long image_SlicerGoing4_length         = 584;
static const unsigned long image_SlicerGoing4_decoded_length = 1323;

static const unsigned char image_SlicerGoing4[] = 
  "eNr7/3+QgocPH+7bt+/evXvkaT98+PDz1z/27t1LnvZHjx4dOHDg/v37EO7169dXrlx55c"
  "oV8kxbv379/adfly9fTlDl02fPly5bVVZRFx2XWl7VsHT5midPn9+8eXPNmjVXr14lqLez"
  "q7+kun/KggOrdj0EkqW1E7t7pwBNIMaRS5atLKzsXbHj0erdz4Bo1a5ny7Y/LKqauGT5Gm"
  "K0F5VW98zas2TbEzhavPVx98w9pRUNQNkjR47U19cDIxSX9uCw2Nlr78zb+Gjuhgez192b"
  "ufr29JXXJy2+EBaVBJRtb28/d/1jdXU1Lu0FReUVLUtbp+5rnLCjrm9rbc/mut4thbVz8o"
  "vKgbLHjh1rbm4GRigu7QsXL41MKMivmZdVPiO9ZEp68eSM0mnhsbnzFy4lxu9Pnz5ram4L"
  "DE8JjSuJSKwNji70C05obGp7/PgpkSnkyZOn8+YvyszK9fDyA5Jz5y989PjJhg0bQkJCgA"
  "mPvFQXHx+/79Qbf39/ItXPmTNHU1Nz8uTJEO7mzZsjIyNXr15NpHZzc/N1e58rKyuT59oF"
  "CxYYGBhMmzZtsBZI/wHykJs7";

/* 
 * Resource generated for file:
 *    SlicerGoing5.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing5_width          = 21;
static const unsigned int  image_SlicerGoing5_height         = 21;
static const unsigned int  image_SlicerGoing5_pixel_size     = 3;
static const unsigned long image_SlicerGoing5_length         = 580;
static const unsigned long image_SlicerGoing5_decoded_length = 1323;

static const unsigned char image_SlicerGoing5[] = 
  "eNr7/38UEAYPHz7ct2/fvXv3yNN++PDh569/7N27l6DKp8+eL122qqyiLjoutbyqYenyNU"
  "+ePn/06NGBAwfu379PUG9nV39Jdf+UBQdW7XoIJEtrJ3b3TgGaQIwjlyxbWVjZu2LHo9W7"
  "nwHRql3Plm1/WFQ1ccnyNcRoLyqt7pm1Z8m2J3C0eOvj7pl7SisagLLXr19fuXLllStXcG"
  "kPDoudvfbOvI2P5m54MHvdvZmrb09feX3S4gthUUlA2fXr199/+nX58uW4tBcUlVe0LG2d"
  "uq9xwo66vq21PZvrercU1s7JLyoHyt68eXPNmjVXr17FpX3h4qWRCQX5NfOyymekl0xJL5"
  "6cUTotPDZ3/sKlxPj96dNnTc1tgeEpoXElEYm1wdGFfsEJjU1tjx8/xa9xzpw5mpqakydP"
  "fvLk6bz5izKzcj28/IDk3PkLHz1+cuTIkfr6emDCw6Xd3Nx83d7nysrKWGXb29vPXf9YXV"
  "2NS/uCBQsMDAymTZsG4W7YsCEkJAQYTRDusWPHmpubgQmPyEQeHx+/79Qbf39/8vLI5s2b"
  "IyMjV69ePWgLAQAgp5s7";

/* 
 * Resource generated for file:
 *    SlicerGoing6.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing6_width          = 21;
static const unsigned int  image_SlicerGoing6_height         = 21;
static const unsigned int  image_SlicerGoing6_pixel_size     = 3;
static const unsigned long image_SlicerGoing6_length         = 576;
static const unsigned long image_SlicerGoing6_decoded_length = 1323;

static const unsigned char image_SlicerGoing6[] = 
  "eNr7/38UDCLw9NnzpctWlVXURcelllc1LF2+5snT58Tr7ezqL6nun7LgwKpdD4Fkae3E7t"
  "4pRJqwZNnKwsreFTserd79DIhW7Xq2bPvDoqqJS5avwaVlzpw5mpqakydPBrKLSqt7Zu1Z"
  "su0JHC3e+rh75p7Sigag7MOHD/ft23fv3j1k7ebm5uv2PldWVgayg8NiZ6+9M2/jo7kbHs"
  "xed2/m6tvTV16ftPhCWFQSUPbw4cPPX//Yu3cvsvYFCxYYGBhMmzYNyC4oKq9oWdo6dV/j"
  "hB11fVtrezbX9W4prJ2TX1QOlH306NGBAwfu37+PyyMLFy+NTCjIr5mXVT4jvWRKevHkjN"
  "Jp4bG58xcuJSrknz5ram4LDE8JjSuJSKwNji70C05obGp7/Pgpfo0bNmwICQlZuXLlkydP"
  "581flJmV6+HlByTnzl/46PGT69evA6WuXLmCS3t8fPy+U2/8/f2xyq5fv/7+06/Lly/HpX"
  "3z5s2RkZGrV6+GcI8cOVJfXw+MJgj35s2ba9asuXr1KpHJr729/dz1j9XV1eSl/GPHjjU3"
  "NwOjadDmTQBivJs7";

/* 
 * Resource generated for file:
 *    SlicerGoing7.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGoing7_width          = 21;
static const unsigned int  image_SlicerGoing7_height         = 21;
static const unsigned int  image_SlicerGoing7_pixel_size     = 3;
static const unsigned long image_SlicerGoing7_length         = 580;
static const unsigned long image_SlicerGoing7_decoded_length = 1323;

static const unsigned char image_SlicerGoing7[] = 
  "eNr7/38UgMCcOXM0NTUnT55MnnZzc/N1e58rKyuTp33BggUGBgbTpk17+uz50mWryirqou"
  "NSy6sali5f8+TpcyINAert7Oovqe6fsuDAql0PgWRp7cTu3ilEmrBk2crCyt4VOx6t3v0M"
  "iFbterZs+8OiqolLlq/BpWXDhg0hISErV64EsotKq3tm7Vmy7QkcLd76uHvmntKKBlza4+"
  "Pj95164+/vD2QHh8XOXntn3sZHczc8mL3u3szVt6evvD5p8YWwqCRc2jdv3hwZGbl69Wog"
  "u6CovKJlaevUfY0TdtT1ba3t2VzXu6Wwdk5+UTkxfl+4eGlkQkF+zbys8hnpJVPSiydnlE"
  "4Lj82dv3ApUSH/9FlTc1tgeEpoXElEYm1wdKFfcEJjU9vjx0/xazxy5Eh9ff2+ffuePHk6"
  "b/6izKxcDy8/IDl3/sJHj588fPgQKHXv3j1c2tvb289d/1hdXY1V9vDhw89f/9i7dy8u7c"
  "eOHWtubj5w4ACEe/36dWBUXrlyBcJ99OgRUOr+/ftEJr/169fff/p1+fLl5GWBmzdvrlmz"
  "5urVq4O2iAAAxu+bOw==";

/* 
 * Resource generated for file:
 *    SlicerIncrement.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerIncrement_width          = 21;
static const unsigned int  image_SlicerIncrement_height         = 21;
static const unsigned int  image_SlicerIncrement_pixel_size     = 3;
static const unsigned long image_SlicerIncrement_length         = 52;
static const unsigned long image_SlicerIncrement_decoded_length = 1323;

static const unsigned char image_SlicerIncrement[] = 
  "eNpjYmJgogz9JxeMasejgIGBYURpZ8ANRoNuNMtQSzslCAAglQV1";

/* 
 * Resource generated for file:
 *    SlicerInformation.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerInformation_width          = 21;
static const unsigned int  image_SlicerInformation_height         = 21;
static const unsigned int  image_SlicerInformation_pixel_size     = 3;
static const unsigned long image_SlicerInformation_length         = 148;
static const unsigned long image_SlicerInformation_decoded_length = 1323;

static const unsigned char image_SlicerInformation[] = 
  "eNpjYKAU/CcXDCrtpHoWmUu8M5C14NLu0r8fjUTWvmXLFkpsJ6h9YG1nZGSEC6JJ0cJ24u"
  "O9r6/v9OnTmNohDoaI9+EGp8EAl3a4+XgA1kQL1I7sNlLTPFA78XoxtZOkF1M7SXqpld8p"
  "AQDMEXbq";

/* 
 * Resource generated for file:
 *    SlicerInvisible.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerInvisible_width          = 21;
static const unsigned int  image_SlicerInvisible_height         = 21;
static const unsigned int  image_SlicerInvisible_pixel_size     = 3;
static const unsigned long image_SlicerInvisible_length         = 372;
static const unsigned long image_SlicerInvisible_decoded_length = 1323;

static const unsigned char image_SlicerInvisible[] = 
  "eNpjYWJhoQz9+feHPERb7b/AiHTtb9692bJpy5xZc6ZMm7Jm1Zo79+4Qrx2oGKhlz749Tx"
  "49AZpz5tSZJcuWAElitAPVAy0FakTzBdAxmG4gPuju3LqzZ9ceCPry6QvVQ76nqwfCBXoc"
  "UyXQ7/B0cuXSFVy2A/2oo6fz5dsXNO01dTUlRSUE/P7rD9BqFzeXnKwceOiBAnPeHB8vn5"
  "a2FmAI4NEOjGVgyAAZQItExERCgkJSklKAjlFRUwFqBIYYMFKQHYai/defI4eOwKMJyAbq"
  "Bbpky7YtSxZB4x1oCNAx+CMOqBHoWhDj2BFIML559QboMGJTHdCKV28g5gDTG1wQM/0Tjv"
  "dfA5dh6aKdEgQAMlPcrA==";

/* 
 * Resource generated for file:
 *    SlicerLink.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerLink_width          = 21;
static const unsigned int  image_SlicerLink_height         = 21;
static const unsigned int  image_SlicerLink_pixel_size     = 3;
static const unsigned long image_SlicerLink_length         = 296;
static const unsigned long image_SlicerLink_decoded_length = 1323;

static const unsigned char image_SlicerLink[] = 
  "eNq9k9EJwzAMREvW6Y/3cH6cEbxM8B7OBJ4jEwSySvtAINy6kUNDa4wQUk66k6NhuA3X7u"
  "Pb81P4uq4ppWmanHNYfCIn4aWUZVnur2ccR+JdOF3Ait22bd93rFTz3iuHI7jw5HuAGsSX"
  "CmSP4KoXp9ULB+DEP8LRhTqRid/qhQARJtnC6YIusjCkfs4ZG0Ko9Rrd4SlYWqgfY1S9tn"
  "ZqkqJ+zYTuysSePIrI6qi7czC6n3kFQ/vbW0sd+5+vJ1//afM8S4XuytR69YCt9dor092y"
  "v+37SfiV+wSJk0xc";

/* 
 * Resource generated for file:
 *    SlicerDownload.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerDownload_width          = 21;
static const unsigned int  image_SlicerDownload_height         = 21;
static const unsigned int  image_SlicerDownload_pixel_size     = 3;
static const unsigned long image_SlicerDownload_length         = 296;
static const unsigned long image_SlicerDownload_decoded_length = 1323;

static const unsigned char image_SlicerDownload[] = 
  "eNr7/5+aoDaCAT/Cr70znqErjaE7naErgQHIxkT4tc/KYIaiPCQSSfA/LUF/iVVflU1fOQ"
  "oCCcLY+LXPbfMEonkt7mgILohf+5pJoWsnhUDQuqmhcAQUXz85eM2UUJr6/dS6zBNrsk6s"
  "zDq+Jufkhuzj63OAXCADKA5kAxF+7Ze3F1/eVQwhr2wphHIhItuLL+4owa/91r5K/Og/7c"
  "G/f/+YmBgiJ1pAEJANFyfSBLj2KCTtxAOstpOqERkRbwimCWpqqngCihLH//nzB5cbyAs3"
  "OODi4sCvAAB0aTI1";

/* 
 * Resource generated for file:
 *    SlicerUpload.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerUpload_width          = 21;
static const unsigned int  image_SlicerUpload_height         = 21;
static const unsigned int  image_SlicerUpload_pixel_size     = 3;
static const unsigned long image_SlicerUpload_length         = 292;
static const unsigned long image_SlicerUpload_decoded_length = 1323;

static const unsigned char image_SlicerUpload[] = 
  "eNr7/5+a4Na+SvwIv/bL24sv7yqGkFe2FEK5EJHtxRd3lODXfmpd5ok1WSdWZh1fk3NyQ/"
  "bx9TlALpABFAeygeg/LcGaSaFrJ4VA0LqpoXAEFF8/OXjNlFD82ue2eQLRvBZ3NAQXxK+9"
  "v8Sqr8qmrxwFgQRhbJr6fVYGMxTlIZFIgvi1d8YzdKUxdKczdCUwANmYCL/22ggG/Og/HQ"
  "EXFwfZepmYGKImWgBJ8vRGTrSAIFJNQNYLQWpqqmTrJcMNxDv+379/eLQTDL0/f/5gGojV"
  "dkyLAFqzMjU=";


/* 
 * Resource generated for file:
 *    SlicerLoad.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerLoad_width          = 21;
static const unsigned int  image_SlicerLoad_height         = 21;
static const unsigned int  image_SlicerLoad_pixel_size     = 3;
static const unsigned long image_SlicerLoad_length         = 332;
static const unsigned long image_SlicerLoad_decoded_length = 1323;

static const unsigned char image_SlicerLoad[] = 
  "eNpjYmJgogx9/fr1z58//0kHEO2Y4tzcnMjcv3///gMDCBfOxqodKBI10QKrsQRtB3IjJ1"
  "pAEEET0LQj64UgNTVVIrVj6iXoBuo6nnLteJRhjSMg+vXrF7IaTPT+agMEYToVWTtcGS4E"
  "146ZbCDaz27LdbFVwYWAajw83IEMoDKs2ovTbTfOi/v18RoeBFGDqf3OkbIQb52vL/Z8f7"
  "kXF3r7YAdQze/fvzG1T2ryA6LPDxbhQRA1WFNdoIf2/ZONH2504UIvLrYB1bx+/Rqr9soc"
  "B/whv3RyBFANrjRPEM1oD7xw4QKpqQ6t3ICXGEwUF3QAugMjww==";

/* 
 * Resource generated for file:
 *    SlicerNext.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerNext_width          = 21;
static const unsigned int  image_SlicerNext_height         = 21;
static const unsigned int  image_SlicerNext_pixel_size     = 3;
static const unsigned long image_SlicerNext_length         = 132;
static const unsigned long image_SlicerNext_decoded_length = 1323;

static const unsigned char image_SlicerNext[] = 
  "eNpjYKAU/CcXDFft+/bsoUS7kZHRzu3bKdEeGxu7Y9s2srXPmT0bvwn4tc+dPXsuXhOI0Q"
  "4xAcgl23aseonRDtRrbGxMdsjjspcY7aGhoWTHu6Wl5YjNccRrpwQAAA7f934=";

/* 
 * Resource generated for file:
 *    SlicerPause.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerPause_width          = 21;
static const unsigned int  image_SlicerPause_height         = 21;
static const unsigned int  image_SlicerPause_pixel_size     = 3;
static const unsigned long image_SlicerPause_length         = 76;
static const unsigned long image_SlicerPause_decoded_length = 1323;

static const unsigned char image_SlicerPause[] = 
  "eNpjYmJgogz9JxcMTu0MDAwERXBpB6q8f/8+snpMkVHtmNp//fo1GnQDpZ3CND+08jslCA"
  "AGnaB0";

/* 
 * Resource generated for file:
 *    SlicerPingPong.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerPingPong_width          = 21;
static const unsigned int  image_SlicerPingPong_height         = 21;
static const unsigned int  image_SlicerPingPong_pixel_size     = 3;
static const unsigned long image_SlicerPingPong_length         = 88;
static const unsigned long image_SlicerPingPong_decoded_length = 1323;

static const unsigned char image_SlicerPingPong[] = 
  "eNpjYmJgogz9JxeMaqeKdgYGYs2Bq8SqHZc5mAowtTOAAS7tcDVYtRMDBAQEaGQ75X4nO+"
  "RH0zz9tVOCAI4Lw9g=";

/* 
 * Resource generated for file:
 *    SlicerPlayBackward.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerPlayBackward_width          = 21;
static const unsigned int  image_SlicerPlayBackward_height         = 21;
static const unsigned int  image_SlicerPlayBackward_pixel_size     = 3;
static const unsigned long image_SlicerPlayBackward_length         = 144;
static const unsigned long image_SlicerPlayBackward_decoded_length = 1323;

static const unsigned char image_SlicerPlayBackward[] = 
  "eNq91LENwCAMBEDEyizgihXYyXPYG1BaSizSROn4V7DcnnjAUGupXF9oneFzTpiralkFcD"
  "NL6O4Az8ypxhgAj4jWmoj4ql3ee39iAzwDv+0u/9jDq5N750+evHd+6viZ51/c398F0zdy"
  "3NfT";

/* 
 * Resource generated for file:
 *    SlicerPlayForward.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerPlayForward_width          = 21;
static const unsigned int  image_SlicerPlayForward_height         = 21;
static const unsigned int  image_SlicerPlayForward_pixel_size     = 3;
static const unsigned long image_SlicerPlayForward_length         = 144;
static const unsigned long image_SlicerPlayForward_decoded_length = 1323;

static const unsigned char image_SlicerPlayForward[] = 
  "eNpjYmJgogz9JxfQVPuXL1/I1s4ABqdOnSJb+9OnT4HkkydPyNY+ffp0IAOXLwhqB4KGho"
  "bi4uI/f/6Qpx3ihba2NrK1Q0wAeoRs7RATBsR2SvxOSchTGO+UpDpK0jyFOY5uxQUlCAAt"
  "ZdfT";

/* 
 * Resource generated for file:
 *    SlicerPreparing.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerPreparing_width          = 21;
static const unsigned int  image_SlicerPreparing_height         = 21;
static const unsigned int  image_SlicerPreparing_pixel_size     = 3;
static const unsigned long image_SlicerPreparing_length         = 164;
static const unsigned long image_SlicerPreparing_decoded_length = 1323;

static const unsigned char image_SlicerPreparing[] = 
  "eNr7/38UjAJKwbFjx/r7+0+fPg1kr1+/PiEhYdu2bUD25MmTNTU158yZg197d3f3lTufp0"
  "2bBmRHR0cfPvc2KysLyFZWVl6397m5uTl+7SdOnJg0adLZs2eB7I0bN6ampu7YsQPIBhpo"
  "YGCwYMGC0QgaBZQDABw9A20=";

/* 
 * Resource generated for file:
 *    SlicerPrevious.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerPrevious_width          = 21;
static const unsigned int  image_SlicerPrevious_height         = 21;
static const unsigned int  image_SlicerPrevious_pixel_size     = 3;
static const unsigned long image_SlicerPrevious_length         = 136;
static const unsigned long image_SlicerPrevious_decoded_length = 1323;

static const unsigned char image_SlicerPrevious[] = 
  "eNpjYKAU/CcXjATt+/bsIVv7zu3bjYyMyNO+Y9u22NhY8rRD9M6ZPZsM7RC9c2fPnku6dq"
  "B6uN65ZNmObAJ5fjc2NoaYQHbIQ9xAtnYgCA0NpUQ7EFhaWo7YHIdfOyUAAOf5934=";

/* 
 * Resource generated for file:
 *    SlicerReDo.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerReDo_width          = 21;
static const unsigned int  image_SlicerReDo_height         = 21;
static const unsigned int  image_SlicerReDo_pixel_size     = 3;
static const unsigned long image_SlicerReDo_length         = 384;
static const unsigned long image_SlicerReDo_decoded_length = 1323;

static const unsigned char image_SlicerReDo[] = 
  "eNpjYmJgogz9JxfQTXvE8SpKtPOusQ0/Vkm2dpaVZkwrTeNO1uHX/v7Xp45rC/LP9YQfq4"
  "g8VhV5vApIRhyrZFxhwrDCGIjSTrfi0n7/y1P+tfYQZXiQ3+FCrNrjT9QDZXUWBiqFmKuG"
  "WQJJ5VAL1XArzShbZO2aqQ6Y2o++vsi12lp4ndPRS6cw/Q7RyL7KUjfGEavjlz3cAVQQc7"
  "wWa9ABpYDe14txwhV0E28uB6rJO9eNS7tSsDmeiJt0awVQTcG5XqzaMfWiaV90fytQe+LJ"
  "RvIS7bl3N0Chui2E7DRv1RwKNKHxyqwocGrBRPi1n39/U7zDBldqAYY8wTQPNEGv3ks+wU"
  "Ihwlwx1BwYYsDEA0V4Q36gigtKEAA1jXjX";

/* 
 * Resource generated for file:
 *    SlicerRecord.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerRecord_width          = 21;
static const unsigned int  image_SlicerRecord_height         = 21;
static const unsigned int  image_SlicerRecord_pixel_size     = 3;
static const unsigned long image_SlicerRecord_length         = 120;
static const unsigned long image_SlicerRecord_decoded_length = 1323;

static const unsigned char image_SlicerRecord[] = 
  "eNpjYmJgogz9JxeMaqeF9qlTp7q6unJxcQHJKVOmkqQdqJdBU5phYiQUaUpjNQGXdqCNCL"
  "1gBBQhXjvQzWjagSJ0s51CvwPBFApCfjTN00c7JQgAD+ba2w==";


/* 
 * Resource generated for file:
 *    SlicerRefresh.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerRefresh_width          = 21;
static const unsigned int  image_SlicerRefresh_height         = 21;
static const unsigned int  image_SlicerRefresh_pixel_size     = 3;
static const unsigned long image_SlicerRefresh_length         = 1052;
static const unsigned long image_SlicerRefresh_decoded_length = 1323;

static const unsigned char image_SlicerRefresh[] = 
  "eNpjYKAU/EcF/zDAfxwATTtQ5d+//x6//brl4tMJu2+UrDr368/fjBk7uzaf3XHl2bMP3/"
  "BoB+r9/efvzivPqtacT5x7PHHO8cTZxyFSHqWTY6btrVl/Yd+NF1i1Q+xdfepB4pyjibOP"
  "Td554+C1F8/ef4ObnNY6J3rK7sR5Rzecf4SpHah35+Un8TMP5S48se3CEyAXzZ3PX7zIbZ"
  "6aNmtf/OxDe64+RdP+5N2X8uUnYqfu33oWavjOA8erO6bCbTcwNk3LLyubtDxiwraKFSeA"
  "6pG1bzn/IHrqnonbLkPs7ZyyQMsu2DK8DBLy/OLyhjbula0TZq/ZmTd5XfS0PUD1yNp7tp"
  "yLmrTzANhVO/cf07IPdsnsjenf9+fPX3XbAHVrf9fM7tYFOxduPtQ0e11A81KgeqCxcO05"
  "8/cBXfX0LchJVe2TzcOKYibsiZmw169qvlV0uXNmV3jHppqlJ5bvPr1g80G3oglA9cjaIy"
  "dsCu/f/AfscvfITNe8/qgJO4EotGN9QMPS0PZ1QHbmzP2bj1/feuScXVo9UD2y47Nm7wjr"
  "2/j07Wcgu7J9YkDTkoi+LWiodc2JEzeebDty1jy6EKgeWXvn+mMhPWv3XLwPZO84cMQ1rz"
  "O0Z31oNxSF9WyI6N24/uStW49fzlmzzTQiF6geWfum0zeDO1d1rTsKDCsgt3/hmtDOVcFd"
  "q0Goc3VY95rpO848f/f56au32fU9VgnlQPXI2h+//pA/a0tg+/KVe09C4vrguatTt53Mnb"
  "1t0pYTey/de/v52+v3n6YsXKnhHBxTPxWoHi3Nbz97K7B1acKEtUu2AOPrD0Tw79+/wIzw"
  "7cfPj5+/Tlu8yiow3iq+DKgSa45beeiif/Mi75pZ5f1zV27afuf+o1+/ft+6+2DFxm2ZlU"
  "0qNl7AQFuw/RhcPSMjI1p+3376Ru609X6NCxwym4yC0tQdA5UsXDWcgoxDMiKr+4GyePI7"
  "BDx+9X790csty/ck9a30b5wPJIFsoAhQHE0lpu0kAXh5xYgBmJiYmJmZmTAAsgKgRgAc6j"
  "1V";

/* 
 * Resource generated for file:
 *    SlicerSave.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerSave_width          = 21;
static const unsigned int  image_SlicerSave_height         = 21;
static const unsigned int  image_SlicerSave_pixel_size     = 3;
static const unsigned long image_SlicerSave_length         = 352;
static const unsigned long image_SlicerSave_decoded_length = 1323;

static const unsigned char image_SlicerSave[] = 
  "eNpjYmJgogx9//79P1kAoh1NJHKiBRBFTbSAS/37949U7ZFI2omxHVkjMsJjCJrtmCaoqa"
  "mS53dSHY8sGEWEXlzagYCLi4O8iMOjDKs7kUMeF3p/tQGCMAMKWTtcGS4EV/z371+s2s9u"
  "y3WxVcGFgGo8PNyBDKAyrNqL0203zov79fEaHgRRg6n9zpGyEG+dry/2fH+5Fxd6+2AHUM"
  "3v378xtU9q8gOizw8W4UEQNViDLtBD+/7Jxg83unChFxfbgGpev36NVXtljgP+kF86OQKo"
  "Bi3iINmZmIJlRnvghQsX0LT/+fMHV3oDxu/Xr18/fPjw6dOnHz9+ALnIRQcTxQUdAD8ZI2"
  "c=";

/* 
 * Resource generated for file:
 *    SlicerStopRecording.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerStopRecording_width          = 21;
static const unsigned int  image_SlicerStopRecording_height         = 21;
static const unsigned int  image_SlicerStopRecording_pixel_size     = 3;
static const unsigned long image_SlicerStopRecording_length         = 60;
static const unsigned long image_SlicerStopRecording_decoded_length = 1323;

static const unsigned char image_SlicerStopRecording[] = 
  "eNpjYmJgogz9JxeMah9s2vmxAZK0nzA3R0ZDSzslfh9NdUNCOyUIABvP5Uo=";

/* 
 * Resource generated for file:
 *    SlicerTimedOut.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerTimedOut_width          = 21;
static const unsigned int  image_SlicerTimedOut_height         = 21;
static const unsigned int  image_SlicerTimedOut_pixel_size     = 3;
static const unsigned long image_SlicerTimedOut_length         = 664;
static const unsigned long image_SlicerTimedOut_decoded_length = 1323;

static const unsigned char image_SlicerTimedOut[] = 
  "eNrdkkurqWEUxzcDJBNDUwNGBgwMlG/gEqFIDJRLMVHCwO2ElPslNieScs0hE+Vg4zjIV3"
  "H7Cvv8e+W0693HGZ/zG6zW87zrv971rLXe3/8rbrfbeDz2+/1KpVIsFisUCp/PNxqNrtfr"
  "X7WHw8Hj8TgcjkKhMJlMttstbD6ft9lsbrd7v98/0e52O8QUi8WfD6hU6t1BHiRxuVzwP9"
  "VeLhen05nNZn8QbAmEQiEsjpvNZr1ep1IplHE+n8nydrttMBhWq9WaYEPA5XLvR9y/EWi1"
  "2larRZZbLJZIJLJcLt8+wOFwYJcEi8ViPp+HQiGz2UyWCwQCpJ0/uAez2ez78TvBbDZrNp"
  "t8Pp8sZzAY0+n05eVl9gEWi4V7Go1Gp9OPxyOTycQE4ZPlPB6vUqmMCL4RDIfDwWDQ7/d7"
  "vV632+10OuhPJpNBJFmOvqHzqB/lNRqNer3+laBWq1Wr1VcCOFarFZFkOeKlUmm5XC6VSh"
  "hxLpfDj9LpdOoBfIxVIpEgkiw/nU46nQ79TyaTiUQiHo9Ho9EvD+DHYjGTyYTBIfLTzcF0"
  "5HK50WjEBMPhcCAQoFAoAYJgMKjX62UyGWKe7C2+Ir9IJFKr1dh8r9drt9tVKhXWT6PRPN"
  "f+fgVeh/6gw5gRLHzc/Knmf5dfgakWrQ==";

/* 
 * Resource generated for file:
 *    SlicerTinyHelp.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerTinyHelp_width          = 15;
static const unsigned int  image_SlicerTinyHelp_height         = 15;
static const unsigned int  image_SlicerTinyHelp_pixel_size     = 3;
static const unsigned long image_SlicerTinyHelp_length         = 124;
static const unsigned long image_SlicerTinyHelp_decoded_length = 675;

static const unsigned char image_SlicerTinyHelp[] = 
  "eNr7/58cwIAbYKrEbw5Wrkv/fjQSIrtlyxbiTcaqmHYmMzIywrlwQUpMxh/OfX19p0+fRl"
  "YMcQBEVx8qOA0GmIrh5qABNIcBFWO1ESsAKiZSJRAQoxIAcP0Asw==";

/* 
 * Resource generated for file:
 *    SlicerUnDo.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerUnDo_width          = 21;
static const unsigned int  image_SlicerUnDo_height         = 21;
static const unsigned int  image_SlicerUnDo_pixel_size     = 3;
static const unsigned long image_SlicerUnDo_length         = 388;
static const unsigned long image_SlicerUnDo_decoded_length = 1323;

static const unsigned char image_SlicerUnDo[] = 
  "eNpjYmJgogz9JxfQSHvE8SqytYcfq+RdY0ue9riTdUwrTVlWmpGhPe10K8MKYyBiXGESca"
  "wy8lhV5PEqIBl+rCL/XE/HtQXvf33Cpd3vcCFELx7Ev9b+/penWLVrpjogq9SMslUNt1IO"
  "tVAKMVcNswSSOgsDgeLxJ+pxOV43xpF9lSVEO6ZPj146JbzOiWu19dHXF3EFnV6ME9DjWL"
  "UDQczxWqDUsoc78EScUrA5Lu1557qBUhNvLsefbIAmYNVecK4XqH3SrRXkJdrEk41A7Yvu"
  "byVPu+a2EKD2c+9u4NEOSi0YKOp4VeOVWUC9Vs2h+NM8JOSxIvEOm/Pvb+LXDgw3YCKBom"
  "BzxVBzhQhz+QQLvXovZL20y+8kaacEAQAaP3jX";

/* 
 * Resource generated for file:
 *    SlicerUnlink.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerUnlink_width          = 21;
static const unsigned int  image_SlicerUnlink_height         = 21;
static const unsigned int  image_SlicerUnlink_pixel_size     = 3;
static const unsigned long image_SlicerUnlink_length         = 296;
static const unsigned long image_SlicerUnlink_decoded_length = 1323;

static const unsigned char image_SlicerUnlink[] = 
  "eNq9k9EJwyAQhkvW6Uv20BcdIcuIeyQTOIcTBLJK+8HBkWqr0tBKOORy39396k3Tbbr2Pb"
  "5dP8VzzjFG7/08z1j2eAbxlNK2bffXZa3F38WpAit23/fjOLCSzRijPXzCpU/iAdXJXjLw"
  "t4GLXvBaLz2A42/gIhONtV4awMNJNnA6JP+6rljn3FnvSHVK0DNh5FmWRfUOapeTpyKRVN"
  "dOxk9e7h3VjXMocPITgLr61b29hQJXvfVdS572m1e9xUsLIUiG7sic9eqCPettj0x3yv42"
  "74P4le8JZw1kRA==";

/* 
 * Resource generated for file:
 *    SlicerVisible.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerVisible_width          = 21;
static const unsigned int  image_SlicerVisible_height         = 21;
static const unsigned int  image_SlicerVisible_pixel_size     = 3;
static const unsigned long image_SlicerVisible_length         = 448;
static const unsigned long image_SlicerVisible_decoded_length = 1323;

static const unsigned char image_SlicerVisible[] = 
  "eNpjYWJhoQz9+feHPEQT7b+gjDv37pw5dwZNkKD2Pbv2HDl0BMKYM2sORO+adWswTUDR/g"
  "tqBdDSJYuWABlbtm2ZMm0KkAF0A0Tky7cvyIagae/p6nny7AmQDbQrJChERk5GREjEx88n"
  "JSnlzbs3IAM3bXnz6g0ux0NsAVpqY2eDHDU6WjpTJk3Zs2/PmlVr8Psd6F8ZKZmcrJyUtJ"
  "SQsBCIAqADgKilqQXoNiCCuASrdmBY1dTVAA0BhsCVa1dExESACoAkUBzoNaADgFJnTp3B"
  "pb2lrQWoC+iFmLgYIGJhgzoeKAh0FdxeXNqBgfPl0xegYhUlFYgsMPSAgpCABUrh1w5UcO"
  "TYEQgD6AZgiAGNAnKBJJpe7Nq/fQEGL9A6oN8hUQwMK6B/gVFJjHZ4cgUaAgwuoO1Lli2B"
  "OIDkLPMLe1KnbY4bUtopQQBV26vE";

/* 
 * Resource generated for file:
 *    SlicerVolume.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerVolume_width          = 21;
static const unsigned int  image_SlicerVolume_height         = 21;
static const unsigned int  image_SlicerVolume_pixel_size     = 4;
static const unsigned long image_SlicerVolume_length         = 116;
static const unsigned long image_SlicerVolume_decoded_length = 1764;

static const unsigned char image_SlicerVolume[] = 
  "eNrtlDEKACAMA0N/2Wf1xbo6aSItdlA4yHRwgzXDsAIy3ytnRFCoTmXvnO5OoTqZzbQDoF"
  "j9jJPZSjvr/O117ae/edOe5VRuiNLe6dZ1cmYzAUeB320=";

/* 
 * Resource generated for file:
 *    SlicerWait.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerWait_width          = 21;
static const unsigned int  image_SlicerWait_height         = 21;
static const unsigned int  image_SlicerWait_pixel_size     = 3;
static const unsigned long image_SlicerWait_length         = 408;
static const unsigned long image_SlicerWait_decoded_length = 1323;

static const unsigned char image_SlicerWait[] = 
  "eNrNk6+Lg3AYxu/f8D8ZrhgnLlkuaLMMq820MmVoMqwYNC0rjlXFwWB1CwaTbUlkEx0K7p"
  "5DuHTwVYTjXvDl8ZEPL74/3u9/FB+kIOLb7db3/ePxeL1esyxDhoaz2+2G4DzPf/4W8Il4"
  "XdeqqpZl+Xw+8dp1HTL06/WCj6/E3xcEIU3TXrdt24vb7SaK4pDuWZa12WyC4BAEQZ7nh8"
  "O3NE0T/sD+7/d71OKWS47j8EiSBGfUBF3XZVkWvUJ2HGfsAjwej9lsRlEUTdP3+30sjoqL"
  "xQI4wzC2bY9iFUU5n89N0yRJgtnFcQxnIOt53ul0AltVFQbdzy6KIizeEHw+nxdFAfZyua"
  "B1YRhC9z6RRbnVaqVpmmEYP2eyXq91XZdlmbh1E3d++sVNufc/ji/NaDyv";

/* 
 * Resource generated for file:
 *    SlicerZoomIn.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerZoomIn_width          = 21;
static const unsigned int  image_SlicerZoomIn_height         = 21;
static const unsigned int  image_SlicerZoomIn_pixel_size     = 3;
static const unsigned long image_SlicerZoomIn_length         = 400;
static const unsigned long image_SlicerZoomIn_decoded_length = 1323;

static const unsigned char image_SlicerZoomIn[] = 
  "eNrNUzuKhEAUnOvo+kMMjMRcMfBzAAOPYSp4CTHxAIOBxoIgigqKKIjG3mK3QFh2l2W2Z4"
  "aBfUHxqK7q1+919+XybLw/Gq+zt20bhqFpmoIgAJGDIbSnaarr+tv3AAP+TzuqaJoGfZIk"
  "67ru+w5Efu7w9Qy/2nHO0wvjPM8QAJGfO2D1th2dQoaK27bVdQ0BEDkY8Fi9bcesIFuW5c"
  "f9ggGPVZLqfd9nWRZFEQRA5GBIqgdBAFkcx03TXK9XCIDIwZD07vu+oijn9KqqyvMcSDj5"
  "cRzRnSiKsixDT9M0RVFAkns/jsMwDIZhuq5jWVZVVVgIX92nd5omoGVZd715z/M4jhuGAV"
  "7btu/9MpIklWXJ87zjOA/8uKIosIPruv/hv5Pbn4kPR0lbTg==";

/* 
 * Resource generated for file:
 *    SlicerZoomOut.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerZoomOut_width          = 21;
static const unsigned int  image_SlicerZoomOut_height         = 21;
static const unsigned int  image_SlicerZoomOut_pixel_size     = 3;
static const unsigned long image_SlicerZoomOut_length         = 356;
static const unsigned long image_SlicerZoomOut_decoded_length = 1323;

static const unsigned char image_SlicerZoomOut[] = 
  "eNrNkz1qhVAQhd9+4h9iYSX2ioU/C7BwGbaCdRZgpRt4VVrBwkILxUqetbtIPhBDXggvVx"
  "+BnGIYzsyZuTP33svlWbyfxd/Ju67L89z3fU3TsPgwgvLr9eq67ss9YOB/ldPFcRzyy7Kc"
  "53lZFiz+VuHrGX6Uc85Ni/C2A3+rQPSxnElJo+PtHjDwRB/L2RVp0zR9u18YeKIi3du2ra"
  "rqdQc+jEj3LMtIK4qiruu3HfgwIrOnaWpZ1rnNj+PIdLqum6Z59N7XdfU8T5Kkvu9lWbZt"
  "G4ngq/vUsmFsEASH3nySJIqiDMOANgzDo1/GMIymaVRVjaLoxI/jaqgQx/F/+O/i8mfwAR"
  "UhZPw=";


/* 
 * Resource generated for file:
 *    SlicerGo.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerGo_width          = 21;
static const unsigned int  image_SlicerGo_height         = 21;
static const unsigned int  image_SlicerGo_pixel_size     = 3;
static const unsigned long image_SlicerGo_length         = 716;
static const unsigned long image_SlicerGo_decoded_length = 1323;

static const unsigned char image_SlicerGo[] = 
  "eNoTE6MU/CcXYNX+4/e3K6/PbLy1aMa5tu4TFdPPtm64ufDqm7NAcYLab7y9tODyhK4TpZ"
  "gIKH7r7WU82s8+P9J1rAyrXgjqPlF27sVRrNpvvrkElMXU0nm8tONYCZCEmwBUiaYd6K/5"
  "l/qx2tiwOw+ooGFnLtwEoC8g4QDXfunF6bYjRe1HUVDr4cLmAwVFS5MgdtVszQKq6ThWDJ"
  "S6/Oo0svY1V+c37s9t2JNTuzOreltG1dYMIFmzPRPIzZuZAPdsxca0pgN5QLT22jxk7b2H"
  "a2p2ZZSuS1p0dPqPXz++//wOJCEIyEYO7dJ1ibW7M/qO1CBrr9iaWr41OXVW6P23t/9gAL"
  "ToLlgdV7EtBVl7y67iwvUxcf1+c/ZOunHrBgq6eQNZr3uGTca8MKB6ZO0LT03JXR2ZMMXf"
  "Jd3KMtDAIkAfSEKQoZs2XK+Zn15wg2vmolCgemTtZx4dzVwemrogKGaST2SPJzLyLLSF6v"
  "XVC6pzTp4XCFQJVI+s/fuvr117q4DimCi8wx2qtx6qt3N3JVA9Wqq7/OxM9sqwrBWhEATX"
  "Ht3v7ZFnA9ELFAeqAarEmuaP3tuDbAIEpcwPTJzuDyQheg/f3Q1XLy4ujpbjLj8907mnCs"
  "0ECOraVQmUJZjfgf468/DogpMT67fmZq+IAJJANlAE4l9kgGk7GaUNEIhjAAkJCUlJSQkM"
  "gKwAqBEAMwg98Q==";

/* 
 * Resource generated for file:
 *    SlicerCameraButton.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerCamera_width          = 21;
static const unsigned int  image_SlicerCamera_height         = 21;
static const unsigned int  image_SlicerCamera_pixel_size     = 3;
static const unsigned long image_SlicerCamera_length         = 580;
static const unsigned long image_SlicerCamera_decoded_length = 1323;

static const unsigned char image_SlicerCamera[] = 
  "eNrNk8tqwkAUhl1030fxVi+1akBREFrEVzARQXAlUdwE9A1cq4ji2sTrIzQKrtO60LXpQr"
  "CaRVBsf2aoBCNt0S76LUJm5v/OnMwQi+VaPi7lX+n5fJ5l2Vgs1mq1LtB5nq9Wq5VKpVar"
  "bbfb3W73e73dbnu9XpfLdUdwu90ej+ee8EBgGCYajYbDYUmSzDpcdM5xnCAIDocjnU4PBo"
  "M3Qr/fz2QyiUQimUwiE4lEdF0/HA5mvVQq4Ql3NBrhBIrFYqFQwMtwOMQk+sFqMBhcLpeo"
  "YNSxRF273d7tdhuNhs1muyXgWzBEz+gKgUAgMJvNNpvNiV4ul6muqmoul4N4Q8ALThU7Op"
  "1OqiuKsl6vT5qnOjLQkTfq+ATodHe/339WPzaPPpvNptVqNTYviiLdHbdwVj8eXSqVwrGj"
  "Ak+o1+s4fFwKKmPV5/OZddSkF5fNZrELKvR6PZXQ6XQwpJ2DUChk1jGMx+NPhMdvGY/HZl"
  "3TtPl8Pp1OJwRkZFl+NoAhJrGEDJLIG/X9fr9arRaLxesXLwSFQN/pPDJIIn/yy2AGNd9/"
  "Ahkk/+p/v4ZPtqoBKg==";

/* 
 * Resource generated for file:
 *    SlicerMoreOptions.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerMoreOptions_width          = 21;
static const unsigned int  image_SlicerMoreOptions_height         = 21;
static const unsigned int  image_SlicerMoreOptions_pixel_size     = 3;
static const unsigned long image_SlicerMoreOptions_length         = 412;
static const unsigned long image_SlicerMoreOptions_decoded_length = 1323;

static const unsigned char image_SlicerMoreOptions[] = 
  "eNqlVLtqhUAQBe/1bcwVvUl6KzuxkXyAX2Fjo4W/YiEoCIKFXxCxEZtUARHEVCnTp0qVD0"
  "gOLFxEUIi7THF2ds7O7tnZYRiWWdj5zIuioij3sqzyvAQPx4mSdIcpywpYPZ04+AVBJhgB"
  "v0cHyUhDx9kO08HFTQ/ToQkNHQrTHB50iE9DXylfVZVlWXmeA3ddFwRB0zTA0zSlaToMwz"
  "L4ctFX2V3XfXn9Mk0TOI7jt/dv3/eBi6L4+PxJkmQZrOsPKJ6lp65r27YRDNz3fRiGbdsC"
  "z/OcZdk4jstgw3ikkU7TjFX2fw1V1baK1nGeYVEUEbCVHd9na3PHccqy9DxvKwB3x9fbOR"
  "522Fm9Xp/26fsD704jHWXRIjV9tyEtDq0Mr4AqIgCawDAlnltnW9kfu2MH+w==";

/* 
 * Resource generated for file:
 *    SlicerTable.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerTable_width          = 21;
static const unsigned int  image_SlicerTable_height         = 21;
static const unsigned int  image_SlicerTable_pixel_size     = 3;
static const unsigned long image_SlicerTable_length         = 228;
static const unsigned long image_SlicerTable_decoded_length = 1323;

static const unsigned char image_SlicerTable[] = 
  "eNq9k9ENhCAMhnWmwkpwK7kCS7CAd0vwxgS8YWOjIVJtTy/3PZg/pp/8mHQYnlLv8ltd2T"
  "bnXErh9anCXKe5AsDIZRyJMeIXWB0kcCaEkFJi9fcGTrJZ1CkbY/ZM+j5zob9WvPfOOXoe"
  "UJan0/9c3m9Q+b6/sry1lr2Isjzp/UW+Kt//f/H0z0pbvs03yrf5WtdwpuMe4TYFibONwy"
  "3G90nisO9PWADEltKU";


/* 
 * Resource generated for file:
 *    SlicerFiducialsAddNew.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerFiducialsAddNew_width          = 21;
static const unsigned int  image_SlicerFiducialsAddNew_height         = 21;
static const unsigned int  image_SlicerFiducialsAddNew_pixel_size     = 3;
static const unsigned long image_SlicerFiducialsAddNew_length         = 168;
static const unsigned long image_SlicerFiducialsAddNew_decoded_length = 1323;

static const unsigned char image_SlicerFiducialsAddNew[] = 
  "eNpjYmJgogz9JxcQ1N4xby/dtONXjKkG03b8JqDJwrX//fsPJvaPmECDqCcp5L98+0180D"
  "3WZUAjyQh5XBrRtAPDBC1YgBrhVqMZAleMafuPn3/IsJ2KfscMYVJC/h8lWQYcJjhM+PcX"
  "V6qjVpofwAxLXmlDCQIANTYPHg==";

/* 
 * Resource generated for file:
 *    SlicerFiducialsDeleteAll.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerFiducialsDeleteAll_width          = 21;
static const unsigned int  image_SlicerFiducialsDeleteAll_height         = 21;
static const unsigned int  image_SlicerFiducialsDeleteAll_pixel_size     = 3;
static const unsigned long image_SlicerFiducialsDeleteAll_length         = 204;
static const unsigned long image_SlicerFiducialsDeleteAll_decoded_length = 1323;

static const unsigned char image_SlicerFiducialsDeleteAll[] = 
  "eNpjYmJgogz9Jxfg187ExApBpGr/+/cvkFw2bTacJEY73Dr8CI92NPOfPHmye8dOCDl71u"
  "yOjg782imxHWLCv3//Dhw4AGQDyadPn86cORNCXr16Fc15WLU/1gWJYCWJ0Q5hQLSgsYm0"
  "HW4dGps+to/6nTy/H0AFWzZugpBzZs4iqB2YPfNS0nEhtMxLldKGEgQA/Lcusw==";

/* 
 * Resource generated for file:
 *    SlicerFiducialsDeleteLastClicked.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerFiducialsDeleteLastClicked_width          = 21;
static const unsigned int  image_SlicerFiducialsDeleteLastClicked_height         = 21;
static const unsigned int  image_SlicerFiducialsDeleteLastClicked_pixel_size     = 3;
static const unsigned long image_SlicerFiducialsDeleteLastClicked_length         = 280;
static const unsigned long image_SlicerFiducialsDeleteLastClicked_decoded_length = 1323;

static const unsigned char image_SlicerFiducialsDeleteLastClicked[] = 
  "eNpjYmJgogz9Jxfg187ExApBpGr/+/cvkFw2bTacJEY73Dr8CI92NPOfPHmye8dOCDl71u"
  "yOjg782imxHWLCv3//Dhw4AGQDyadPn86cORNCXr16Fc15WLUDyRs3bkDId+/eAQ2BkEBD"
  "SNUOcQNB7R3z9kIQCws7hPFYlwGZ7F10AMgQEpGCq8SvHdkQICJee+f8fRCNcKuBqGvBPj"
  "rbTrzfKQx5FO3//kG1X79ORrwfQAVbNm6CkHNmziKoHZg981LScSG0zEuV0oYSBABHHQ7g";

/* 
 * Resource generated for file:
 *    SlicerFiducialsSelectAll.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerFiducialsSelectAll_width          = 21;
static const unsigned int  image_SlicerFiducialsSelectAll_height         = 21;
static const unsigned int  image_SlicerFiducialsSelectAll_pixel_size     = 3;
static const unsigned long image_SlicerFiducialsSelectAll_length         = 272;
static const unsigned long image_SlicerFiducialsSelectAll_decoded_length = 1323;

static const unsigned char image_SlicerFiducialsSelectAll[] = 
  "eNpjYmJgogz9JxfAtYcXtQDJwNx6IOkSnQkkvTOqgaQPmISwnSPT4Wp8M2uQtaOZidUuNj"
  "YWXLb7ZdWi6X2sy4BGIstC1GPajsaFa8QqC9cenNcAJJmZGZFth1uNbDtQDVw9tWyHhC3x"
  "foeoxxryHBxsWEMeUxyu3TOtkqQEA1GP1XZlZUWsWjDFEamusJkkv0PUUyvkEyq7QKna2Y"
  "lgvAPVwNVTy/aAnDqS/A5RT1KOw6oSM78HZtdgze9BYBsJ5ndSSxtKEAAW8HSu";

/* 
 * Resource generated for file:
 *    SlicerFiducialsSelectAllInList.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerFiducialsSelectAllInList_width          = 21;
static const unsigned int  image_SlicerFiducialsSelectAllInList_height         = 21;
static const unsigned int  image_SlicerFiducialsSelectAllInList_pixel_size     = 3;
static const unsigned long image_SlicerFiducialsSelectAllInList_length         = 136;
static const unsigned long image_SlicerFiducialsSelectAllInList_decoded_length = 1323;

static const unsigned char image_SlicerFiducialsSelectAllInList[] = 
  "eNpjYmJgogz9JxdQUbtnWiVJJC1sJwZg2v737z+4LAMYoGlBE4Sox2o7pl40cfx+hyh7rI"
  "tOYhpL0HaIRuJth6sBaoRbDTcEzQFUt526ficv5AnGO3VT3VDPcWRrpwQBAOiFrAM=";

/* 
 * Resource generated for file:
 *    SlicerFiducialsSelectNone.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerFiducialsSelectNone_width          = 21;
static const unsigned int  image_SlicerFiducialsSelectNone_height         = 21;
static const unsigned int  image_SlicerFiducialsSelectNone_pixel_size     = 3;
static const unsigned long image_SlicerFiducialsSelectNone_length         = 256;
static const unsigned long image_SlicerFiducialsSelectNone_decoded_length = 1323;

static const unsigned char image_SlicerFiducialsSelectNone[] = 
  "eNpjYmJgogz9JxfAtYcXtQDJwNx6IOkSnQkkvTOqgaQPmISwnSPT4Wp8M2uQtaOZidUuNj"
  "YWXLb7ZdXi14smC1GPaTuR2tFsD85rAJLMzIwEtQPVwNVTy3ZI2BKvHaIea8hzcLBh1Ysp"
  "DtfumVZJUoKBqMdqu7KyIlYtmOKIVFfYTJLfIeqpFfIJlV2gVO3sRFA7UA1cPbVsD8ipI0"
  "k7RD1JOQ6rSsz8HphdgzW/B4FtJJjfSS1tKEEAjXGi5g==";

/* 
 * Resource generated for file:
 *    SlicerFiducialsSelectNoneInList.png (zlib, base64) (image file)
 */
static const unsigned int  image_SlicerFiducialsSelectNoneInList_width          = 21;
static const unsigned int  image_SlicerFiducialsSelectNoneInList_height         = 21;
static const unsigned int  image_SlicerFiducialsSelectNoneInList_pixel_size     = 3;
static const unsigned long image_SlicerFiducialsSelectNoneInList_length         = 124;
static const unsigned long image_SlicerFiducialsSelectNoneInList_decoded_length = 1323;

static const unsigned char image_SlicerFiducialsSelectNoneInList[] = 
  "eNpjYmJgogz9JxdQUbtnWiVJJC1sJwZg2v737z+YJIjBAAZw9WhcuHpctqMphoh8+fYbl+"
  "2YigmKkGo7Hr8TtAirOC1sp5HfiQt5ouKduqluqOc4srVTggAkl8Lt";

