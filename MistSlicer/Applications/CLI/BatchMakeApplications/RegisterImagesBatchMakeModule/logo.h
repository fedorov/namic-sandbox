/* 
 * Resource generated for file:
 *    logo.png (zlib, base64) (image file)
 */
static const unsigned int  image_logo_width          = 40;
static const unsigned int  image_logo_height         = 40;
static const unsigned int  image_logo_pixel_size     = 3;
static const unsigned long image_logo_length         = 228;
static const unsigned long image_logo_decoded_length = 4800;

static const unsigned char image_logo[] = 
  "eNrt18ENgCAQRFEa8WYnVmhJ9uDdKjwrCYkhQdYBlp3E8DP3d57rGo3ec85R0HOf7GmKG9"
  "AwS5rixqgZnaJEtzedQ7vSMkp0e9AISnR1aRxVpEtRRfeYZz8QXdbNr5F+0GeforEbo410"
  "ir7qqdjiCmjs5tBqWnYDLaMV9Cfqh6BEF6d1UdBF0FIXoTu5Mg2iui6O1rk5muIWodVuSl"
  "NcZ9648L/vBqXki6g=";

