
  Section of Biomedical Image Analysis
  Department of Radiology
  University of Pennsylvania
  3600 Market Street, Suite 380
  Philadelphia, PA 19104

  Web:   http://www.rad.upenn.edu/sbia/
  Email: sbia-software at uphs.upenn.edu

  Copyright (c) 2012 University of Pennsylvania. All rights reserved.
  See http://www.rad.upenn.edu/sbia/software/license.html or COPYING file.



INTRODUCTION
============

  Please see the list of software packages distributed by SBIA [1] for information on
  the GLISTR software package.  You can request the download links for the GLISTR package
  by submitting the software request form [2], providing your email address along with
  your name and affiliation.

  The Slicer 4 extension of GLISTR is part of the GLISTR software since version 2.0.

  Please contact the SBIA Group <sbia-software at uphs.upenn.edu> if you have any
  questions regarding the GLISTR package or the corresponding Slicer extension.



REFERENCES
==========

  [1] http://www.rad.upenn.edu/sbia/software/index.html#glistr
  [2] http://www.rad.upenn.edu/sbia/software/request.php
