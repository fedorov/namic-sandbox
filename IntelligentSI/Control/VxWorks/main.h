/**
 * \mainpage Open Core Control software for surgical robots
 * \author Hiroaki Kozuka \@Nagoya Institute of Technologey
 *  E-mail:ch16503@stn.nitech.ac.jp
 *
 */
#include "Driver.h"
#include "Device.h"
#include "ACP420.h"
#include "ACP560.h"
#include "Robot.h"
#include "Joint.h"
#include "InterfaceManager.h"
#include "InterfaceMasterR.h"
#include "InterfaceSlicer.h"
#include "FrameInverse.h"
#include "FrameForward.h"
#include "ConsoleW.h"
#include "fReader.h"


void Main();

