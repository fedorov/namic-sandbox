Patched VTK-5.8.0 files for enabling advanced debugging of memory leaks. See more information at:
http://www.slicer.org/slicerWiki/index.php?title=Slicer3:VTK_Leak_Debugging
