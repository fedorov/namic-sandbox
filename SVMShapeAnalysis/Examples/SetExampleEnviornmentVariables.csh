#
# set ITKSVM_BIN_DIR to the directory that contains the svmTrain,
# svmClassify, svmJackknife, and svmDDCalculator applications
#
setenv ITKSVM_BIN_DIR  /usr/local/dev/MITGollandDist-Build/bin

#
# set ITKSVM_DATA_DIR to the directory that contians the Data
# directory that was included with this distribution
#
setenv ITKSVM_DATA_DIR /usr/local/dev/MITGollandDist/Data

#
# set ITKSVM_OUTPUT_DIR to the directory where you would like to write
# trained support vector machines and other files.
#
setenv ITKSVM_OUTPUT_DIR /tmp/svmtmp
