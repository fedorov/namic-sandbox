
This project is directed by 

      David Tuch
      Martinos Center for Biomedical Imaging
      Massachusetts General Hospital
      www.mit.edu/~dtuch
      

For full details in this project, please visit

http://www.na-mic.org/Wiki/index.php/Engineering:Programmers_Week_Summer_2005


Use case: 

Register an EPI image to a conventional structural image. This will help
correct for the distortions in EPI images, and help me use tools which have
been developed for conventional structural images 



Vision: 

Software tool which can perform robust non-rigid, intrasubject registration
between an EPI image and a conventional structural image. 



Goal: 

Identify optimal ITK method and parameter settings for non-rigid intrasubject
registration of T2 EPI to T1 conventional. Provide software devliverable. 



Test Data: 

We'll look at all 6 healthy individuals from the Dartmouth data set.
DTI data and convertional data. 



Team members: 

Snyder, Tuch, Ibanez


