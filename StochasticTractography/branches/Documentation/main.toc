\contentsline {chapter}{\numberline {1}Introduction}{15}
\contentsline {chapter}{\numberline {2}Background}{19}
\contentsline {section}{\numberline {2.1}Neuroanatomy and Fiber Tracts}{20}
\contentsline {section}{\numberline {2.2}Diffusion Tensor MRI Physics}{20}
\contentsline {section}{\numberline {2.3}Diffusion Tensor}{22}
\contentsline {subsection}{\numberline {2.3.1}Streamline Tractography}{24}
\contentsline {subsection}{\numberline {2.3.2}Stochastic Tractography}{25}
\contentsline {subsubsection}{Bootstrap Method}{25}
\contentsline {subsubsection}{Bayesian Methods}{26}
\contentsline {chapter}{\numberline {3}Stochastic Tractography Algorithm}{29}
\contentsline {section}{\numberline {3.1}Mathematical Derivation}{31}
\contentsline {subsection}{\numberline {3.1.1}Linearized Diffusion Tensor Model}{31}
\contentsline {subsection}{\numberline {3.1.2}Constrained Diffusion Tensor Model}{33}
\contentsline {subsection}{\numberline {3.1.3}Fiber Orientation Likelihood Function}{34}
\contentsline {subsection}{\numberline {3.1.4}Connectivity Probability Function}{35}
\contentsline {subsection}{\numberline {3.1.5}Stochastic Fiber Tract Generation}{36}
\contentsline {chapter}{\numberline {4}Implementation}{39}
\contentsline {section}{\numberline {4.1}Architecture}{39}
\contentsline {section}{\numberline {4.2}ITK Stochastic Tractography Filter}{44}
\contentsline {section}{\numberline {4.3}Command Line Module Interface}{46}
\contentsline {section}{\numberline {4.4}3D Slicer Interface}{48}
\contentsline {chapter}{\numberline {5}Analysis of Right Internal Capsule Fibers}{51}
\contentsline {section}{\numberline {5.1}Single ROI}{53}
\contentsline {section}{\numberline {5.2}Two ROIs}{55}
\contentsline {section}{\numberline {5.3}Comparison with streamlining tractography}{56}
\contentsline {chapter}{\numberline {6}Discussion and conclusions}{65}
\contentsline {section}{\numberline {6.1}Potential Extensions}{65}
\contentsline {section}{\numberline {6.2}Study of frontal lobe fibers in schizophrenia}{66}
\contentsline {subsection}{\numberline {6.2.1}Background}{67}
\contentsline {subsection}{\numberline {6.2.2}Method}{68}
\contentsline {section}{\numberline {6.3}Summary}{69}
\contentsline {chapter}{\numberline {A}Command Line Module Interface}{71}
