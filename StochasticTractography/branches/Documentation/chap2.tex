\chapter{Background}

Diffusion Tensor Imaging (DTI or DTMRI) is a recently developed Magnetic Resonance (MR) technique that provides information about the diffusion of water molecules in the brain.  In white matter, due to the interactions between water molecules and the surrounding nerve fibers, the principal diffusion direction is aligned with the local fiber orientation.

White matter tractography is a visualization and analysis tool for DTI data.  It takes local diffusion information provided by DTI images and produces explicit representations of fiber bundles which may explain the observed global diffusion distribution.  White matter tractography characterizes fiber bundles in-vivo and provides insights into questions concerning white matter architecture.

A number of clinical studies have used tractography to compare fiber bundle characteristics in different populations.  Many of these studies utilize tractography methods which do not provide a measure of the confidence regarding the estimated fiber bundles.  The stochastic tractography system implemented in this research provides these measures of confidence, and may open new avenues of clinical investigation of white matter architecture.

\section{Neuroanatomy and Fiber Tracts}
\begin{figure}
	\includegraphics[width=\linewidth]{graysfibertracts}
	\caption{Example of human brain fiber tracts viewed from the front (coronal) and from the left (sagittal).  This image was derived from anatomical atlas diagrams in Gray's Anatomy \cite{odonnel06}.}
\label{fig:graysfibertracts}
\end{figure}

Nerve tissue in the brain can be divided into gray and white matter.  Gray matter is found throughout the brain but is concentrated on the cortical surface as well as in structures deep within the brain such as the thalamus.  The defining characteristic of gray matter is its lack of myelinated axons.  In contrast, white matter is white in color because it has an abundance of myelinated axons.  Myelin consists mostly of lipids and gives white matter its color.  Bundles of these axons comprise white matter tracts. Figure \ref{fig:graysfibertracts} illustrates some prominent fiber tracts.

\section{Diffusion Tensor MRI Physics}

MRI is typically used to differentiate between different tissue types, such as gray and white matter.  This technique works by magnetically polarizing a particular slice of the brain.  A strong uniform magnetic field is applied to the entire brain causing the spins of the water molecules to orient in the same direction.  Another magnetic field, this one nonuniform in space, polarizes the spins of the atoms in the brain differently depending on their location.  This gradient field is turned off and as the spins of the electrons reorient, or relax, back to the strong uniform field, they release an electro-magnetic signal which is picked up by the receiving coil.  The frequency of these emitted waves depends on the atom's polarization which in turn depends on its position in space.  The time needed for the spins to relax, known as the relaxation time, depends on the type of tissue.  Using this data, an image can be constructed that differentiates between tissue types due to their characteristic relaxation time. Unfortunately, white matter appears homogeneous in anatomical MRI images.  Anatomical MRI images do not provide much information about the orientation of the white fiber tracts within each voxel.  Without this information it is not possible to reliably determine the connectivity between different regions of gray matter.  Diffusion Tensor Imaging is a recently developed MR technique which provides more information to characterize fiber tracts.

Diffusion Tensor Imaging (DTI) or DTMRI is an imaging technique that indirectly provides information about fiber tract orientation from the diffusion profile of water in the brain tissues. Diffusion in many parts of the brain occurs anisotropically; the rate of diffusion varies with direction.  This anisotropy is caused by local physical constraints that impede diffusion.  The diffusion of water molecules, which are the predominant signal emitters in MR imaging, is believed to be constrained by the myelin that surrounds axons.  DTI images describe the diffusion profile of water within each voxel using a diffusion tensor.  These tensors can be modeled as ellipsoids with the eigenvectors describing the major and minor axes of the ellipsoid and the associated eigenvalues scaling these axes.  Isotropic diffusion profiles result in spherical tensors while anisotropic diffusion profiles produce more eccentric tensors.  The parameters which describe these tensors are obtained from Diffusion Weighted Images (DWI) of the same volume captured using at least six unique gradient directions and one reference image obtained in the absence of weighting gradients.

Each Diffusion Weighted Image (DWI) provides information about the magnitude of diffusion in one particular direction.  Diffusion Weighted imaging works similarly to anatomical MRI imaging but it also captures the Brownian diffusion of molecules during the imaging process.  Unlike anatomical MRI, an additional gradient magnetic field is applied in a chosen direction which then makes the resulting observations sensitive to the self diffusion of water in that direction. An MRI image obtained using these diffusion sensitizing gradients is referred to as a Diffusion Weighted Image or DWI.  Associated with each of these images is the direction of the magnetic field gradient used to polarize the molecules.  This information is necessary because different magnetic field gradients may result in significantly different DWI images due to the anisotropy of diffusion in certain regions of the brain.  Finally, this diffusion information can be used to estimate the parameters of a diffusion tensor which is then used to infer the orientation of fiber tracts in that voxel.

\section{Diffusion Tensor}
The diffusion tensor is a 3x3 symmetric matrix which describes the distribution of diffusion within each voxel.  Under ideal, noise-free conditions, the diffusion tensor is related to the DWI intensity by the observation model:
\begin{equation}
z_{i}=z_0 e^{-b_i \mathbf{g}_i^T \mathbf{D} \mathbf{g}_i}
\end{equation}
where $\mathbf{D}$ is the diffusion tensor, $z_i$ is the DWI intensity, $z_0$ is the baseline intensity given by the B0 image, $\mathbf{g}_i$ and $b_i$ are the associated magnetic gradient directions and diffusion weighting factor respectively.

The diffusion tensor is positive definite, thus all of its eigenvalues are positive.  Each eigenvalue represents the magnitude of diffusion in the direction of the eigenvector associated with that eigenvalue.  The diffusion distribution described by the tensor can be visualized as an ellipsoid, whose major and minor axis are described by the eigenvectors and associated eigenvalues of the tensor.  The eigenvector associated with the largest eigenvalue is sometimes referred to as the principal diffusion direction.  If the diffusion is sufficiently anisotropic, the principal diffusion direction is a good estimate of the local fiber orientation \cite{Lin01}. %add physical meaning of the ellipsoid
%insert picture of ellipsoid

The diffusion tensor is sensitive to changes in the orientation of the object being scanned. Thus clinical studies tend to use statistics which are invariant to changes in orientation.  The most commonly used properties are the trace and the fractional anisotropy.  The trace of the tensor is the sum of the diagonal elements of the tensor and represents the average total diffusion.  A higher trace implies that there are few obstacles to water diffusion in that voxel.  Fractional anisotropy is a measure of the degree of difference between the largest eigenvalue and the smaller two eigenvalues:
\begin{equation}
FA=\sqrt{\frac{3[(\lambda_1-D_{av})^2 + (\lambda_2-D_{av})^2 + (\lambda_3-D_{av})^2]}
{2(\lambda_1^2+\lambda_2^2+\lambda_3^2)}}
\end{equation}

\begin{equation}
D_{av}=\frac{\lambda_1+\lambda_2+\lambda_3}{3}
\end{equation}
where $\lambda_1,\lambda_2,\lambda_3$ are the eigenvalues of the diffusion tensor and $D_{av}$
FA ranges from 0 for perfectly isotropic diffusion, a sphere, to 1 for perfectly anisotropic diffusion, an infinite cylinder.  While many other measures of anisotropy exist, fractional anisotropy is currently the most popular.

%fill this in with related properties of the diffusion tensor
%various measures of anisotropy
%mathematic properties


%[most have been voxel based,
% ROI studies, select a subset of voxels belonging to a particular fiber bundle of interest]
%[ROI studies sometimes select voxels in only one slice, which may not capture the information presented by the entire fiber tract...]


%[insert pictures]
%directionally coded images here
\begin{figure}
  \subfigure[superquadric tensor glyphs]{
	  \includegraphics[width=0.5\linewidth]{packedglyphs}
	  \label{fig:visualization:a}
	}
	\subfigure[streamline tractography]{
	  \includegraphics[width=0.5\linewidth]{tractography}
	  \label{fig:visualization:b}
	}
	\caption{Glyphs and streamlining tractography on the same DTI data \cite{KindlmannTVCG2006}. The color in both images represent the estimated orientation of the fiber tract modulated by the degree of anisotropy in the data.  The color key is red is for left-right, blue for superior-inferior, green for anterior-posterior.  Regions that are white have low anisotropy while saturated regions exhibit highly anisotropic diffusion.}
  \label{fig:visualization}
\end{figure}

The two primary ways to visualize DTI data is through the use of glyphs and tractography.  Figure \ref{fig:visualization} demonstrates these two methods.  Glyphs are visual representations of the tensors at each voxel in one slice of DTI data.  For instance, direct renderings of the diffusion tensor can be used as glyph.  Glyph visualization is used for understanding diffusion in a localized region of interest.  Studies that investigate local differences in DTI observations between different subjects are known as Region of Interest or ROI studies. Although ROI analysis is straightforward, it is limited in the information it can provide and may actually introduce errors.  For instance, it is difficult to determine if the observed location along a fiber bundle in one patient corresponds to the observed location in another patient.  Ultimately, clinical researchers are often interested in the global fiber bundles which produced these local DTI observations.  The fiber bundles span multiple voxels, limiting the usefulness of glyph visualization in global studies of fiber bundle characteristics.  These inquiries into the characteristics of fiber bundles led to the invention of white matter tractography methods.  In contrast to glyph visualization, white matter tractography incorporates diffusion information across multiple voxels in order to estimate a fiber bundle or bundles which could explain the observed diffusion data.

\subsection{Streamline Tractography}
%talk about as many past works in DTI as possible
%name them by what the authors have called them
%Survey of White Matter Tractography
%Behrens
%Bootstrap
%Fast marching
%Stochastic Tractography

Tractography can be performed in a number of ways. One method is to estimate tracts which are collinear with the principal direction of diffusion direction of every voxel it passes through.  These methods are collectively known as streamlining methods and have been suggested and characterized by a number of researchers \cite{Mori99, Basser00}.  Streamline tractography has relatively low computational cost and is very useful for visualization of DTI data.  However, streamline approaches do not provide information regarding the certainty of the estimated fiber tracts, limiting their usefulness in clinical studies which investigate white matter architecture characteristics.  Additionally, this lack of confidence information limits the regions that streamlining tractography can analyze.  Fiber orientation in highly isotropic regions are very uncertain.  Since streamlining methods do not account for this uncertainty, one cannot confidently analyze a region containing isotropic voxels using streamlining methods.  To reflect this limitation, many streamline methods will not estimate tracts that even momentarily pass through isotropic regions.  Unfortunately, isotropic voxels occur throughout the brain, even in regions with highly coherent fibers.  These voxels appear isotropic due to noise, distortions in the DTI data or due to limitations in imaging resolution which result in partial volume effects.  Partial volume effects occur when multiple fibers cross within a single voxel resulting in a diffusion distribution which is affected by both fiber orientations.  Under a single fiber observation model, partial volume effects result in reduced anisotropy and thus increased uncertainty in the fiber orientation estimate.  This lack of robustness limits the method's application in studies of fiber bundle characteristics.  These limitations motivated the invention of stochastic or probabilistic tractography algorithms.  This class of tractography algorithms overcomes the shortcomings of streamline methods by explicitly modeling the uncertainty in the local fiber orientation.

\subsection{Stochastic Tractography}
Stochastic tractography, sometimes known as probabilistic tractography, differs from streamlining methods in that it takes into account the uncertainty in fiber orientation when calculating estimates of fiber tracts.  Stochastic methods perform tractography under a probabilistic framework;  beliefs regarding the estimated local fiber orientation are propagated to provide a measure of confidence regarding fiber tracts that span multiple voxels.  This explicit modeling and propagation of beliefs allows stochastic methods to generate tracts in regions of low anisotropy. Stochastic methods can generate tracts that momentarily pass through regions of low anisotropy because they integrate local fiber orientation uncertainty into the uncertainty of the entire tract.  The robustness of stochastic methods to local fiber orientation uncertainty has even enabled some studies to directly assess the connectivity of gray matter, which generally exhibits isotropic diffusion, with other regions of the brain\cite{behrensMRM03}.

\subsubsection{Bootstrap Method}

The Bootstrap Method is a stochastic method that calculates the degree of connectivity between different regions of the brain based on the variance in the original DTI data.  The method obtains a measure of the variance of the DTI data by using redundant sets of DTI data and through the creation of new data which consist of recombinations of the original data.  In Jones et al. \cite{derek} nine redundant sets of DWI volumes are obtained to perform the Bootstrap method.  Random combinations of portions of DWI volumes are sampled from this pool to generate a large number of complete mixed DWI sets known as bootstraps.  These complete mixed DWI sets are then converted to DTI images.  Standard streamline tractography is then performed on each bootstrap set at the same starting, or seed location.  A visitation percentage is then calculated for each voxel in the volume indicating the percentage of sample sets which generated a tract that passed through that particular voxel.  This visitation percentage can be interpreted as the probability that a voxel is connected to the seed point via a fiber tract.

\subsubsection{Bayesian Methods}
Stochastic tractography methods which use Bayesian frameworks express beliefs about estimated fiber tracts by generating a posterior probability distribution of fibers given the observed DTI data.   These tractography methods use a probabilistic model to relate the underlying fiber orientation with the observed DTI data.  The probabilistic model is applied to every voxel to generate a posterior distribution of possible fiber orientations given the observed diffusion in that voxel.  A streamline-like tractography method is then used to generate tracts by randomly sampling fiber directions from the fiber orientation posterior at each voxel as calculated by the local model.  The sampled tract is a realization of a random variable generated from the posterior distribution of fibers.  Since there are many possible paths, to obtain a good approximation of the posterior distribution, many paths must be sampled.  Additionally, similar to the Bootstrap method, the probability that region $A$ is connected to region $B$ can then be found by calculating the fraction of paths that pass through region $B$ originating from $A$.

Behrens's Bayesian approach was one of the pioneering works in the field of stochastic tractography\cite{behrensMRM03}. An important idea in Behren's work is in keeping clear the distinction between estimating a general diffusion distribution from DWI data and estimating the local fiber orientation from DWI data.  Although an inference of the local fiber orientation can be made from the tensor model's principal diffusion direction, the tensor model is primarily a model to infer the distribution of diffusion given the data.  However, in stochastic tractography, we wish to infer the local fiber orientation from the observed diffusion data.  Behrens's formulates this distinction by avoiding the tensor model altogether in favor of the two-compartment model. The two-compartment model makes the assumption that only a single fiber passes through a voxel.  Deviations from this simple model due to crossing fibers is captured as uncertainty in the fiber orientation.  In this model a voxel is described as two compartments whose net diffusion profile is the sum of a small anisotropic diffusion component that occurs in and around the fiber and a larger isotropic diffusion component outside of the fiber \cite{behrensMRM03}.  The fiber orientation distribution is analytically intractable.  Behrens overcomes this issue by computing the PDF using Markov Chain Monte Carlo (MCMC) techniques \cite{andrieu03introduction}.  MCMC is a method to numerically integrate an analytically intractable integral.  Unfortunately, MCMC methods are sometimes computationally expensive.  Thus Friman et al. \cite{frimanTMI06} introduced a stochastic method that avoids MCMC.  Our system implements a stochastic tractography method based on Friman's approach.  In the next section, we provide a detailed explanation of the theory behind the stochastic tractography algorithm implemented in this thesis.
