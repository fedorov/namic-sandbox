%
% Complete documentation on the extended LaTeX markup used for Insight
% documentation is available in ``Documenting Insight'', which is part
% of the standard documentation for Insight.  It may be found online
% at:
%
%     http://www.itk.org/

\documentclass{InsightArticle}

\usepackage[dvips]{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  hyperref should be the last package to be loaded.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[dvips,
bookmarks,
bookmarksopen,
backref,
colorlinks,linkcolor={blue},citecolor={blue},urlcolor={blue},
]{hyperref}


%  This is a template for Papers to the Insight Journal. 
%  It is comparable to a technical report format.
\title{Implementing IO Streaming in ITK Image Readers}

% Increment the release number whenever significant changes are made.
% The author and/or editor can define 'significant' however they like.
\release{1.00}

% At minimum, give your name and an email address.  You can include a
% snail-mail address if you like.
\author{Hans Johnson$^{1}$, Douglas Alan$^{2}$ and Luis Ib\'{a}\~{n}ez$^{3}$}
\authoraddress{$^{1}$University of Iowa\\
               $^{2}$Harward University, IIC\\
               $^{3}$Kitware Inc.}

\begin{document}


\ifpdf
\else
   %
   % Commands for including Graphics when using latex
   % 
   \DeclareGraphicsExtensions{.eps,.jpg,.gif,.tiff,.bmp,.png}
   \DeclareGraphicsRule{.jpg}{eps}{.jpg.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.gif}{eps}{.gif.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.tiff}{eps}{.tiff.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.bmp}{eps}{.bmp.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.png}{eps}{.png.bb}{`convert #1 eps:-}
\fi


\maketitle


\ifhtml
\chapter*{Front Matter\label{front}}
\fi


% The abstract should be a paragraph or two long, and describe the
% scope of the document.
\begin{abstract}
\noindent
This document describes an improvement to the Image IO infrastructure of the
Insight Toolkit ITK. The toolkit as a whole was designed and implemented to
support streaming data accross filters. The purpose fo this functionality was
to allowing performing image processing in large images without having to load
the entire image into memory. Instead, the processing can be performed by
loading smaller pieces of the image and processing every piece independently.

Unfortunately, the implementation of streaming was currently not usable due to
the lack of streaming support at the level of the readers. This report
describes the modifications that must be applied to the Image IO infrastructure
in order to support streaming at read time. 
\end{abstract}

\tableofcontents

\section{Introduction}

One of the initial design requirements of ITK was to be able to process images
larger than the actual RAM of the computer in which the processing was
performed. This requirement was motivated by the need of processing the data
from the Visible Human datasets. In order to satisfy this requirement, the
pipeline architecture of ITK included the concept of \emph{RequestedRegion},
that was negotiated filters placed in contiguous locations of the pipeline.

Typically, the requested region was expected to be smaller than the total size
of the image, and even smaller than the portion of the image that was loaded
into memory at that point. By requesting regions smaller than the total size of
the image, filters in the pipeline were empowered to process large images
without needing to have the entire image in memory at a given time. The image was
partitioned in sub-regions and each one of them was processed sequentially.

This streaming architecture, is currently fully implemented in the ITK
pipeline, but suffers from the limitation that most of the image file readers
were not supporting it. As a consequence, when the request for a smaller porting
of the full image arrived to the the ultimate data source: the reader, it was 
addressed by disregarding the request and loading the full image into memory.

Honoring the request for a smaller region of the image, however, needed only a set
of relatively minor modifications in the Image IO infrastructure of the toolkit, 
as well as customized modifications in every one of the ImageIO classes.

This paper describes the modifications required in the basic infrastructure,
and provide examples on how to modify some of the ImageIO classes. In
particular the YAFF, MetaImage, FITS and Nifty image IO classes.



\section{Streaming Architecture}

 When an image is passed through a pipeline, each intermediate filter allocates
 memory for its output. Normally this results in a memory allocation that is
 several times the size of the input image. For example, in order to load an
 image of pixel type \code{signed short} with $256 \times 256 \times 200$
 pixels, the reader will allocate 12 megabytes. If we pass this image to a
 \doxygen{MedianImageFilter}, the filter will allocate another 12 megabytes in
 order to store the output of the median filtering. As we add more and more
 filters to the pipeline, the memory consumption increases, and the final
 result is that in many cases, certain images are too large for allowing the
 entire pipeline to be allocated in memory. In those circumstances, it is
 useful to be able to cut the image into smaller pieces and process the pieces
 one by one.  In the case of the previous example, we could cut the image into
 eight regions of $128 \times 128 \times 100$ pixels each. In this way, the
 reader only has to allocate 4 megabytes at a time, and so does the median
 filter. The pipeline will request one after another each one of the eight
 pieces that are required to complete the output image. 
 
 Note that the blocks that are requested to the reader do not necessarily form
 a partition of the image, instead they may have overlapping regions. This
 happens because some filters may need an extra boundary in order to compute
 the output region. This is the case of the Median filter, for example, if we
 use a kernel with a radius of 1 pixel, in order to produce a given output region
 the median filter will need to request an input region that is equal to the
 output region enlarged by two pixels along every direction. As a result, the
 regions that are requested to the reader will have overlaps of 2 pixels in the
 central region.  As more and more pixels are connected in the pipeline, these
 overlapping regions will grow larger, and may become tens of pixels wide in
 the final regions requested to the reader. This progressive enlargement of the
 overlapping regions is an unfortunate consequence of using filters that
 require extra boundaries, and it is an inherent characteristics of many image
 processing filters. Of course, there are extreme cases to this situation.
 There are the nice filters that are pixel-wise in nature and that will request
 an input region that is identical to their output region. There are also the
 filters that are not well suited for streaming at all and that will request
 the entire image at input, regardless of how small the output region might be.

Figure~\ref{fig:StreamingConceptDiagram} illustrates the basic concept of data
streaming in a $2D$ case in which the input image is divided into four blocks.
The decision on how the image blocks are going to be requested is made by the
\doxygen{StreamingImageFilter} at the end of the pipeline. More specifically,
this filter use a helper class called the \doxygen{ImageRegionSplitter}.
Different implementations of splitter will cut the image using different
policies. For example, some splitters may cut the images in a slice-oriented
fashion, while other splitters may subdivided the image region into smaller
cubes. Users could implement customized splitting policies by writing their own
image region splitters and connecting them to the StreamingImageFilter.

The Streaming image filter recompose the output image by pasting together all
the image blocks. Its output region is, therefore, as large as the size that
the output image would have been if the pipeline would have taken as input the
entire image in a single block.

The basic infrastructure of the image IO architecture in ITK is described in
chapter 7 of the ITK Software Guide \cite{ITKSoftwareGuideSecondEdition}.

\begin{figure}
\center
\includegraphics[width=1.0\textwidth]{StreamingConceptDiagram.eps}
\itkcaption[Streaming Concept Diagram]{Basic concept of streaming illustrated
in the case of a $2D$ image. The StreamingImageFilter requests sub-regions of
the output image region, the upstream filter convey those request to their
predecessors until the request reaches the data source of the pipeline, that is
typically an image reader. The reader provides the requested chunk of data, and
the processing starts going forwards in the pipeline. The image chunks are
processed one after another, and the final result is recomposed in the output
of the StreamingImageFilter.}
\label{fig:StreamingConceptDiagram}
\end{figure}



\subsection{What was missing in the ImageFileReader}

The limitation with the current IO infrastructure of ITK is that, when the
request for subregions reached the ImageFileReader and ImageIO classes, it was
ignored. Regardless of how much of the image is being requested the reader is
always providing the entire image as output. Depending on how big the input
image is, this behavior can easily defeat the entire purpose of the streaming,
and prevent users from processing large datasets. Certainly datasets with sizes
similar to the typical modern workstation, one to four gigabytes, would be
impossible to load in a pipeline, due to the curren limitation of the IO
infrastructure.

Fortunately, the modifications needed for making this last step of the
streaming work properly, were rather minor. In fact, that IO infrastructure was
already aware of the notion of streaming, and it was simply missing to perform
several key steps needed to honor the request of the filters down the pipeline.

\begin{itemize}
\item The first of those steps, is that, during the pipeline negociations, the
\doxygen{ImageFileReader} had the opportunity to indicate how much of a region
it will provide if asked for a given requested region. This was done in the
\code{EnlargeOutputRequestedRegion()}. The current implementation of this method
was reporting that requested region should be enlarged to be equal to the largest
possible region.

\item The second step was at the top of the \code{GenerateData()} method of the ImageFileReader
where the previously enlarged requested region is used to allocate the buffer of the output
image and it is set as the IO region in the ImageIO class that is actually going to read
the image into memory.

\item The third step was in the \code{Read} method of every ImageIO class, where they should have
checked for the size of the \code{ImageIORegion} and load only that size of data into the
buffer.
\end{itemize}

These three steps has been fixed in different ways in the code that is attached
to this paper, as follows:


\begin{itemize}
\item First, the ImageFileReader now invokes a new method of the ImageIO family
in which every ImageIO class reports what would be the appropriate region that
the ImageIO could return if asked for a specific requested region. This new
method is called \code{GenerateStreamableReadRegionFromRequestedRegion()}. A
default implementation of this virtual method is provided in the ImageIOBase
class, and returns by default the LargestPossibleRegion of the image. In other
words, the default behavior is \textbf{not to stream}. Specific ImageIO classes
will provide overload implementation of this method returning the regions that
are natural for their encoding.

\item The new enlarged regions, are still taken in the GenerateData() method of
the ImageFileReader and used for allocating the buffer of the output image, as
well as, for being passes as the ImageIORegion to the ImageIO class.

\item The Read method of the ImageIO classes are now getting the specification
of the ImageIORegion that the ImageFileReader set and are honoring that request
by loading into memory only that region of the image. Of course, the details on
how every ImageIO will grab chunks of data from the image in a file depend on
the specifica characteristics of the file format, and most likely on the
intermediate helper library used to interact with the files. For example, the
way the MetaImageIO class loads pieces of the image, will depend on how the
actual MetaImage library provides that functionality at the low level.
\end{itemize}

Since not all image file formats are well suited for supporting streaming, the
functionality must be configured at the top level in the IO infrastructure,
tested in several specific ImageIO classes, and progressively be implemented in
most othere file formats. As a proof of concept, the streaming capabilities
were implemented here for four different file formats: YAFF, FITS, Nifti and
MetaImage. The implementation details are described in the following sections.

\subsection{YAFF Reader}
\subsection{FITS Reader}
\subsection{Nifti Reader}
\subsection{MetaImage Reader}

\section{Tests and Examples}

A group of tests are included with the code attached to this paper. These tests
verify the basic functionality of the streaming capabilties in the modified IO
infrastructure.

The list of test can be found by using the \code{ctest -N} command on the binary
directory that results from configuring and building the code. The output of this
command should look like:

\begin{verbatim}
ctest -N                                                                                                                          ~/bin/NAMIC/NAMICSandbox/ImageIOStreaming
Start processing tests
Test project /home/ibanez/bin/NAMIC/NAMICSandbox/ImageIOStreaming
  1/  8 Testing KWStyleTest
  2/  8 Testing ImageIOStreamingTest01
  3/  8 Testing ImageIOStreamingTest02
  4/  8 Testing ImageIOStreamingTest03
  5/  8 Testing ImageStreamedFilteringTest01
  6/  8 Testing ImageStreamedFilteringTest02
  7/  8 Testing ImageStreamedFilteringTest03
  8/  8 Testing ImageStreamedFilteringTest04
\end{verbatim}

Individual tests can be executed with the command \code{ctest -V -R testname}.
Where \emph{testname} is any of the names listed on the rightmost column of the
output presented above.

\subsection{ImageIOStreamingTest01}
This tests uses a small 3D image from the Insight/Examples/Data directory in
order to illustrate how the streaming capabilities behave in a pipeline
composed of one reader, the RegionOfInterestImageFilter, and a writer.  In this
simplified scenario, we use the RegionOfInterestImageFilter as a tool for
selecting a custom \code{RequestedImage} from the command line. The typical 
command line will look like:

\begin{verbatim}
ImageIOStreamingTest inputImage.yaff  outputImage.mha  15 25 3  50 60 40
\end{verbatim}

Where the input image is supposed to be in one of the file format that supports
streaming, the output image is in any of the ITK supported formats, the next three 
numbers are the image index of the first pixel of the region to be extracted, and 
the last three numbers are the size in pixels of the region of interest.

When runing this test, you should be able to verify that the reader only brings
into memory the piece of the image that corresponds to the requested reqion
equal to the output region of the \code{RegionOfInterestImageFilter}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Insert the bibliography using BibTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliographystyle{plain}
\bibliography{InsightJournal}


\end{document}

