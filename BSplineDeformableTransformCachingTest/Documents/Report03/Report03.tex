\documentclass{InsightArticle}

\usepackage[dvips]{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  hyperref should be the last package to be loaded.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[dvips,
bookmarks,
bookmarksopen,
backref,
colorlinks,linkcolor={blue},citecolor={blue},urlcolor={blue},
]{hyperref}


\title{Viola-Wells Mutual Information Metric GetValueAndDerivative Performance - Threaded vs. Unthreaded}

\release{1.00}

\input{authors}

\begin{document}


\ifpdf
\else
   %
   % Commands for including Graphics when using latex
   % 
   \DeclareGraphicsExtensions{.eps,.jpg,.gif,.tiff,.bmp,.png}
   \DeclareGraphicsRule{.jpg}{eps}{.jpg.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.gif}{eps}{.gif.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.tiff}{eps}{.tiff.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.bmp}{eps}{.bmp.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.png}{eps}{.png.bb}{`convert #1 eps:-}
\fi


\maketitle


\ifhtml
\chapter*{Front Matter\label{front}}
\fi


% The abstract should be a paragraph or two long, and describe the
% scope of the document.
\begin{abstract}
\noindent
This report compares the execution time and memory usage of the default ITK implementation of Viola-Wells Mutual Information
and a multi-threaded implementation of Viola-Wells Mutual Information. The comparison is done in the context of b-spline
deformable registration.
\end{abstract}

\tableofcontents

\section{Introduction}



\section{Components used in Deformable Image Registration}

When performing image registration in ITK with a deformable Trasnsform, the
following combination of components is usually employed.

\begin{itemize}
\item \doxygen{MattesMutualInformationImageToImageMetric}
\item \doxygen{BSplineDeformableTransform}
\item \doxygen{LFBGSBOptimizer}
\item \doxygen{LinearInterpolator}
\end{itemize}

Of course, variations of this combinations are also possible, and sometimes
desirable. However, for the purpose of this study we focus on this particular
combination of software components.

The ITK implementation of Viola-Wells Mutual Information Metric is based on the
description of the algorimth presented in \cite{}. Its
usage is described in the ITK Software Guide
\cite{ITKSoftwareGuideSecondEdition}.


\section{Critical Parameters in Deformable Registration}
\label{sec:CriticalParameters}

Given the combination of software components described above, the following
items are the critical parameters that will dominate the memory requirements
and computational time of the registration.

\begin{itemize}
\item \textbf{NSS}: Number of Spatial Samples
\item \textbf{NBN}: Number of BSpline Grid Nodes
\item \textbf{BSO}: Order of the BSpline
\item \textbf{ID}:  Image Dimension
\end{itemize}

\subsection{Number of Spatial Samples}

The \emph{Samples} are a set of randomly selected pixels from the Fixed image,
that are used as a \emph{representative set} for the entire image.

The number of spatial samples is usually computed as a fraction of the total
number of pixels in the input image.  In the case of the Mattes Mutual
Information metric implementation, these samples are selected at the very
beginning of the registration process and they remain unchanged during the
subsequent iterations of the registration. In the case of the Viola-Wells
implementation of Mutual Information, the samples are selected again at every
iteration of the optimizer. This makes possible in the Viola-Wells
implementation to use a much smaller set of samples than in the Mattes
implementation.

It is typical, in the Mattes implementation, to use a number of samples in the
range of $1\%$ to $20\%$ of the Fixed image pixels. Using a number of spatial
samples that is too low will result in noisy evaluations of the Metric and
therefore in an unstable behavior of the Optimizer when it is walking the
parametric space of the Transform. In some cases, it may be desirable to raise
the number of spatial samples above the $20\%$ of the image pixels, in order to
gain stability, but this is done at the expense of memory consumption and
computation time.

\subsection{Number of BSpline Grid Nodes}

A BSpline deformable transform is basically defined as BSpline interpolation
computed from a collection of nodes in which deformation vectors are known.
The BSpline transform parameters are the collection of components from the
deformation vectors located at the grid nodes. In the context of the ITK image
registration framework, finding an optimal combination of this set of
parameters is the final goal of the registration process.

\subsection{Order of the BSpline}
\label{sec:BSplineOrder}

BSplines interpolation kernels can be seen as sucessive convolutions of the box
car kernel. The box car kernel is considered to be a BSpline interpolator of
order zero. A common interpolation order ot use is $3$, and it is commonly
referred as \emph{cubic} BSpline interpolation.

The most significant effect of the BSpline order is that it defines the support
size for the interpolation kernel. In a one-dimensional grid, a zero order
BSpline will have kernel whose support has the same size as the separation
between two consecutive grid nodes. A first order BSpline will have a kernel
with a support that is twice as long as the support for order zero. In general,
the support size of a BSpline kernel will be equal to the BSpline order plus
one, times the separation between two consecutive grid nodes.

When a BSpline is used in $N$ dimensions, then the number of grid nodes
included in its support becomes equal to the one-dimensional support to the
power of the dimension. For example, the support of a cubic BSpline in $2D$ is
equal to $4^2=16$ nodes, and in $3D$ is equal to $4^3=64$ nodes. This factor
has important implications in memory consumption, as it is described in section
\ref{sec:MemoryRequirements}.

\subsection{Image Dimension}

The image dimension, independently of the image size (number of pixels along
each dimension), is significant because it defines how many BSpline grid nodes
can affect the location of a given point in space. See description in section
\ref{sec:BSplineOrder}.

\section{Memory Requirements}
\label{sec:MemoryRequirements}

Our review of the ITK implementation of the Mattes Mutual Information Metric
revealed several relationships between memory requirements and the critical
parameters listed in the previous section.

\section{Software Requirements}

In order to reproduce the results described in this report you need to have the
following software installed:

% The {itemize} environment uses a bullet for each \item.  If you want the 
% \item's numbered, use the {enumerate} environment instead.
\begin{itemize}
  \item  Insight Toolkit 3.4.
  \item  CMake 2.6
\end{itemize}

\section{Results}

The results described in this section can be reproduced by running the Tests in
the \code{Testing} subdirectory of this report.

\input{ExperimentInformation}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{ThreadingTiming.pdf}
\caption{Timing Results for GetValueAndDerivative Calls}
\label{fig:GetValueTiming}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{ThreadingMemory.pdf}
\label{fig:GetValueMemory}
\caption{Memory Usage for GetValueAndDerivative Calls}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{ThreadingTiming2.pdf}
\caption{Timing results for GetValueAndDerivative using the new implementation.}
\label{fig:GetValueTiming2}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{ThreadingMemory2.pdf}
\label{fig:GetValueMemory2}
\caption{Memory usage for GetValueAndDerivative using the new implementation.}
\end{figure}


\input{SystemInformation}

\end{document}

