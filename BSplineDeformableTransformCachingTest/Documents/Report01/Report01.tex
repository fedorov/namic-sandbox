\documentclass{InsightArticle}

\usepackage[dvips]{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  hyperref should be the last package to be loaded.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[dvips,
bookmarks,
bookmarksopen,
backref,
colorlinks,linkcolor={blue},citecolor={blue},urlcolor={blue},
]{hyperref}


\title{Estimation of Memory and Computation Time Trade-Offs for Deformable
Registration Based on Mattes Mutual Information and BSpline Deformable
Transforms}

\release{1.00}

\include{authors}

\begin{document}


\ifpdf
\else
   %
   % Commands for including Graphics when using latex
   % 
   \DeclareGraphicsExtensions{.eps,.jpg,.gif,.tiff,.bmp,.png}
   \DeclareGraphicsRule{.jpg}{eps}{.jpg.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.gif}{eps}{.gif.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.tiff}{eps}{.tiff.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.bmp}{eps}{.bmp.bb}{`convert #1 eps:-}
   \DeclareGraphicsRule{.png}{eps}{.png.bb}{`convert #1 eps:-}
\fi


\maketitle


\ifhtml
\chapter*{Front Matter\label{front}}
\fi


% The abstract should be a paragraph or two long, and describe the
% scope of the document.
\begin{abstract}
\noindent
This document describes the most significant trade-offs between memory
consumption and computation time that must be considered when performing
deformable registration using the Mattes Mutual Information Metric and BSpline
Deformable Transform classes available in ITK.

This paper is accompanied with the source code, input data, parameters and
output data that we used for validating the algorithm described in this paper.
This adheres to the fundamental principle that scientific publications must
facilitate \textbf{reproducibility} of the reported results.
\end{abstract}

\tableofcontents

\section{Introduction}

Deformable registration of images is a computationally demanding task. It
usually involves a large number of parameters that users must select, and for
which, there is not always a clear set of guidelines available. This report
provides some of such guidelines. In particular we are interested on analyzing
how the choices of specific parameters affects the memory requirements of the
registration versus the time that it will require to run.


\section{Components used in Deformable Image Registration}

When performing image registration in ITK with a deformable Trasnsform, the
following combination of components is usually employed.

\begin{itemize}
\item \doxygen{MattesMutualInformationImageToImageMetric}
\item \doxygen{BSplineDeformableTransform}
\item \doxygen{LFBGSBOptimizer}
\item \doxygen{LinearInterpolator}
\end{itemize}

Of course, variations of this combinations are also possible, and sometimes
desirable. However, for the purpose of this study we focus on this particular
combination of software components.

The ITK implementation of Mattes Mutual Information Metric is based on the
description of the algorimth presented in \cite{Mattes2001,Mattes2003}. Its
usage is described in the ITK Software Guide
\cite{ITKSoftwareGuideSecondEdition}.


\section{Critical Parameters in Deformable Registration}
\label{sec:CriticalParameters}

Given the combination of software components described above, the following
items are the critical parameters that will dominate the memory requirements
and computational time of the registration.

\begin{itemize}
\item \textbf{NSS}: Number of Spatial Samples
\item \textbf{NHB}: Number of Histogram Bins
\item \textbf{NBN}: Number of BSpline Grid Nodes
\item \textbf{BSO}: Order of the BSpline
\item \textbf{ID}:  Image Dimension
\end{itemize}

\subsection{Number of Spatial Samples}

The \emph{Samples} are a set of randomly selected pixels from the Fixed image,
that are used as a \emph{representative set} for the entire image.

The number of spatial samples is usually computed as a fraction of the total
number of pixels in the input image.  In the case of the Mattes Mutual
Information metric implementation, these samples are selected at the very
beginning of the registration process and they remain unchanged during the
subsequent iterations of the registration. In the case of the Viola-Wells
implementation of Mutual Information, the samples are selected again at every
iteration of the optimizer. This makes possible in the Viola-Wells
implementation to use a much smaller set of samples than in the Mattes
implementation.

It is typical, in the Mattes implementation, to use a number of samples in the
range of $1\%$ to $20\%$ of the Fixed image pixels. Using a number of spatial
samples that is too low will result in noisy evaluations of the Metric and
therefore in an unstable behavior of the Optimizer when it is walking the
parametric space of the Transform. In some cases, it may be desirable to raise
the number of spatial samples above the $20\%$ of the image pixels, in order to
gain stability, but this is done at the expense of memory consumption and
computation time.

\subsection{Number of Histogram Bins}

Mutual Information is computed based on the joint histogram of the Fixed and
Moving images. The number of bins used in this Histogram is a critical
parameter of the Metric because it influences the precision at which the Joint
Entropy of the images is estimated.

In principle, the number of bins required for a reliable estimation of Joint 
Histogram could be computed based on the Entropy of each one of the images.

However, users, must note that this should be restricted to the \emph{useful}
section of the intensity distributions of the Fixed and Moving images. For
example, when registering PET images to MRI images, it is desirable to exclude
the high intensities from the PET image since those values relate to functional
and metabolic information rather than carrying the anatomical and structural
information that we could expect to be correlated with the MRI image. In this
situation, the brightest pixels from the PET image should be excluded, and the
Entropy of the remaining intensity values is the one that should be used for
choosing the number of bins to use in the joint histogram.

A balance should be kept between the number of histogram bins and the number of
samples in order to have a meaningful estimation of mutual information. The
number of samples should be large enough to populate the bins, therefore, if a
user select a large number of bins, this may impose a demand on using also a
large number pixel samples to be able to populate that histogram. The number of
bins provided to the metric result in a square number of actual bins in the joint
histogram. Therefore, doubling the parameter of number of bins passed to the
metric, may require to multiply by four the number of sample pixels to be drawn
for the image, in order to have the same level of quality in the estimation of
mutual information.


\subsection{Number of BSpline Grid Nodes}

A BSpline deformable transform is basically defined as BSpline interpolation
computed from a collection of nodes in which deformation vectors are known.
The BSpline transform parameters are the collection of components from the
deformation vectors located at the grid nodes. In the context of the ITK image
registration framework, finding an optimal combination of this set of
parameters is the final goal of the registration process.

\subsection{Order of the BSpline}
\label{sec:BSplineOrder}

BSplines interpolation kernels can be seen as sucessive convolutions of the box
car kernel. The box car kernel is considered to be a BSpline interpolator of
order zero. A common interpolation order ot use is $3$, and it is commonly
referred as \emph{cubic} BSpline interpolation.

The most significant effect of the BSpline order is that it defines the support
size for the interpolation kernel. In a one-dimensional grid, a zero order
BSpline will have kernel whose support has the same size as the separation
between two consecutive grid nodes. A first order BSpline will have a kernel
with a support that is twice as long as the support for order zero. In general,
the support size of a BSpline kernel will be equal to the BSpline order plus
one, times the separation between two consecutive grid nodes.

When a BSpline is used in $N$ dimensions, then the number of grid nodes
included in its support becomes equal to the one-dimensional support to the
power of the dimension. For example, the support of a cubic BSpline in $2D$ is
equal to $4^2=16$ nodes, and in $3D$ is equal to $4^3=64$ nodes. This factor
has important implications in memory consumption, as it is described in section
\ref{sec:MemoryRequirements}.

\subsection{Image Dimension}

The image dimension, independently of the image size (number of pixels along
each dimension), is significant because it defines how many BSpline grid nodes
can affect the location of a given point in space. See description in section
\ref{sec:BSplineOrder}.

\section{Memory Requirements}
\label{sec:MemoryRequirements}

Our review of the ITK implementation of the Mattes Mutual Information Metric
revealed several relationships between memory requirements and the critical
parameters listed in the previous section.

\subsection{Data Structures}

Memory allocation is dominated by the following data structures used inside the
\doxygen{MattesMutualInformationImageToImageMetric}. They are not listed in a 
particular order, given that their actual memory demands depend on the specific
numerical values of the critical parameters.


\begin{itemize}
\item \textbf{BIWS}: BSpline Interpolation Weights per Sample
\item \textbf{BNIS}: BSpline Node Indices per sample
\item \textbf{PDFD}: Probability Density Function Derivatives
\end{itemize}


\subsubsection{BSpline Interpolation Weights per Sample}
\subsubsection{BSpline Node Indices per Sample}
\subsubsection{PDF Derivatives}

\subsection{Memory Relationships}

The relationships between the critical parameters and the memory footprint of
the data structures are listed below.

\begin{equation}
\mbox{BIWS} = \mbox{BSO+1}^{\mbox{ID}} \times \mbox{NSS} \times \mbox{sizeof(double)}
\end{equation}

\begin{equation}
\mbox{BNIS} = \mbox{BSO+1}^{\mbox{ID}} \times \mbox{NSS} \times \mbox{sizeof(long int)}
\end{equation}

\begin{equation}
\mbox{PDFD} = \mbox{NBN} \times \mbox{ID} \times \mbox{NHB} \times  \mbox{sizeof(double)}
\end{equation}


\subsubsection{Numerical Examples}

In order to put in context the relationships listed above we provide here some
concrete numerical examples.

\begin{itemize}
\item Image Size: 256 256 20
\item BSpline Order: 3
\item Image Dimension: 3
\item BSpline Grid: 32 32 16
\item Percent of Samples: $20\%$
\end{itemize}

With these parameters, the memory allocations for significant data structures
are the following:


\begin{itemize}
\item BIWS: $(3+1)^3 \times (256 \times 256 \times 200 ) \times 0.2 \times 8 $ = 1.34 Gigabytes 
\item BNIS: $(3+1)^3 \times (256 \times 256 \times 200 ) \times 0.2 \times 4 $ = 0.67 Gigabytes 
\item PDFD: $(32 \time 32 \times 16 ) \times 3 \times ( 50 \times 50 ) \times 8$ = 0.96 Gigabytes
\end{itemize}


\section{Software Requirements}

In order to reproduce the results described in this report you need to have the
following software installed:

% The {itemize} environment uses a bullet for each \item.  If you want the 
% \item's numbered, use the {enumerate} environment instead.
\begin{itemize}
  \item  Insight Toolkit 3.4.
  \item  CMake 2.6
\end{itemize}


\appendix

\section{Results}

The results described in this section can be reproduced by running the Tests in
the \code{Testing} subdirectory of this report.

\begin{figure}[ht]
\centering
\includegraphics[width=0.85\textwidth]{CachingTiming.pdf}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=0.85\textwidth]{CachingMemory.pdf}
\end{figure}

\include{SystemInformation}

\end{document}

