
#ifndef __itkReplaceSpecialFunctions_h
#define __itkReplaceSpecialFunctions_h

/// Evaluates the Associated Legendre Polynomial with parameters (l,m) at x.
double LegendreP( int l, int m, double x );
///Compute the Binomial coefficient
int binomialCoeff( int n, int k );

#endif //__itkReplaceSpecialFunctions_h
