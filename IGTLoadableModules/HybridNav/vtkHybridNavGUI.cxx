/*==========================================================================

  Portions (c) Copyright 2008 Brigham and Women's Hospital (BWH) All Rights Reserved.

  See Doc/copyright/copyright.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Program:   3D Slicer
  Module:    $HeadURL: $
  Date:      $Date: $
  Version:   $Revision: $

==========================================================================*/

#include "vtkObject.h"
#include "vtkObjectFactory.h"

#include "vtkHybridNavGUI.h"
#include "vtkSlicerApplication.h"
#include "vtkSlicerModuleCollapsibleFrame.h"
#include "vtkSlicerSliceControllerWidget.h"
#include "vtkSlicerSliceGUI.h"
#include "vtkSlicerSlicesGUI.h"

#include "vtkSlicerColor.h"
#include "vtkSlicerTheme.h"

#include "vtkKWTkUtilities.h"
#include "vtkKWWidget.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWFrame.h"
#include "vtkKWLabel.h"
#include "vtkKWEvent.h"
#include "vtkKWCheckButton.h"

#include "vtkKWPushButton.h"
#include "vtkKWEntry.h"
#include "vtkSlicerNodeSelectorWidget.h"
#include "vtkKWMultiColumnListWithScrollbars.h"
#include "vtkKWMultiColumnList.h"

#include "vtkCornerAnnotation.h"
#include "vtkPivotCalibration.h"
#include "vtkMRMLHybridNavToolNode.h"
#include "vtkMRMLModelNode.h"

//---------------------------------------------------------------------------
vtkStandardNewMacro (vtkHybridNavGUI );
vtkCxxRevisionMacro ( vtkHybridNavGUI, "$Revision: 1.0 $");
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
vtkHybridNavGUI::vtkHybridNavGUI ( )
{

  //----------------------------------------------------------------
  // Logic values
  this->Logic = NULL;
  this->DataCallbackCommand = vtkCallbackCommand::New();
  this->DataCallbackCommand->SetClientData( reinterpret_cast<void *> (this) );
  this->DataCallbackCommand->SetCallback(vtkHybridNavGUI::DataCallback);

  //----------------------------------------------------------------
  // GUI widgets

  //----------------------------------------------------------------
  //Tools Frame
  this->ToolList = NULL;
  this->AddToolButton = NULL;
  this->DeleteToolButton = NULL;
  this->ToolNameEntry = NULL;
  this->ToolNodeSelectorMenu = NULL;
  this->ToolDescriptionEntry = NULL;
  this->ToolCheckButton = NULL;

  //----------------------------------------------------------------
  //Pivot Calibration Frame
  this->CalibrationNodeSelectorMenu = NULL;
  this->StartCalibrateButton = NULL;
  this->numPointsEntry = NULL;
  this->CalibrationResult = NULL;
  this->CalibrationError = NULL;

  //----------------------------------------------------------------
  //Manual Calibration Frame
  this->ObjectiveTransformNodeSelectorMenu = NULL;
  this->CurrentTransformNodeSelectorMenu = NULL;
  this->ManualCalibrateButton = NULL;

  //----------------------------------------------------------------
  // Variables
  this->pivot = vtkPivotCalibration::New();
  Calibrating = 0;

  //----------------------------------------------------------------
  // Locator  (MRML)  //----------------------------------------------------------------
  // GUI widgets
  //----------------------------------------------------------------
  this->TimerFlag = 0;

}

//---------------------------------------------------------------------------
vtkHybridNavGUI::~vtkHybridNavGUI ( )
{

  //----------------------------------------------------------------
  // Remove Callbacks

  if (this->DataCallbackCommand)
    {
    this->DataCallbackCommand->Delete();
    }

  //----------------------------------------------------------------
  // Remove Observers

  this->RemoveGUIObservers();

  //----------------------------------------------------------------
  // Remove GUI widgets

  //----------------------------------------------------------------
  //Tools Frame
  if (this->ToolList)
    {
    this->ToolList->SetParent(NULL);
    this->ToolList->Delete();
    }
  if (this->AddToolButton)
    {
    this->AddToolButton->SetParent(NULL);
    this->AddToolButton->Delete();
    }
  if (this->DeleteToolButton)
    {
    this->DeleteToolButton->SetParent(NULL);
    this->DeleteToolButton->Delete();
    }
  if (this->ToolNameEntry)
    {
    this->ToolNameEntry->SetParent(NULL);
    this->ToolNameEntry->Delete();
    }
  if (this->ToolNodeSelectorMenu)
    {
    this->ToolNodeSelectorMenu->SetParent(NULL);
    this->ToolNodeSelectorMenu->Delete();
    }
  if (this->ToolDescriptionEntry)
    {
    this->ToolDescriptionEntry->SetParent(NULL);
    this->ToolDescriptionEntry->Delete();
    }
  if (this->ToolCheckButton)
    {
    this->ToolCheckButton->SetParent(NULL);
    this->ToolCheckButton->Delete();
    }

  //-----------------------------------------------------------------
  //Pivot Calibration Frame
  if (this->CalibrationNodeSelectorMenu)
    {
    this->CalibrationNodeSelectorMenu->SetParent(NULL);
    this->CalibrationNodeSelectorMenu->Delete();
    }

  if (this->numPointsEntry)
    {
    this->numPointsEntry->SetParent(NULL);
    this->numPointsEntry->Delete();
    }

  if (this->CalibrationResult)
    {
    this->CalibrationResult->SetParent(NULL);
    this->CalibrationResult->Delete();
    }

  if (this->CalibrationError)
    {
    this->CalibrationError->SetParent(NULL);
    this->CalibrationError->Delete();
    }

  if (this->StartCalibrateButton)
    {
    this->StartCalibrateButton->SetParent(NULL);
    this->StartCalibrateButton->Delete();
    }


  //-----------------------------------------------------------------
  //Manual Calibration Frame
  if (this->ObjectiveTransformNodeSelectorMenu)
    {
    this->ObjectiveTransformNodeSelectorMenu->SetParent(NULL);
    this->ObjectiveTransformNodeSelectorMenu->Delete();
    }

  if (this->CurrentTransformNodeSelectorMenu)
    {
    this->CurrentTransformNodeSelectorMenu->SetParent(NULL);
    this->CurrentTransformNodeSelectorMenu->Delete();
    }

  if (this->ManualCalibrateButton)
    {
    this->ManualCalibrateButton->SetParent(NULL);
    this->ManualCalibrateButton->Delete();
    }

  //----------------------------------------------------------------
  // Variables
  //----------------------------------------------------------------
  if (this->pivot)
    {
      this->pivot->Delete();
    }


  //----------------------------------------------------------------
  // Unregister Logic class

  this->SetModuleLogic ( NULL );

}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::Init()
{
  vtkMRMLScene* scene = this->GetMRMLScene();

  //Register new node type to the scene
  vtkMRMLHybridNavToolNode* toolNode = vtkMRMLHybridNavToolNode::New();
  scene->RegisterNodeClass(toolNode);
  toolNode->Delete();

}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::Enter()
{
  // Fill in
  //vtkSlicerApplicationGUI *appGUI = this->GetApplicationGUI();

  if (this->TimerFlag == 0)
    {
    this->TimerFlag = 1;
    this->TimerInterval = 100;  // 100 ms
    ProcessTimerEvents();
    }
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::Exit ( )
{
  // Fill in
}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::PrintSelf ( ostream& os, vtkIndent indent )
{
  this->vtkObject::PrintSelf ( os, indent );

  os << indent << "HybridNavGUI: " << this->GetClassName ( ) << "\n";
  os << indent << "Logic: " << this->GetLogic ( ) << "\n";
}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::RemoveGUIObservers ( )
{
  //vtkSlicerApplicationGUI *appGUI = this->GetApplicationGUI();

  //-------------------------------------------------------------------------
  //Tools Frame
  if (this->ToolList && this->ToolList->GetWidget())
    {
    this->ToolList->GetWidget()
      ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->AddToolButton)
    {
    this->AddToolButton
       ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->DeleteToolButton)
    {
    this->DeleteToolButton
       ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->ToolNameEntry)
    {
    this->ToolNameEntry
       ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->ToolNodeSelectorMenu)
    {
    this->ToolNodeSelectorMenu
       ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->ToolDescriptionEntry)
    {
    this->ToolDescriptionEntry
       ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->ToolCheckButton)
    {
    this->ToolCheckButton
       ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }

  //--------------------------------------------------------------------------
  //Pivot Calibration Frame
  if (this->numPointsEntry)
    {
    this->numPointsEntry
      ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->CalibrationResult)
    {
    this->CalibrationResult
      ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->CalibrationError)
    {
    this->CalibrationError
      ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->StartCalibrateButton)
    {
    this->StartCalibrateButton
      ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }
  if (this->CalibrationNodeSelectorMenu)
    {
    this->CalibrationNodeSelectorMenu
      ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }

  //--------------------------------------------------------------------------
  //Manual Calibration Frame
  if (this->ManualCalibrateButton)
    {
    this->ManualCalibrateButton
      ->RemoveObserver((vtkCommand *)this->GUICallbackCommand);
    }

  this->RemoveLogicObservers();

}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::AddGUIObservers ( )
{
  this->RemoveGUIObservers();
 // vtkSlicerApplicationGUI *appGUI = this->GetApplicationGUI();

  //----------------------------------------------------------------
  // MRML

  vtkIntArray* events = vtkIntArray::New();
  events->InsertNextValue(vtkMRMLScene::NodeAddedEvent);
  events->InsertNextValue(vtkMRMLScene::NodeRemovedEvent);
  events->InsertNextValue(vtkMRMLScene::SceneCloseEvent);

  if (this->GetMRMLScene() != NULL)
    {
    this->SetAndObserveMRMLSceneEvents(this->GetMRMLScene(), events);
    }
  events->Delete();

  //----------------------------------------------------------------
  // GUI Observers

  //----------------------------------------------------------------
  //Tool Frame
  this->ToolList->GetWidget()
    ->AddObserver(vtkKWMultiColumnList::SelectionChangedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->AddToolButton
   ->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->DeleteToolButton
   ->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->ToolNameEntry
     ->AddObserver(vtkKWEntry::EntryValueChangedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->ToolNodeSelectorMenu
    ->AddObserver(vtkSlicerNodeSelectorWidget::NodeSelectedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->ToolDescriptionEntry
     ->AddObserver(vtkKWEntry::EntryValueChangedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->ToolCheckButton
     ->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand *)this->GUICallbackCommand);

  //----------------------------------------------------------------
  //Pivot Calibration frame
  this->CalibrationNodeSelectorMenu
    ->AddObserver(vtkSlicerNodeSelectorWidget::NodeSelectedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->numPointsEntry
     ->AddObserver(vtkKWEntry::EntryValueChangedEvent, (vtkCommand *)this->GUICallbackCommand);
  this->StartCalibrateButton
    ->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand *)this->GUICallbackCommand);
  
  //----------------------------------------------------------------
  //Manual Calibration frame
  this->ManualCalibrateButton
    ->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand *)this->GUICallbackCommand);
    
  this->AddLogicObservers();

}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::RemoveLogicObservers ( )
{
  if (this->GetLogic())
    {
    this->GetLogic()->RemoveObservers(vtkCommand::ModifiedEvent,
                                      (vtkCommand *)this->LogicCallbackCommand);
    }
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::AddLogicObservers ( )
{
  this->RemoveLogicObservers();

  if (this->GetLogic())
    {
    this->GetLogic()->AddObserver(vtkHybridNavLogic::StatusUpdateEvent,
                                  (vtkCommand *)this->LogicCallbackCommand);
    }
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::HandleMouseEvent(vtkSlicerInteractorStyle *style)
{
}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::ProcessGUIEvents(vtkObject *caller,
                                         unsigned long event, void *callData)
{

  const char *eventName = vtkCommand::GetStringFromEventId(event);

  if (strcmp(eventName, "LeftButtonPressEvent") == 0)
    {
    vtkSlicerInteractorStyle *style = vtkSlicerInteractorStyle::SafeDownCast(caller);
    HandleMouseEvent(style);
    return;
    }

  //--------------------------------------------------------------------------
  //Tool Frame
  
  //Select Element in the Tool List Frame
  else if (this->ToolList->GetWidget() == vtkKWMultiColumnList::SafeDownCast(caller)
           && event == vtkKWMultiColumnList::SelectionChangedEvent)
    {
    std::cerr << "Tool selected" << std::endl;
    int selected = this->ToolList->GetWidget()->GetIndexOfFirstSelectedRow();
    UpdateToolPropertyFrame(selected);
    }
  
  //Add a Tool to the Tool List Frame
  else if (this->AddToolButton == vtkKWPushButton::SafeDownCast(caller)
           && event == vtkKWPushButton::InvokedEvent)
    {
    std::cerr << "Tool has been added" << std::endl;
    //Create Tool object, populate list and add node to scene
    vtkMRMLHybridNavToolNode* tool = vtkMRMLHybridNavToolNode::New();
    tool = this->GetLogic()->CreateToolModel(tool);         //Represent the tool
    this->GetMRMLScene()->AddNode(tool);
    this->GetMRMLScene()->Modified();
    tool->Modified();
    tool->Delete();
    }
  else if (this->DeleteToolButton == vtkKWPushButton::SafeDownCast(caller)
           && event == vtkKWPushButton::InvokedEvent)
    {
    std::cerr << "Tool has been deleted" << std::endl;
    int selected = this->ToolList->GetWidget()->GetIndexOfFirstSelectedRow();
      if (selected >= 0 && selected < (int)this->ToolNodeList.size())
        {
        vtkMRMLHybridNavToolNode* tnode
          = vtkMRMLHybridNavToolNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->ToolNodeList[selected]));
        if (tnode)
          {
          this->GetMRMLScene()->RemoveNode(tnode);
          this->GetMRMLScene()->Modified();
          //tnode->Delete();
          int nrow = this->ToolList->GetWidget()->GetNumberOfRows();
          if (selected >= nrow)
            {
            selected = nrow - 1;
            }
          this->ToolList->GetWidget()->SelectSingleRow(selected);
          UpdateToolPropertyFrame(selected);
          }
        }
    }
  else if (this->ToolNameEntry == vtkKWEntry::SafeDownCast(caller)
      && event == vtkKWEntry::EntryValueChangedEvent)
    {
    std::cerr << "Tool Name has been modified." << std::endl;
    int selected = this->ToolList->GetWidget()->GetIndexOfFirstSelectedRow();
    if (selected >= 0 && selected < (int)this->ToolNodeList.size())
      {
      vtkMRMLHybridNavToolNode* tool
        = vtkMRMLHybridNavToolNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->ToolNodeList[selected]));
      if (tool)
        {
        tool->SetName(this->ToolNameEntry->GetValue());
        UpdateToolList(UPDATE_SELECTED_ONLY);
        }
      }
    }
  else if (this->ToolNodeSelectorMenu == vtkSlicerNodeSelectorWidget::SafeDownCast(caller)
      && event == vtkSlicerNodeSelectorWidget::NodeSelectedEvent)
    {
    std::cerr << "Node Selector is pressed." << std::endl;
    int selected = this->ToolList->GetWidget()->GetIndexOfFirstSelectedRow();
    if (selected >= 0 && selected < (int)this->ToolNodeList.size())
      {
      vtkMRMLHybridNavToolNode* tool
        = vtkMRMLHybridNavToolNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->ToolNodeList[selected]));
      if (tool)
        {
        if (this->ToolNodeSelectorMenu->GetSelected())   //check if transform has actually been selected
          {
          tool->SetAndObserveTransformNodeID(this->ToolNodeSelectorMenu->GetSelected()->GetID());
          tool->InvokeEvent(vtkMRMLTransformableNode::TransformModifiedEvent);
          UpdateToolList(UPDATE_SELECTED_ONLY);
          }
        }
      }
    }
  else if (this->ToolDescriptionEntry == vtkKWEntry::SafeDownCast(caller)
      && event == vtkKWEntry::EntryValueChangedEvent)
    {
    std::cerr << "Tool description has been modified." << std::endl;
    int selected = this->ToolList->GetWidget()->GetIndexOfFirstSelectedRow();
    if (selected >= 0 && selected < (int)this->ToolNodeList.size())
      {
      vtkMRMLHybridNavToolNode* tool
        = vtkMRMLHybridNavToolNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->ToolNodeList[selected]));
      if (tool)
        {
        tool->SetToolDescription(this->ToolDescriptionEntry->GetValue());
        UpdateToolList(UPDATE_SELECTED_ONLY);
        }
      }
    }
  
  //ToolCheckButton pressed
  else if (this->ToolCheckButton == vtkKWCheckButton::SafeDownCast(caller)
      && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    std::cerr << "Tool CheckButton has been pressed." << std::endl;
    int selected = this->ToolList->GetWidget()->GetIndexOfFirstSelectedRow();
    if (selected >= 0 && selected < (int)this->ToolNodeList.size())
      {
      vtkMRMLHybridNavToolNode* tool
        = vtkMRMLHybridNavToolNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->ToolNodeList[selected]));
      if (tool)
        {
        this->GetLogic()->SetVisibilityOfToolModel(tool, this->ToolCheckButton->GetSelectedState());
        UpdateToolList(UPDATE_SELECTED_ONLY);
        }
      }
    }
  //--------------------------------------------------------------------------
  //Calibration Frame
  //Events related with changing numPoints text box
  else if (this->numPointsEntry == vtkKWEntry::SafeDownCast(caller)
      && event == vtkKWEntry::EntryValueChangedEvent)
    {
    std::cerr << "numPoints has been modified." << std::endl;
    }

  //Events related with pressing calibration button
  else if (this->StartCalibrateButton == vtkKWPushButton::SafeDownCast(caller)
      && event == vtkKWPushButton::InvokedEvent)
    {
    //Acknowledge the button has been pressed
    std::cerr << "StartCalibrateButton is pressed." << std::endl;

    //Check if a node has been selected
    if (this->CalibrationNodeSelectorMenu->GetSelected())
      {
      vtkMRMLHybridNavToolNode* tnode = vtkMRMLHybridNavToolNode::SafeDownCast(this->CalibrationNodeSelectorMenu->GetSelected());
      if (tnode)
        {
        // Annull previous appended calibration matrix
        vtkMatrix4x4* m1 = vtkMatrix4x4::New();
        vtkMatrix4x4* cm1 = vtkMatrix4x4::New();
        tnode->GetCalibrationMatrix(cm1);
        m1->Invert(cm1, m1);
        tnode->vtkMRMLTransformableNode::ApplyTransform(m1);
        
        // Delete any previous calibration matrix
        vtkMatrix4x4* m2 = vtkMatrix4x4::New();
        m2->Identity();
        tnode->SetCalibrationMatrix(m2);
        
        // Add observer to Tracked Transform Node
        vtkIntArray* nodeEvents = vtkIntArray::New();
        nodeEvents->InsertNextValue(vtkMRMLTransformableNode::TransformModifiedEvent);
        vtkMRMLTransformNode* transformNode = tnode->GetParentTransformNode();
        vtkSetAndObserveMRMLNodeEventsMacro(transformNode,transformNode,nodeEvents);
        nodeEvents->Delete();
        //Initialize pivot calibration object
        pivot->Initialize(this->numPointsEntry->GetValueAsInt(), tnode);
        this->Calibrating = 1;

        //Clean up
        m1->Delete();
        cm1->Delete();
        m2->Delete();
        }
      }
    }
   
  //--------------------------------------------------------------------------
  // Manual Calibration Frame
  //Events pressing manual calibration button
  else if (this->ManualCalibrateButton == vtkKWPushButton::SafeDownCast(caller)
      && event == vtkKWPushButton::InvokedEvent)
    {
    //Acknowledge the button has been pressed
    std::cerr << "Manual Calibrate Button is pressed." << std::endl;

    //Check if the nodes have been selected
    if ((this->ObjectiveTransformNodeSelectorMenu->GetSelected()) && 
        (this->CurrentTransformNodeSelectorMenu->GetSelected()))
      {
      vtkMRMLHybridNavToolNode* PointerTool =
         vtkMRMLHybridNavToolNode::SafeDownCast(this->ObjectiveTransformNodeSelectorMenu->GetSelected());
      vtkMRMLHybridNavToolNode* BetaProbeTool =
         vtkMRMLHybridNavToolNode::SafeDownCast(this->CurrentTransformNodeSelectorMenu->GetSelected());
      //Perform the manual calibration
      this->GetLogic()->ManualCalibration(PointerTool, BetaProbeTool);
      }
    else
      {
      std::cerr << "Nodes not selected for calibration" << std::endl;
      return;
      }
    }
}

//----------------------------------------------------------------------------------------
void vtkHybridNavGUI::DataCallback(vtkObject *caller,
                                     unsigned long eid, void *clientData, void *callData)
{
  vtkHybridNavGUI *self = reinterpret_cast<vtkHybridNavGUI *>(clientData);
  vtkDebugWithObjectMacro(self, "In vtkHybridNavGUI DataCallback");
  self->UpdateAll();
}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::ProcessLogicEvents ( vtkObject *caller,
                                             unsigned long event, void *callData )
{

  if (this->GetLogic() == vtkHybridNavLogic::SafeDownCast(caller))
    {
    if (event == vtkHybridNavLogic::StatusUpdateEvent)
      {
      //this->UpdateDeviceStatus();
      }
    }
}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::ProcessMRMLEvents ( vtkObject *caller,
                                            unsigned long event, void *callData )
{
  // -----------------------------------------
  // Adding a new node
  if (event == vtkMRMLScene::NodeAddedEvent)
    {
    vtkObject* obj = (vtkObject*)callData;
    vtkMRMLNode* node = vtkMRMLNode::SafeDownCast(obj);
    
    //Check to see if node is from a HybridNavTool
    if (node && strcmp(node->GetNodeTagName(), "HybridNavTool") == 0)
      {
      std::cerr << "Hybrid Nav Tool Node event" << std::endl;
      vtkMRMLHybridNavToolNode* tnode = vtkMRMLHybridNavToolNode::SafeDownCast(node);
      //Add Observer to Node
      vtkIntArray* nodeEvents = vtkIntArray::New();
      nodeEvents->InsertNextValue(vtkCommand::ModifiedEvent);
      vtkSetAndObserveMRMLNodeEventsMacro(tnode,tnode,nodeEvents);
      nodeEvents->Delete();
      
      // obtain the list of tools in the scene
      UpdateToolNodeList();
      UpdateToolList(UPDATE_ALL);
      int select = this->ToolList->GetWidget()->GetNumberOfRows() - 1;
      this->ToolList->GetWidget()->SelectSingleRow(select);
      UpdateToolPropertyFrame(select);
      }
    }

  //---------------------------------------------
  // Removing a node
  else if (event == vtkMRMLScene::NodeRemovedEvent)
    {
    vtkObject* obj = (vtkObject*)callData;
    vtkMRMLNode* node = vtkMRMLNode::SafeDownCast(obj);
    
    //Check to see if node is from a HybridNavTool
    if (node && strcmp(node->GetNodeTagName(), "HybridNavTool") == 0)
      {
      std::cerr << "Tool Node to be deleted" << std::endl;
      vtkMRMLHybridNavToolNode* node = vtkMRMLHybridNavToolNode::SafeDownCast(obj);
      if (node)
        {
        UpdateToolNodeList();
        UpdateToolList(UPDATE_ALL);
        }
      }
    }

  // Detect if something has happened in the MRML scene
  if (event == vtkMRMLTransformableNode::TransformModifiedEvent)
    {
    std::cerr<< "Tool modified event called" << std::endl;
    vtkMRMLNode* node = vtkMRMLNode::SafeDownCast(caller);

    //Check to see if calibration transform node has been updated
    if ((node == vtkMRMLNode::SafeDownCast(pivot->transformNode)) && (Calibrating == 1))
      {
      std::cerr<< "Calibration Node event called" << std::endl;
      //Print node in terminal and send node to calibration vector
      int i = pivot->AcquireTransform();
      if (i == 1)
        {
        //Print out calculated Calibration Matrix
        std::cerr << "Calibration matrix: ";
        pivot->CalibrationTransform->Print(std::cerr);
        Calibrating = 0;
        vtkSetAndObserveMRMLNodeEventsMacro(node, node, NULL);    //Remove observer on selected node
        
        //Update Tool Calibration status
        pivot->toolNode->SetCalibrated(1); 
        pivot->toolNode->SetCalibrationMatrix(pivot->CalibrationTransform);  //Assign Calibration Matrix to tool node
        UpdateToolList(UPDATE_ALL);
        
        //Create linear transform node with Calibration Matrix
        //vtkMRMLLinearTransformNode* tnode = vtkMRMLLinearTransformNode::New();
        //tnode->ApplyTransform(pivot->CalibrationTransform);  //assign calibration matrix to transform
        //tnode->SetAndObserveTransformNodeID(pivot->toolNode->GetParentTransformNode()->GetID());
        //this->GetMRMLScene()->AddNode(tnode);
        //this->GetMRMLScene()->Modified();
        
        //Assign tool model with calibration matrix
        //pivot->toolNode->SetAndObserveTransformNodeID(pivot->toolNode->GetID());
        vtkMatrix4x4* CalibrationMatrix = vtkMatrix4x4::New();
        pivot->toolNode->GetCalibrationMatrix(CalibrationMatrix);
        pivot->toolNode->vtkMRMLTransformableNode::ApplyTransform(CalibrationMatrix);
        pivot->toolNode->Modified();
        this->GetMRMLScene()->Modified();
        //Assign new geometry to the tool to reflect tool tip and sensor location
        this->GetLogic()->AppendToolTipModel(pivot->toolNode);
        
        //Print result in GUI text box
        this->CalibrationResult->SetValueAsDouble(CalibrationMatrix->GetElement(2,3));
        this->CalibrationError->SetValueAsDouble(pivot->RequestCalibrationRMSE());
        CalibrationMatrix->Delete();
        }
      }
    }
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::ProcessTimerEvents()
{
  if (this->TimerFlag)
    {
    // update timer
    vtkKWTkUtilities::CreateTimerHandler(vtkKWApplication::GetMainInterp(),
                                         this->TimerInterval,
                                         this, "ProcessTimerEvents");
    }
}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::BuildGUI ( )
{

  // ---
  // MODULE GUI FRAME
  // create a page
  this->UIPanel->AddPage ( "HybridNav", "HybridNav", NULL );

  BuildGUIForHelpFrame();
  BuildGUIForToolFrame();
  BuildGUIForCalibrationFrame();
  BuildGUIForManualCalibrationFrame();
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::BuildGUIForHelpFrame ()
{
  // Define your help text here.
  const char *help =
    "See "
    "<a>http://www.slicer.org/slicerWiki/index.php/Modules:HybridNav</a> for details.";
  const char *about =
    "This work is supported by NCIGT, NA-MIC.";

  vtkKWWidget *page = this->UIPanel->GetPageWidget ( "HybridNav" );
  this->BuildHelpAndAboutFrame (page, help, about);
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::BuildGUIForToolFrame()
{

  vtkSlicerApplication *app = (vtkSlicerApplication *)this->GetApplication();
  vtkKWWidget *page = this->UIPanel->GetPageWidget ("HybridNav");

  vtkSlicerModuleCollapsibleFrame *ToolFrame = vtkSlicerModuleCollapsibleFrame::New();

  ToolFrame->SetParent(page);
  ToolFrame->Create();
  ToolFrame->SetLabelText("Tracked Tools");
  app->Script ("pack %s -side top -anchor nw -fill x -padx 2 -pady 2 -in %s",
               ToolFrame->GetWidgetName(), page->GetWidgetName());

  // ----------------------------------------------------------------------
  // Tracked Tools List Frame

  //Frame with Label
  vtkKWFrameWithLabel *listFrame = vtkKWFrameWithLabel::New();
  listFrame->SetParent(ToolFrame->GetFrame());
  listFrame->Create();
  listFrame->SetLabelText ("Tracked Tools");
  app->Script ( "pack %s -fill both -expand true",
                listFrame->GetWidgetName());
  listFrame->Delete();

  //Tool List Widget
  this->ToolList = vtkKWMultiColumnListWithScrollbars::New();
  this->ToolList->SetParent(listFrame->GetFrame());
  this->ToolList->Create();
  this->ToolList->SetHeight(1);
  this->ToolList->GetWidget()->SetSelectionTypeToRow();
  this->ToolList->GetWidget()->SetSelectionModeToSingle();
  this->ToolList->GetWidget()->MovableRowsOff();
  this->ToolList->GetWidget()->MovableColumnsOff();
  const char* labels[] =
    { "Name", "Tracking Node", "Calibrated?", "Description"};
  const int widths[] =
    { 10, 13, 10, 15 };
  for (int col = 0; col < 4; col ++)
    {
    this->ToolList->GetWidget()->AddColumn(labels[col]);
    this->ToolList->GetWidget()->SetColumnWidth(col, widths[col]);
    this->ToolList->GetWidget()->SetColumnAlignmentToLeft(col);
    this->ToolList->GetWidget()->ColumnEditableOff(col);
    this->ToolList->GetWidget()->SetColumnEditWindowToSpinBox(col);
    }
  this->ToolList->GetWidget()->SetColumnEditWindowToCheckButton(0);

  //ToolList Buttons Frame
  vtkKWFrame *listButtonsFrame = vtkKWFrame::New();
  listButtonsFrame->SetParent(listFrame->GetFrame());
  listButtonsFrame->Create();
  app->Script ("pack %s %s -fill both -expand true",
               this->ToolList->GetWidgetName(), listButtonsFrame->GetWidgetName());

  listButtonsFrame->Delete();

  //ToolList Buttons
  this->AddToolButton = vtkKWPushButton::New();
  this->AddToolButton->SetParent(listButtonsFrame);
  this->AddToolButton->Create();
  this->AddToolButton->SetText( "Add" );
  this->AddToolButton->SetWidth (6);

  this->DeleteToolButton = vtkKWPushButton::New();
  this->DeleteToolButton->SetParent(listButtonsFrame);
  this->DeleteToolButton->Create();
  this->DeleteToolButton->SetText( "Delete" );
  this->DeleteToolButton->SetWidth (6);

  app->Script( "pack %s %s -side left -anchor nw -expand n -padx 2 -pady 2",
               this->AddToolButton->GetWidgetName(), this->DeleteToolButton->GetWidgetName());

  // ---------------------------------------------------------------------------------
  // Tool Property frame

  vtkKWFrameWithLabel *controlFrame = vtkKWFrameWithLabel::New();
  controlFrame->SetParent(ToolFrame->GetFrame());
  controlFrame->Create();
  controlFrame->SetLabelText ("Tool Properties");
  app->Script ( "pack %s -fill both -expand true",
                controlFrame->GetWidgetName());

  controlFrame->Delete();

  // Tool Property -- Tool name
  vtkKWFrame *nameFrame = vtkKWFrame::New();
  nameFrame->SetParent(controlFrame->GetFrame());
  nameFrame->Create();
  app->Script ( "pack %s -fill both -expand true",
                nameFrame->GetWidgetName());

  vtkKWLabel *nameLabel = vtkKWLabel::New();
  nameLabel->SetParent(nameFrame);
  nameLabel->Create();
  nameLabel->SetWidth(12);
  nameLabel->SetText("Name: ");

  this->ToolNameEntry = vtkKWEntry::New();
  this->ToolNameEntry->SetParent(nameFrame);
  this->ToolNameEntry->Create();
  this->ToolNameEntry->SetWidth(25);
  app->Script("pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
              nameLabel->GetWidgetName() , this->ToolNameEntry->GetWidgetName());
  nameLabel->Delete();
  nameFrame->Delete();

  //Tool Property -- Tool Tracking Node
  vtkKWFrame *nodeFrame = vtkKWFrame::New();
  nodeFrame->SetParent(controlFrame->GetFrame());
  nodeFrame->Create();
  app->Script ( "pack %s -fill both -expand true",
                nodeFrame->GetWidgetName());

  vtkKWLabel *nodeLabel = vtkKWLabel::New();
  nodeLabel->SetParent(nodeFrame);
  nodeLabel->Create();
  nodeLabel->SetWidth(12);
  nodeLabel->SetText("Node: ");

  this->ToolNodeSelectorMenu = vtkSlicerNodeSelectorWidget::New();
  this->ToolNodeSelectorMenu->SetParent(nodeFrame);
  this->ToolNodeSelectorMenu->Create();
  this->ToolNodeSelectorMenu->SetWidth(30);
  this->ToolNodeSelectorMenu->SetNewNodeEnabled(0);
  this->ToolNodeSelectorMenu->SetNodeClass("vtkMRMLLinearTransformNode", NULL, NULL, NULL);
  this->ToolNodeSelectorMenu->NoneEnabledOn();
  this->ToolNodeSelectorMenu->SetShowHidden(1);
  this->ToolNodeSelectorMenu->Create();
  this->ToolNodeSelectorMenu->SetMRMLScene(this->Logic->GetMRMLScene());
  this->ToolNodeSelectorMenu->UpdateMenu();
  this->ToolNodeSelectorMenu->SetBorderWidth(0);
  this->ToolNodeSelectorMenu->SetBalloonHelpString("Select a transform from the Scene");
  app->Script("pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
              nodeLabel->GetWidgetName() , this->ToolNodeSelectorMenu->GetWidgetName());
  nodeLabel->Delete();
  nodeFrame->Delete();

  //Tool Property -- Tool Description
  vtkKWFrame* descriptionFrame = vtkKWFrame::New();
  descriptionFrame->SetParent(controlFrame->GetFrame());
  descriptionFrame->Create();
  this->Script ( "pack %s -fill both -expand true",
                 descriptionFrame->GetWidgetName());

  vtkKWLabel* descriptionLabel = vtkKWLabel::New();
  descriptionLabel->SetParent(descriptionFrame);
  descriptionLabel->Create();
  descriptionLabel->SetWidth(12);
  descriptionLabel->SetText("Description: ");

  this->ToolDescriptionEntry = vtkKWEntry::New();
  this->ToolDescriptionEntry->SetParent(descriptionFrame);
  this->ToolDescriptionEntry->Create();
  this->ToolDescriptionEntry->SetWidth(25);

  this->Script("pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
                 descriptionLabel->GetWidgetName(),
                 this->ToolDescriptionEntry->GetWidgetName());
  
  descriptionFrame->Delete();
  descriptionLabel->Delete();

  //Tool Model -- Visualization Check button
  vtkKWFrame* ModelFrame = vtkKWFrame::New();
  ModelFrame->SetParent(controlFrame->GetFrame());
  ModelFrame->Create();
  this->Script ( "pack %s -fill both -expand true",
                 ModelFrame->GetWidgetName());

  vtkKWLabel* ModelLabel = vtkKWLabel::New();
  ModelLabel->SetParent(ModelFrame);
  ModelLabel->Create();
  ModelLabel->SetWidth(12);
  ModelLabel->SetText("Show Tool: ");
  
  this->ToolCheckButton = vtkKWCheckButton::New();
  this->ToolCheckButton->SetParent(ModelFrame);
  this->ToolCheckButton->Create();
  this->ToolCheckButton->SelectedStateOff();
  //this->ToolCheckButton->SetText("Show Locator");
  
  app->Script("pack %s %s -side left -anchor w -padx 2 -pady 2", 
               ModelLabel->GetWidgetName(),
               this->ToolCheckButton->GetWidgetName());

  ModelFrame->Delete();
  ModelLabel->Delete();
  
  ToolFrame->Delete();
 
  //Un-enable all elements in Tool Property Frame
  UpdateToolPropertyFrame(-1);
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::BuildGUIForCalibrationFrame()
{

  vtkSlicerApplication *app = (vtkSlicerApplication *)this->GetApplication();
  vtkKWWidget *page = this->UIPanel->GetPageWidget ("HybridNav");

  vtkSlicerModuleCollapsibleFrame *calibrationFrame = vtkSlicerModuleCollapsibleFrame::New();

  calibrationFrame->SetParent(page);
  calibrationFrame->Create();
  calibrationFrame->SetLabelText("Pivot Calibration");
  app->Script ("pack %s -side top -anchor nw -fill x -padx 2 -pady 2 -in %s",
               calibrationFrame->GetWidgetName(), page->GetWidgetName());

  // -----------------------------------------
  // Calibration Options Frame

  vtkKWFrameWithLabel *frame = vtkKWFrameWithLabel::New();
  frame->SetParent(calibrationFrame->GetFrame());
  frame->Create();
  frame->SetLabelText ("Calibration Options");
  this->Script ( "pack %s -side top -fill x -expand y -anchor w -padx 2 -pady 2",
                 frame->GetWidgetName() );

  //Node Selector Menu
  vtkKWFrame *nodeFrame = vtkKWFrame::New();
  nodeFrame->SetParent(frame->GetFrame());
  nodeFrame->Create();
  //nodeFrame->SetWidth(20);
  app->Script ( "pack %s -fill both -expand true",
                nodeFrame->GetWidgetName());

  vtkKWLabel* nodeLabel = vtkKWLabel::New();
  nodeLabel->SetParent(nodeFrame);
  nodeLabel->Create();
  nodeLabel->SetText("Select Tool: ");
  nodeLabel->SetWidth(15);

  this->CalibrationNodeSelectorMenu = vtkSlicerNodeSelectorWidget::New();
  this->CalibrationNodeSelectorMenu->SetParent(nodeFrame);
  this->CalibrationNodeSelectorMenu->Create();
  this->CalibrationNodeSelectorMenu->SetWidth(15);
  this->CalibrationNodeSelectorMenu->SetNewNodeEnabled(0);
  this->CalibrationNodeSelectorMenu->SetNodeClass("vtkMRMLHybridNavToolNode", NULL, NULL, NULL);
  this->CalibrationNodeSelectorMenu->NoneEnabledOn();
  this->CalibrationNodeSelectorMenu->SetShowHidden(1);
  this->CalibrationNodeSelectorMenu->Create();
  this->CalibrationNodeSelectorMenu->SetMRMLScene(this->Logic->GetMRMLScene());
  this->CalibrationNodeSelectorMenu->UpdateMenu();
  this->CalibrationNodeSelectorMenu->SetBorderWidth(0);
  this->CalibrationNodeSelectorMenu->SetBalloonHelpString("Select a transform from the Scene");

  this->Script("pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
               nodeLabel->GetWidgetName(), this->CalibrationNodeSelectorMenu->GetWidgetName());

  //Number of points for calibration entry
  vtkKWFrame* pointsFrame = vtkKWFrame::New();
  pointsFrame->SetParent(frame->GetFrame());
  pointsFrame->Create();
  this->Script ( "pack %s -fill both -expand true",
                 pointsFrame->GetWidgetName());

  vtkKWLabel* pointsLabel = vtkKWLabel::New();
  pointsLabel->SetParent(pointsFrame);
  pointsLabel->Create();
  pointsLabel->SetWidth(15);
  pointsLabel->SetText("Number of Points: ");

  this->numPointsEntry = vtkKWEntry::New();
  this->numPointsEntry->SetParent(pointsFrame);
  this->numPointsEntry->SetRestrictValueToInteger();
  this->numPointsEntry->Create();
  this->numPointsEntry->SetWidth(6);
  this->numPointsEntry->SetValueAsInt(20);

  this->Script ( "pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
                 pointsLabel->GetWidgetName(),
                 this->numPointsEntry->GetWidgetName());

  //Calibration Result TextBox
  vtkKWFrame* resultFrame = vtkKWFrame::New();
  resultFrame->SetParent(frame->GetFrame());
  resultFrame->Create();
  this->Script ( "pack %s -fill both -expand true",
                 resultFrame->GetWidgetName());

  vtkKWLabel* resultLabel = vtkKWLabel::New();
  resultLabel->SetParent(resultFrame);
  resultLabel->Create();
  resultLabel->SetWidth(15);
  resultLabel->SetText("Result (mm): ");

  this->CalibrationResult = vtkKWEntry::New();
  this->CalibrationResult->SetParent(resultFrame);
  this->CalibrationResult->SetRestrictValueToDouble();
  this->CalibrationResult->Create();
  this->CalibrationResult->SetWidth(6);
  this->CalibrationResult->SetValueAsDouble(0.0);
  this->CalibrationResult->ReadOnlyOn();

  this->Script ( "pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
                 resultLabel->GetWidgetName(),
                 this->CalibrationResult->GetWidgetName());
 
  //Calibration Error TextBox
  vtkKWFrame* errorFrame = vtkKWFrame::New();
  errorFrame->SetParent(frame->GetFrame());
  errorFrame->Create();
  this->Script ( "pack %s -fill both -expand true",
                 errorFrame->GetWidgetName());

  vtkKWLabel* errorLabel = vtkKWLabel::New();
  errorLabel->SetParent(errorFrame);
  errorLabel->Create();
  errorLabel->SetWidth(15);
  errorLabel->SetText("Error (mm): ");

  this->CalibrationError = vtkKWEntry::New();
  this->CalibrationError ->SetParent(errorFrame);
  this->CalibrationError ->SetRestrictValueToDouble();
  this->CalibrationError ->Create();
  this->CalibrationError ->SetWidth(6);
  this->CalibrationError ->SetValueAsDouble(0.0);
  this->CalibrationError ->ReadOnlyOn();

  this->Script ( "pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
                 errorLabel->GetWidgetName(),
                 this->CalibrationError->GetWidgetName());
  
  //Calibration button
  vtkKWFrame* startCalibrationFrame = vtkKWFrame::New();
  startCalibrationFrame->SetParent(calibrationFrame->GetFrame());
  startCalibrationFrame->Create();
  this->Script ( "pack %s -fill both -expand true",
                 startCalibrationFrame->GetWidgetName());

  this->StartCalibrateButton = vtkKWPushButton::New ( );
  this->StartCalibrateButton->SetParent ( startCalibrationFrame );
  this->StartCalibrateButton->Create ( );
  this->StartCalibrateButton->SetText ("Start Calibration");
  this->StartCalibrateButton->SetWidth (15);
  this->Script ( "pack %s -side left -expand true",
                 StartCalibrateButton->GetWidgetName());

  //Delete widget pointers
  calibrationFrame->Delete();
  frame->Delete();
  nodeFrame->Delete();
  nodeLabel->Delete();  
  pointsFrame->Delete();
  pointsLabel->Delete();
  resultFrame->Delete();
  resultLabel->Delete();
  errorFrame->Delete();
  errorLabel->Delete();
  startCalibrationFrame->Delete();
}



//---------------------------------------------------------------------------
void vtkHybridNavGUI::BuildGUIForManualCalibrationFrame()
{

  vtkSlicerApplication *app = (vtkSlicerApplication *)this->GetApplication();
  vtkKWWidget *page = this->UIPanel->GetPageWidget ("HybridNav");

  vtkSlicerModuleCollapsibleFrame *manualCalibrationFrame = vtkSlicerModuleCollapsibleFrame::New();

  manualCalibrationFrame->SetParent(page);
  manualCalibrationFrame->Create();
  manualCalibrationFrame->SetLabelText("Manual Calibration");
  app->Script ("pack %s -side top -anchor nw -fill x -padx 2 -pady 2 -in %s",
               manualCalibrationFrame->GetWidgetName(), page->GetWidgetName());

  // -----------------------------------------
  // Manual Calibration Frame

  vtkKWFrameWithLabel *mcframe = vtkKWFrameWithLabel::New();
  mcframe->SetParent(manualCalibrationFrame->GetFrame());
  mcframe->Create();
  mcframe->SetLabelText ("Manual Calibration Settings");
  this->Script ( "pack %s -side top -fill x -expand y -anchor w -padx 2 -pady 2",
                 mcframe->GetWidgetName() );

  //Node Selector Menus
  vtkKWFrame *nodeFrame1 = vtkKWFrame::New();
  nodeFrame1->SetParent(mcframe->GetFrame());
  nodeFrame1->Create();
  //nodeFrame->SetWidth(20);
  app->Script ( "pack %s -fill both -expand true",
                nodeFrame1->GetWidgetName());

  vtkKWLabel* nodeLabel1 = vtkKWLabel::New();
  nodeLabel1->SetParent(nodeFrame1);
  nodeLabel1->Create();
  nodeLabel1->SetText("Pointer Tool: ");
  nodeLabel1->SetWidth(20);

  this->ObjectiveTransformNodeSelectorMenu = vtkSlicerNodeSelectorWidget::New();
  this->ObjectiveTransformNodeSelectorMenu->SetParent(nodeFrame1);
  this->ObjectiveTransformNodeSelectorMenu->Create();
  this->ObjectiveTransformNodeSelectorMenu->SetWidth(35);
  this->ObjectiveTransformNodeSelectorMenu->SetNewNodeEnabled(0);
  this->ObjectiveTransformNodeSelectorMenu->SetNodeClass("vtkMRMLHybridNavToolNode", NULL, NULL, NULL);
  this->ObjectiveTransformNodeSelectorMenu->NoneEnabledOn();
  this->ObjectiveTransformNodeSelectorMenu->SetShowHidden(1);
  this->ObjectiveTransformNodeSelectorMenu->Create();
  this->ObjectiveTransformNodeSelectorMenu->SetMRMLScene(this->Logic->GetMRMLScene());
  this->ObjectiveTransformNodeSelectorMenu->UpdateMenu();
  this->ObjectiveTransformNodeSelectorMenu->SetBorderWidth(0);
  this->ObjectiveTransformNodeSelectorMenu->SetBalloonHelpString("Select a HybridTool from the Scene");

  this->Script("pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
               nodeLabel1->GetWidgetName(), this->ObjectiveTransformNodeSelectorMenu->GetWidgetName());

  vtkKWFrame *nodeFrame2 = vtkKWFrame::New();
  nodeFrame2->SetParent(mcframe->GetFrame());
  nodeFrame2->Create();
  //nodeFrame->SetWidth(20);
  app->Script ( "pack %s -fill both -expand true",
                nodeFrame2->GetWidgetName());

  vtkKWLabel* nodeLabel2 = vtkKWLabel::New();
  nodeLabel2->SetParent(nodeFrame2);
  nodeLabel2->Create();
  nodeLabel2->SetText("BetaProbe Tool: ");
  nodeLabel2->SetWidth(20);

  this->CurrentTransformNodeSelectorMenu = vtkSlicerNodeSelectorWidget::New();
  this->CurrentTransformNodeSelectorMenu->SetParent(nodeFrame2);
  this->CurrentTransformNodeSelectorMenu->Create();
  this->CurrentTransformNodeSelectorMenu->SetWidth(35);
  this->CurrentTransformNodeSelectorMenu->SetNewNodeEnabled(0);
  this->CurrentTransformNodeSelectorMenu->SetNodeClass("vtkMRMLHybridNavToolNode", NULL, NULL, NULL);
  this->CurrentTransformNodeSelectorMenu->NoneEnabledOn();
  this->CurrentTransformNodeSelectorMenu->SetShowHidden(1);
  this->CurrentTransformNodeSelectorMenu->Create();
  this->CurrentTransformNodeSelectorMenu->SetMRMLScene(this->Logic->GetMRMLScene());
  this->CurrentTransformNodeSelectorMenu->UpdateMenu();
  this->CurrentTransformNodeSelectorMenu->SetBorderWidth(0);
  this->CurrentTransformNodeSelectorMenu->SetBalloonHelpString("Select a HybridTool from the Scene");

  this->Script("pack %s %s -side left -anchor w -fill x -padx 2 -pady 2",
               nodeLabel2->GetWidgetName(), this->CurrentTransformNodeSelectorMenu->GetWidgetName());

  //Manual Calibration button
  vtkKWFrame* CalibrationButtonFrame = vtkKWFrame::New();
  CalibrationButtonFrame->SetParent(manualCalibrationFrame->GetFrame());
  CalibrationButtonFrame->Create();
  this->Script ( "pack %s -fill both -expand true",
                 CalibrationButtonFrame->GetWidgetName());

  this->ManualCalibrateButton = vtkKWPushButton::New ( );
  this->ManualCalibrateButton->SetParent (CalibrationButtonFrame );
  this->ManualCalibrateButton->Create ( );
  this->ManualCalibrateButton->SetText ("Manual Calibration");
  this->ManualCalibrateButton->SetWidth (15);
  this->Script ( "pack %s -side left -expand true",
                 this->ManualCalibrateButton->GetWidgetName());

  //Delete widget pointers
  manualCalibrationFrame->Delete();
  mcframe->Delete();
  nodeFrame1->Delete();
  nodeLabel1->Delete();  
  nodeFrame2->Delete();
  nodeLabel2->Delete();  
  CalibrationButtonFrame->Delete(); 
}

//----------------------------------------------------------------------------
void vtkHybridNavGUI::UpdateAll()
{
}

//----------------------------------------------------------------------------
void vtkHybridNavGUI::UpdateToolNodeList()
{
// update the list of tools in the scene, by saving their ID in a vector
  std::cerr << "Tool node list updated" << std::endl;
  std::vector<vtkMRMLNode*> nodes;
  const char* className = this->GetMRMLScene()->GetClassNameByTag("HybridNavTool");
  this->GetMRMLScene()->GetNodesByClass(className, nodes);

  this->ToolNodeList.clear();
  std::vector<vtkMRMLNode*>::iterator iter;
  for (iter = nodes.begin(); iter != nodes.end(); iter ++)
    {
    vtkMRMLHybridNavToolNode* ptr = vtkMRMLHybridNavToolNode::SafeDownCast(*iter);
    if (ptr)
      {
      this->ToolNodeList.push_back(ptr->GetID());
      }
    }
  return;
}

//---------------------------------------------------------------------------
void vtkHybridNavGUI::UpdateToolList(int updateLevel)
{
  std::cerr << "Tool list updated" << std::endl;
  // Update Tool List
  if (this->ToolList == NULL)
    {
    return;
    }

  //----------------------------------------------------------------
  // Change number of rows (UPDATE_ALL only)

  if (updateLevel >= UPDATE_ALL)
    {

    // Adjust number of rows in ToolList
    int numRows = this->ToolList->GetWidget()->GetNumberOfRows();
    int numTools = this->ToolNodeList.size();

    if (numRows < numTools)
      {
      this->ToolList->GetWidget()->AddRows(numTools-numRows);
      }
    else if (numRows > numTools)
      {
      int ndel = numRows-numTools;
      for (int i = 0; i < ndel; i ++)
        {
        this->ToolList->GetWidget()->DeleteRow(numTools);
        }
      }
    }
  int numItems = this->ToolNodeList.size();

  //----------------------------------------------------------------
  // Update rows (UPDATE_ALL, UPDATE_PROPERTIES_ALL and UPDATE_SELECTED_ONLY)

  // Generate list of rows to update
  std::vector<int> updateRows;
  updateRows.clear();
  if (updateLevel >= UPDATE_ALL)
    {
    for (int i = 0; i < numItems; i ++)
      {
      updateRows.push_back(i);
      }
    }
  else if (updateLevel >= UPDATE_SELECTED_ONLY)
    {
    updateRows.push_back(this->ToolList->GetWidget()->GetIndexOfFirstSelectedRow());
    }

  //Update the Tool List row by row
  std::vector<int>::iterator iter;
  for (iter = updateRows.begin(); iter != updateRows.end(); iter ++)
    {
    int i = *iter;
    if (i >= 0 && i < (int)this->ToolNodeList.size())
      {
      vtkMRMLHybridNavToolNode* tool
        = vtkMRMLHybridNavToolNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->ToolNodeList[i]));
      if (tool)
        {
        // Tool Name
        this->ToolList->GetWidget()
          ->SetCellText(i,0, tool->GetName());

        // Tool Node
        if(tool->GetParentTransformNode())
          {
          this->ToolList->GetWidget()
            ->SetCellText(i,1, tool->GetParentTransformNode()->GetID());
          }

        //Tool Calibrated
        const char* c;
        if(tool->GetCalibrated())
            c="Y";
        else
            c="N";
        this->ToolList->GetWidget()
          ->SetCellText(i,2, c);

        //Tool Description
        this->ToolList->GetWidget()
          ->SetCellText(i,3, tool->GetToolDescription());
        }
      }
    }
}


//---------------------------------------------------------------------------
void vtkHybridNavGUI::UpdateToolPropertyFrame(int i)
{
  // if i < 0, all fields are deactivated.

  int numRows = this->ToolList->GetWidget()->GetNumberOfRows();
  if (i >= 0 && i >= numRows)
    {
    return;
    }

  //----------------------------------------------------------------
  // No connector is registered
  if (i < 0 || numRows <= 0)
    {
    // Deactivate everything
    // Tool Name entry
    this->ToolNameEntry->SetValue("");
    this->ToolNameEntry->EnabledOff();
    this->ToolNameEntry->UpdateEnableState();

    //TODO: Would be good to unenable node when nothing is selected
    // Tool Node
    //this->ToolNodeSelectorMenu->SetNoneEnabled(1);
    //this->ToolNodeSelectorMenu->UpdateEnableState();

    // Tool Description Entry
    this->ToolDescriptionEntry->SetValue("");
    this->ToolDescriptionEntry->EnabledOff();
    this->ToolDescriptionEntry->UpdateEnableState();
    
    //Tool Visibility
    this->ToolCheckButton->EnabledOff();
    this->ToolCheckButton->UpdateEnableState();

    return;
    }

  //----------------------------------------------------------------
  // A connector is selected on the list

  if (i < 0 || i >= (int)this->ToolNodeList.size())
    {
    return;
    }

  vtkMRMLHybridNavToolNode* tool
    = vtkMRMLHybridNavToolNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->ToolNodeList[i]));

  if (tool == NULL)  // if the connector has already beeen removed
    {
    UpdateToolNodeList();
    UpdateToolPropertyFrame(i);
    return;
    }

  // Tool Name entry
  this->ToolNameEntry->EnabledOn();
  this->ToolNameEntry->UpdateEnableState();
  this->ToolNameEntry->SetValue(tool->GetName());

  // Tool node
  //TODO: Make sure that when there is no node selected, that the node displays nothing
  if (tool->GetParentTransformNode())
    {
    this->ToolNodeSelectorMenu->SetSelected((vtkMRMLNode*)tool->GetParentTransformNode());
    }

  // Tool Description entry
  this->ToolDescriptionEntry->EnabledOn();
  this->ToolDescriptionEntry->UpdateEnableState();
  this->ToolDescriptionEntry->SetValue(tool->GetToolDescription());
  
  //Tool Visibility
  this->ToolCheckButton->EnabledOn();
  this->ToolCheckButton->UpdateEnableState();
  vtkMRMLDisplayNode* toolDisp = tool->GetDisplayNode();
  std::cerr << "Tool visibility: " << toolDisp->GetVisibility() << std::endl;
  this->ToolCheckButton->SetSelectedState(toolDisp->GetVisibility());

}
