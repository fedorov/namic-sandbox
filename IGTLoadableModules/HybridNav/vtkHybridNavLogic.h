/*==========================================================================

  Portions (c) Copyright 2008 Brigham and Women's Hospital (BWH) All Rights Reserved.

  See Doc/copyright/copyright.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Program:   3D Slicer
  Module:    $HeadURL: $
  Date:      $Date: $
  Version:   $Revision: $

==========================================================================*/

// .NAME vtkHybridNavLogic - slicer logic class for Locator module 
// .SECTION Description
// This class manages the logic associated with tracking device for
// IGT. 


#ifndef __vtkHybridNavLogic_h
#define __vtkHybridNavLogic_h

#include "vtkHybridNavWin32Header.h"

#include "vtkSlicerBaseLogic.h"
#include "vtkSlicerModuleLogic.h"
#include "vtkSlicerApplication.h"
#include "vtkCallbackCommand.h"

#include "vtkMRMLSliceNode.h"
#include "vtkMRMLHybridNavToolNode.h"

class vtkMRMLModelNode;

class VTK_HybridNav_EXPORT vtkHybridNavLogic : public vtkSlicerModuleLogic 
{
 public:
  //BTX
  enum {  // Events
    //LocatorUpdateEvent      = 50000,
    StatusUpdateEvent       = 50001,
  };
  //ETX

 public:
  
  static vtkHybridNavLogic *New();
  
  vtkTypeRevisionMacro(vtkHybridNavLogic,vtkObject);
  void PrintSelf(ostream&, vtkIndent);
  
  //BTX
  //functions to enable a model to represent the tool
  vtkMRMLHybridNavToolNode* CreateToolModel(vtkMRMLHybridNavToolNode* tnode);
  void AppendToolTipModel(vtkMRMLHybridNavToolNode* mnode);
  //vtkMRMLModelNode* AddLocatorModel(const char* nodeName, double r, double g, double b);
  void SetVisibilityOfToolModel(vtkMRMLHybridNavToolNode* tnode, int v);
  void ManualCalibration(vtkMRMLHybridNavToolNode* PointerTool, vtkMRMLHybridNavToolNode* BetaProbeTool);
  //ETX
  
 protected:
  
  vtkHybridNavLogic();
  ~vtkHybridNavLogic();

  void operator=(const vtkHybridNavLogic&);
  vtkHybridNavLogic(const vtkHybridNavLogic&);
  

  static void DataCallback(vtkObject*, unsigned long, void *, void *);
  void UpdateAll();

  vtkCallbackCommand *DataCallbackCommand;

 private:


};

#endif


  
