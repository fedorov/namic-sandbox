/* 
 * Resource generated for file:
 *    MotionTracker_Play.png (zlib, base64) (image file)
 */
static const unsigned int  image_MotionTracker_Play_width          = 18;
static const unsigned int  image_MotionTracker_Play_height         = 18;
static const unsigned int  image_MotionTracker_Play_pixel_size     = 4;
static const unsigned long image_MotionTracker_Play_length         = 288;
static const unsigned long image_MotionTracker_Play_decoded_length = 1296;

static const unsigned char image_MotionTracker_Play[] = 
  "eNrFlL0Kg0AQhH0HQRFbFV9PC/86LcSnsxBFwVIttLM7QZ3kApsiXW4DWZju+NidvVkAOw"
  "DB1H5d19E0Deq6BqOOJ0fEcQxd11EUBSRTocR93yLPc2ia9pLrukjTFMMwsDgkx3EQBAHm"
  "eWZxSLZtI4oirOvK4pAMw4B8N00Ti0MyTRNVVX369zWHZFkWkiQh/5Q5si8Oh+bq+/4vPs"
  "u9y/63bVPau+d5CMMQy7Io/UPf95FlGcZx/DoXMqdy/rIs0XWdUk7P8zzatlXN+ftu4Ed3"
  "7AF5W7bh";


/* 
 * Resource generated for file:
 *    MotionTracker_Pause.png (zlib, base64) (image file)
 */
static const unsigned int  image_MotionTracker_Pause_width          = 18;
static const unsigned int  image_MotionTracker_Pause_height         = 18;
static const unsigned int  image_MotionTracker_Pause_pixel_size     = 3;
static const unsigned long image_MotionTracker_Pause_length         = 72;
static const unsigned long image_MotionTracker_Pause_decoded_length = 972;

static const unsigned char image_MotionTracker_Pause[] = 
  "eNr7/59M8Pbt2z2oAChCUGrbtm0MqAAoQlAKaAKaFFBkVGpUis5S5KVe8nIKGQAAeBvn/A"
  "==";

