/*==========================================================================

  Portions (c) Copyright 2010-2011 Brigham and Women's Hospital (BWH) All Rights Reserved.

  See Doc/copyright/copyright.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Program:   3D Slicer
  Module:    $HeadURL: $
  Date:      $Date: $
  Version:   $Revision: $

==========================================================================*/

/* AbdoNav includes */
#include "vtkAbdoNavGUI.h"

/* KWWidgets includes */
#include "vtkKWFrameWithLabel.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWScale.h"
#include "vtkKWScaleWithEntry.h"

/* MRML includes */
#include "vtkMRMLFiducialListNode.h"
#include "vtkMRMLLinearTransformNode.h"

/* Slicer includes */
#include "vtkSlicerApplication.h"
#include "vtkSlicerNodeSelectorWidget.h"
#include "vtkSlicerSlicesControlGUI.h"
#include "vtkSlicerTheme.h"

/* STL includes */
#include <limits>

//---------------------------------------------------------------------------
vtkCxxRevisionMacro(vtkAbdoNavGUI, "$Revision: $");
vtkStandardNewMacro(vtkAbdoNavGUI);


//---------------------------------------------------------------------------
vtkAbdoNavGUI::vtkAbdoNavGUI()
{
  //----------------------------------------------------------------
  // Initialize logic values.
  //----------------------------------------------------------------
  this->AbdoNavLogic = NULL;
  this->AbdoNavNode = NULL;
  this->TimerLog = NULL;

  //----------------------------------------------------------------
  // Registration frame.
  //----------------------------------------------------------------
  this->TrackerTransformSelector = NULL;
  this->GuidanceToolTypeLabel = NULL;
  this->GuidanceToolTypeMenuButton = NULL;
  this->ToolBoxFileLoadLabel = NULL;
  this->ToolBoxFileLoadButton = NULL;
  this->Point1CheckButton = NULL;
  this->Point1REntry = NULL;
  this->Point1AEntry = NULL;
  this->Point1SEntry = NULL;
  this->Point2CheckButton = NULL;
  this->Point2REntry = NULL;
  this->Point2AEntry = NULL;
  this->Point2SEntry = NULL;
  this->Point3CheckButton = NULL;
  this->Point3REntry = NULL;
  this->Point3AEntry = NULL;
  this->Point3SEntry = NULL;
  this->Point4CheckButton = NULL;
  this->Point4REntry = NULL;
  this->Point4AEntry = NULL;
  this->Point4SEntry = NULL;
  this->Point5CheckButton = NULL;
  this->Point5REntry = NULL;
  this->Point5AEntry = NULL;
  this->Point5SEntry = NULL;
  this->ResetRegistrationPushButton = NULL;
  this->PerformRegistrationPushButton = NULL;

  //----------------------------------------------------------------
  // Navigation frame.
  //----------------------------------------------------------------
  this->RecordLocatorPositionPushButton = NULL;
  this->ShowLocatorCheckButton = NULL;
  this->ProjectionLengthScale = NULL;
  this->FreezeLocatorCheckButton = NULL;
  this->ShowCrosshairCheckButton = NULL;
  this->DrawNeedleProjectionCheckButton = NULL;
  this->RedSliceMenuButton = NULL;
  this->YellowSliceMenuButton = NULL;
  this->GreenSliceMenuButton = NULL;
  this->SetLocatorAllPushButton = NULL;
  this->SetUserAllPushButton = NULL;
  this->FreezeReslicingCheckButton = NULL;
  this->ObliqueReslicingCheckButton = NULL;
}


//---------------------------------------------------------------------------
vtkAbdoNavGUI::~vtkAbdoNavGUI()
{
  //----------------------------------------------------------------
  // Clean up logic values.
  //----------------------------------------------------------------
  // if Logic is NULL, the class was only instantiated but never used,
  // e.g. Slicer was launched with option --ignore_module set to AbdoNav
  if (this->AbdoNavLogic)
    {
    this->RemoveGUIObservers();
    }
  this->SetModuleLogic(NULL);

  // remove AbdoNavNode observers
  vtkSetMRMLNodeMacro(this->AbdoNavNode, NULL);

  if (this->TimerLog)
    {
    this->TimerLog->Delete();
    this->TimerLog = NULL;
    }

  // TODO: all of the following could probably be avoided by using VTK's smart pointers

  //----------------------------------------------------------------
  // Registration frame.
  //----------------------------------------------------------------
  if (this->TrackerTransformSelector)
    {
    this->TrackerTransformSelector->SetParent(NULL);
    this->TrackerTransformSelector->Delete();
    this->TrackerTransformSelector = NULL;
    }
  if (this->GuidanceToolTypeLabel)
    {
    this->GuidanceToolTypeLabel->SetParent(NULL);
    this->GuidanceToolTypeLabel->Delete();
    this->GuidanceToolTypeLabel = NULL;
    }
  if (this->GuidanceToolTypeMenuButton)
    {
    this->GuidanceToolTypeMenuButton->SetParent(NULL);
    this->GuidanceToolTypeMenuButton->Delete();
    this->GuidanceToolTypeMenuButton = NULL;
    }
  if (this->ToolBoxFileLoadLabel)
    {
    this->ToolBoxFileLoadLabel->SetParent(NULL);
    this->ToolBoxFileLoadLabel->Delete();
    this->ToolBoxFileLoadLabel = NULL;
    }
  if (this->ToolBoxFileLoadButton)
    {
    this->ToolBoxFileLoadButton->SetParent(NULL);
    this->ToolBoxFileLoadButton->Delete();
    this->ToolBoxFileLoadButton = NULL;
    }
  if (this->Point1CheckButton)
    {
    this->Point1CheckButton->SetParent(NULL);
    this->Point1CheckButton->Delete();
    this->Point1CheckButton = NULL;
    }
  if (this->Point1REntry)
    {
    this->Point1REntry->SetParent(NULL);
    this->Point1REntry->Delete();
    this->Point1REntry = NULL;
    }
  if (this->Point1AEntry)
    {
    this->Point1AEntry->SetParent(NULL);
    this->Point1AEntry->Delete();
    this->Point1AEntry = NULL;
    }
  if (this->Point1SEntry)
    {
    this->Point1SEntry->SetParent(NULL);
    this->Point1SEntry->Delete();
    this->Point1SEntry = NULL;
    }
  if (this->Point2CheckButton)
    {
    this->Point2CheckButton->SetParent(NULL);
    this->Point2CheckButton->Delete();
    this->Point2CheckButton = NULL;
    }
  if (this->Point2REntry)
    {
    this->Point2REntry->SetParent(NULL);
    this->Point2REntry->Delete();
    this->Point2REntry = NULL;
    }
  if (this->Point2AEntry)
    {
    this->Point2AEntry->SetParent(NULL);
    this->Point2AEntry->Delete();
    this->Point2AEntry = NULL;
    }
  if (this->Point2SEntry)
    {
    this->Point2SEntry->SetParent(NULL);
    this->Point2SEntry->Delete();
    this->Point2SEntry = NULL;
    }
  if (this->Point3CheckButton)
    {
    this->Point3CheckButton->SetParent(NULL);
    this->Point3CheckButton->Delete();
    this->Point3CheckButton = NULL;
    }
  if (this->Point3REntry)
    {
    this->Point3REntry->SetParent(NULL);
    this->Point3REntry->Delete();
    this->Point3REntry = NULL;
    }
  if (this->Point3AEntry)
    {
    this->Point3AEntry->SetParent(NULL);
    this->Point3AEntry->Delete();
    this->Point3AEntry = NULL;
    }
  if (this->Point3SEntry)
    {
    this->Point3SEntry->SetParent(NULL);
    this->Point3SEntry->Delete();
    this->Point3SEntry = NULL;
    }
  if (this->Point4CheckButton)
    {
    this->Point4CheckButton->SetParent(NULL);
    this->Point4CheckButton->Delete();
    this->Point4CheckButton = NULL;
    }
  if (this->Point4REntry)
    {
    this->Point4REntry->SetParent(NULL);
    this->Point4REntry->Delete();
    this->Point4REntry = NULL;
    }
  if (this->Point4AEntry)
    {
    this->Point4AEntry->SetParent(NULL);
    this->Point4AEntry->Delete();
    this->Point4AEntry = NULL;
    }
  if (this->Point4SEntry)
    {
    this->Point4SEntry->SetParent(NULL);
    this->Point4SEntry->Delete();
    this->Point4SEntry = NULL;
    }
  if (this->Point5CheckButton)
    {
    this->Point5CheckButton->SetParent(NULL);
    this->Point5CheckButton->Delete();
    this->Point5CheckButton = NULL;
    }
  if (this->Point5REntry)
    {
    this->Point5REntry->SetParent(NULL);
    this->Point5REntry->Delete();
    this->Point5REntry = NULL;
    }
  if (this->Point5AEntry)
    {
    this->Point5AEntry->SetParent(NULL);
    this->Point5AEntry->Delete();
    this->Point5AEntry = NULL;
    }
  if (this->Point5SEntry)
    {
    this->Point5SEntry->SetParent(NULL);
    this->Point5SEntry->Delete();
    this->Point5SEntry = NULL;
    }
  if (this->ResetRegistrationPushButton)
    {
    this->ResetRegistrationPushButton->SetParent(NULL);
    this->ResetRegistrationPushButton->Delete();
    this->ResetRegistrationPushButton = NULL;
    }
  if (this->PerformRegistrationPushButton)
    {
    this->PerformRegistrationPushButton->SetParent(NULL);
    this->PerformRegistrationPushButton->Delete();
    this->PerformRegistrationPushButton = NULL;
    }

  //----------------------------------------------------------------
  // Navigation frame.
  //----------------------------------------------------------------
  if (this->RecordLocatorPositionPushButton)
    {
    this->RecordLocatorPositionPushButton->SetParent(NULL);
    this->RecordLocatorPositionPushButton->Delete();
    this->RecordLocatorPositionPushButton = NULL;
    }
  if (this->ShowLocatorCheckButton)
    {
    this->ShowLocatorCheckButton->SetParent(NULL);
    this->ShowLocatorCheckButton->Delete();
    this->ShowLocatorCheckButton = NULL;
    }
  if (this->ProjectionLengthScale)
    {
    this->ProjectionLengthScale->SetParent(NULL);
    this->ProjectionLengthScale->Delete();
    this->ProjectionLengthScale = NULL;
    }
  if (this->FreezeLocatorCheckButton)
    {
    this->FreezeLocatorCheckButton->SetParent(NULL);
    this->FreezeLocatorCheckButton->Delete();
    this->FreezeLocatorCheckButton = NULL;
    }
  if (this->ShowCrosshairCheckButton)
    {
    this->ShowCrosshairCheckButton->SetParent(NULL);
    this->ShowCrosshairCheckButton->Delete();
    this->ShowCrosshairCheckButton = NULL;
    }
  if (this->DrawNeedleProjectionCheckButton)
    {
    this->DrawNeedleProjectionCheckButton->SetParent(NULL);
    this->DrawNeedleProjectionCheckButton->Delete();
    this->DrawNeedleProjectionCheckButton = NULL;
    }
  if (this->RedSliceMenuButton)
    {
    this->RedSliceMenuButton->SetParent(NULL);
    this->RedSliceMenuButton->Delete();
    this->RedSliceMenuButton = NULL;
    }
  if (this->YellowSliceMenuButton)
    {
    this->YellowSliceMenuButton->SetParent(NULL);
    this->YellowSliceMenuButton->Delete();
    this->YellowSliceMenuButton = NULL;
    }
  if (this->GreenSliceMenuButton)
    {
    this->GreenSliceMenuButton->SetParent(NULL);
    this->GreenSliceMenuButton->Delete();
    this->GreenSliceMenuButton = NULL;
    }
  if (this->SetLocatorAllPushButton)
    {
    this->SetLocatorAllPushButton->SetParent(NULL);
    this->SetLocatorAllPushButton->Delete();
    this->SetLocatorAllPushButton = NULL;
    }
  if (this->SetUserAllPushButton)
    {
    this->SetUserAllPushButton->SetParent(NULL);
    this->SetUserAllPushButton->Delete();
    this->SetUserAllPushButton = NULL;
    }
  if (this->FreezeReslicingCheckButton)
    {
    this->FreezeReslicingCheckButton->SetParent(NULL);
    this->FreezeReslicingCheckButton->Delete();
    this->FreezeReslicingCheckButton = NULL;
    }
  if (this->ObliqueReslicingCheckButton)
    {
    this->ObliqueReslicingCheckButton->SetParent(NULL);
    this->ObliqueReslicingCheckButton->Delete();
    this->ObliqueReslicingCheckButton = NULL;
    }
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::PrintSelf(ostream& os, vtkIndent indent)
{
  this->vtkObject::PrintSelf(os, indent);

  os << indent << "vtkAbdoNavGUI: " << this->GetClassName() << "\n";
  os << indent << "vtkAbdoNavLogic: " << this->AbdoNavLogic << "\n";
  os << indent << "vtkAbdoNavNode: " << this->AbdoNavNode << "\n";
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::Init()
{
  // this is necessary so that an AbdoNavNode can be created from its class name,
  // e.g. when a previously saved scene (containing an AbdoNavNode) is (re-)loaded
  vtkMRMLAbdoNavNode* node = vtkMRMLAbdoNavNode::New();
  this->AbdoNavLogic->GetMRMLScene()->RegisterNodeClass(node);
  node->Delete();
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::Enter()
{
  // TODO: implement or delete!
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::Exit()
{
  // TODO: implement or delete!
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::AddGUIObservers()
{
  this->RemoveGUIObservers();

  //----------------------------------------------------------------
  // Set observers on slice views.
  //----------------------------------------------------------------
  vtkSlicerApplicationGUI* appGUI = this->GetApplicationGUI();

  appGUI->GetMainSliceGUI("Red")
    ->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor()->GetInteractorStyle()
    ->AddObserver(vtkCommand::LeftButtonPressEvent, (vtkCommand*)this->GUICallbackCommand);
  appGUI->GetMainSliceGUI("Yellow")
    ->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor()->GetInteractorStyle()
    ->AddObserver(vtkCommand::LeftButtonPressEvent, (vtkCommand*)this->GUICallbackCommand);
  appGUI->GetMainSliceGUI("Green")
    ->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor()->GetInteractorStyle()
    ->AddObserver(vtkCommand::LeftButtonPressEvent, (vtkCommand*)this->GUICallbackCommand);

  //----------------------------------------------------------------
  // Registration frame.
  //----------------------------------------------------------------
  this->TrackerTransformSelector->AddObserver(vtkSlicerNodeSelectorWidget::NodeSelectedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->GuidanceToolTypeMenuButton->GetMenu()->AddObserver(vtkKWMenu::MenuItemInvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->ToolBoxFileLoadButton->GetLoadSaveDialog()->AddObserver(vtkKWTopLevel::WithdrawEvent, (vtkCommand*)this->GUICallbackCommand);
  this->Point1CheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->Point2CheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->Point3CheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->Point4CheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->Point5CheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->ResetRegistrationPushButton->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->PerformRegistrationPushButton->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand*)this->GUICallbackCommand);

  //----------------------------------------------------------------
  // Navigation frame.
  //----------------------------------------------------------------
  this->RecordLocatorPositionPushButton->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->ShowLocatorCheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->ProjectionLengthScale->GetWidget()->AddObserver(vtkKWScale::ScaleValueChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->FreezeLocatorCheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->ShowCrosshairCheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->DrawNeedleProjectionCheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->RedSliceMenuButton->GetMenu()->AddObserver(vtkKWMenu::MenuItemInvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->YellowSliceMenuButton->GetMenu()->AddObserver(vtkKWMenu::MenuItemInvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->GreenSliceMenuButton->GetMenu()->AddObserver(vtkKWMenu::MenuItemInvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->SetLocatorAllPushButton->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->SetUserAllPushButton->AddObserver(vtkKWPushButton::InvokedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->FreezeReslicingCheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);
  this->ObliqueReslicingCheckButton->AddObserver(vtkKWCheckButton::SelectedStateChangedEvent, (vtkCommand*)this->GUICallbackCommand);

  // must be called manually!
  this->AddLogicObservers();
  this->AddMRMLObservers();
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::RemoveGUIObservers()
{
  //----------------------------------------------------------------
  // Remove observers from slice views.
  //----------------------------------------------------------------
  vtkSlicerApplicationGUI* appGUI = this->GetApplicationGUI();

  if (appGUI && appGUI->GetMainSliceGUI("Red"))
    {
    appGUI->GetMainSliceGUI("Red")->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor()
      ->GetInteractorStyle()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (appGUI && appGUI->GetMainSliceGUI("Yellow"))
    {
    appGUI->GetMainSliceGUI("Yellow")->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor()
      ->GetInteractorStyle()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (appGUI && appGUI->GetMainSliceGUI("Yellow"))
    {
    appGUI->GetMainSliceGUI("Green")->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor()
      ->GetInteractorStyle()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }

  //----------------------------------------------------------------
  // Registration frame.
  //----------------------------------------------------------------
  if (this->TrackerTransformSelector)
    {
    this->TrackerTransformSelector->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->GuidanceToolTypeMenuButton)
    {
    this->GuidanceToolTypeMenuButton->GetMenu()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->ToolBoxFileLoadButton)
    {
    this->ToolBoxFileLoadButton->GetLoadSaveDialog()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->Point1CheckButton)
    {
    this->Point1CheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->Point2CheckButton)
    {
    this->Point2CheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->Point3CheckButton)
    {
    this->Point3CheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->Point4CheckButton)
    {
    this->Point4CheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->Point5CheckButton)
    {
    this->Point5CheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->ResetRegistrationPushButton)
    {
    this->ResetRegistrationPushButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->PerformRegistrationPushButton)
    {
    this->PerformRegistrationPushButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }

  //----------------------------------------------------------------
  // Navigation frame.
  //----------------------------------------------------------------
  if (this->RecordLocatorPositionPushButton)
    {
    this->RecordLocatorPositionPushButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->ShowLocatorCheckButton)
    {
    this->ShowLocatorCheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->ProjectionLengthScale)
    {
    this->ProjectionLengthScale->GetWidget()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->FreezeLocatorCheckButton)
    {
    this->FreezeLocatorCheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->ShowCrosshairCheckButton)
    {
    this->ShowCrosshairCheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->DrawNeedleProjectionCheckButton)
    {
    this->DrawNeedleProjectionCheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->RedSliceMenuButton)
    {
    this->RedSliceMenuButton->GetMenu()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->YellowSliceMenuButton)
    {
    this->YellowSliceMenuButton->GetMenu()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->GreenSliceMenuButton)
    {
    this->GreenSliceMenuButton->GetMenu()->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->SetLocatorAllPushButton)
    {
    this->SetLocatorAllPushButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->SetUserAllPushButton)
    {
    this->SetUserAllPushButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->FreezeReslicingCheckButton)
    {
    this->FreezeReslicingCheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }
  if (this->ObliqueReslicingCheckButton)
    {
    this->ObliqueReslicingCheckButton->RemoveObserver((vtkCommand*)this->GUICallbackCommand);
    }

  // must be called manually!
  this->RemoveLogicObservers();
  this->RemoveMRMLObservers();
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::AddLogicObservers()
{
  this->RemoveLogicObservers();

  if (this->AbdoNavLogic)
    {
    this->AbdoNavLogic->AddObserver(vtkAbdoNavLogic::RequestFitToBackground, (vtkCommand*)this->LogicCallbackCommand);
    this->AbdoNavLogic->AddObserver(vtkAbdoNavLogic::LocatorPositionRecorded, (vtkCommand*)this->LogicCallbackCommand);
    }
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::RemoveLogicObservers()
{
  if (this->AbdoNavLogic)
    {
    this->AbdoNavLogic->RemoveObservers(vtkAbdoNavLogic::RequestFitToBackground, (vtkCommand*)this->LogicCallbackCommand);
    this->AbdoNavLogic->RemoveObservers(vtkAbdoNavLogic::LocatorPositionRecorded, (vtkCommand*)this->LogicCallbackCommand);
    }
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::AddMRMLObservers()
{
  // add scene observers
  vtkIntArray* events = vtkIntArray::New();
  events->InsertNextValue(vtkMRMLScene::NewSceneEvent);
  events->InsertNextValue(vtkMRMLScene::NodeAddedEvent);
  events->InsertNextValue(vtkMRMLScene::NodeRemovedEvent);
  events->InsertNextValue(vtkMRMLScene::SceneCloseEvent);

  if (this->GetMRMLScene() != NULL)
    {
    this->SetAndObserveMRMLSceneEvents(this->GetMRMLScene(), events);
    }
  events->Delete();
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::RemoveMRMLObservers()
{
  // remove scene observers
  if (this->GetMRMLScene() != NULL)
    {
    this->SetAndObserveMRMLSceneEvents(this->GetMRMLScene(), NULL);
    }

  // remove registration fiducial list observers
  if (this->AbdoNavNode != NULL)
    {
    vtkMRMLFiducialListNode* regFidList = vtkMRMLFiducialListNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->AbdoNavNode->GetRegistrationFiducialListID()));
    if (regFidList != NULL)
      {
      regFidList->RemoveObservers(vtkMRMLFiducialListNode::FiducialModifiedEvent, (vtkCommand*)this->MRMLCallbackCommand);
      regFidList->RemoveObservers(vtkMRMLScene::NodeAddedEvent, (vtkCommand*)this->MRMLCallbackCommand);
      regFidList->RemoveObservers(vtkMRMLScene::NodeRemovedEvent, (vtkCommand*)this->MRMLCallbackCommand);
      regFidList->RemoveObservers(vtkCommand::ModifiedEvent, (vtkCommand*)this->MRMLCallbackCommand);
      }
    }
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::ProcessGUIEvents(vtkObject* caller, unsigned long event, void* callData)
{
  //----------------------------------------------------------------
  // Slice views.
  //
  // If the user clicked in one of the slice views with one of the
  // check buttons associated with the RAS coordinates of
  //  - the guidance needle tip
  //  - the center of marker A
  //  - the center of marker B
  //  - the center of marker C
  //  - the center of marker D
  // being active at the same time, the corresponding fiducial in
  // AbdoNav's fiducial list is being updated. If there exists no
  // corresponding fiducial in AbdoNav's fiducial list yet, a new
  // fiducial is added.
  //
  // The steps are:
  // 0. determine whether or not AbdoNav is the selected module;
  //    neither update nor create a fiducial if it isn't, thus
  //    exit this function
  // 1. if AbdoNav is the selected module, determine which check
  //    button is active (if there is an active one at all)
  // 2. if there is an active check button:
  //      2.0. create/retrieve AbdoNav's fiducial list
  //      2.1. transform XY mouse coordinates into RAS coordinates
  //      2.2. determine whether or not AbdoNav's fiducial list
  //           already contains a fiducial corresponding to the
  //           active check button
  //      2.3. if AbdoNav's fiducial list already contains a corres-
  //           ponding fiducial, then update it; otherwise, add a new
  //           fiducial
  //----------------------------------------------------------------
  vtkSlicerInteractorStyle* style = vtkSlicerInteractorStyle::SafeDownCast(caller);
  if (style != NULL && event == vtkCommand::LeftButtonPressEvent)
    {
    // exit this function if AbdoNav isn't the selected module (this->GetModuleName()
    // returns NULL, thus use this->GetGUIName())
    vtkMRMLLayoutNode* layout = vtkMRMLLayoutNode::SafeDownCast(this->GetMRMLScene()->GetNthNodeByClass(0, "vtkMRMLLayoutNode"));
    if (layout && layout->GetSelectedModule() && strcmp(layout->GetSelectedModule(), this->GetGUIName()) != 0)
      {
      // another module is currently selected, thus exit
      return;
      }

    // determine which check button is active
    std::string activeCheckButton = "";
    if (this->Point1CheckButton->GetSelectedState() && this->Point1CheckButton->GetEnabled())
      {
      activeCheckButton = tip;
      }
    else if (this->Point2CheckButton->GetSelectedState() && this->Point2CheckButton->GetEnabled())
      {
      activeCheckButton = markerA;
      }
    else if (this->Point3CheckButton->GetSelectedState() && this->Point3CheckButton->GetEnabled())
      {
      activeCheckButton = markerB;
      }
    else if (this->Point4CheckButton->GetSelectedState() && this->Point4CheckButton->GetEnabled())
      {
      activeCheckButton = markerC;
      }
    else if (this->Point5CheckButton->GetSelectedState() && this->Point5CheckButton->GetEnabled())
      {
      activeCheckButton = markerD;
      }

    // if there is an active check button
    if (strcmp(activeCheckButton.c_str(), ""))
      {
      // create an AbdoNavNode if none exists yet
      vtkMRMLAbdoNavNode* anode = this->CheckAndCreateAbdoNavNode();

      // create/retrieve AbdoNav's fiducial list
      vtkMRMLFiducialListNode* fiducialList;
      if (anode->GetRegistrationFiducialListID() == NULL)
        {
        // AbdoNav registration fiducial list doesn't exist yet, thus create it
        fiducialList = vtkMRMLFiducialListNode::New();
        fiducialList->SetName("AbdoNav-RegistrationFiducialList");
        fiducialList->SetDescription("Created by AbdoNav");
        // change default look ("StarBurst2D", 5) since it doesn't really
        // suit the purpose of needle identification; the user can always
        // return to the default look using Slicer's Fiducials module
        fiducialList->SetGlyphTypeFromString("Sphere3D");
        fiducialList->SetSymbolScale(2); // called "Glyph scale" in the Fiducials module
        this->GetMRMLScene()->AddNode(fiducialList);
        fiducialList->Delete();
        // update MRML node
        anode->SetRegistrationFiducialListID(fiducialList->GetID());
        // observe fiducial list in order to update the GUI whenever a fiducial is moved via drag & drop or renamed or renumbered externally via the Fiducials module
        fiducialList->AddObserver(vtkMRMLFiducialListNode::FiducialModifiedEvent, (vtkCommand*)this->MRMLCallbackCommand);
        // observe fiducial list in order to update the GUI whenever a fiducial is added externally via the Fiducials module
        fiducialList->AddObserver(vtkMRMLScene::NodeAddedEvent, (vtkCommand*)this->MRMLCallbackCommand);
        // observe fiducial list in order to update the GUI whenever a fiducial is removed externally via the Fiducials module
        fiducialList->AddObserver(vtkMRMLScene::NodeRemovedEvent, (vtkCommand*)this->MRMLCallbackCommand);
        // observe fiducial list in order to update the GUI whenever all fiducials are removed externally via the Fiducials module
        fiducialList->AddObserver(vtkCommand::ModifiedEvent, (vtkCommand*)this->MRMLCallbackCommand);
        // no need to observe vtkMRMLFiducialListNode::DisplayModifiedEvent or vtkMRMLFiducialListNode::FiducialIndexModifiedEvent
        }
      else
        {
        // AbdoNav fiducial list already exists, thus retrieve it
        fiducialList = vtkMRMLFiducialListNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(anode->GetRegistrationFiducialListID()));
        }

      // transform XY mouse coordinates into RAS coordinates
      //
      // first, find out in which slice view the user clicked
      // (necessary information for the XY to RAS conversion)
      vtkSlicerSliceGUI* sliceGUI = this->GetApplicationGUI()->GetMainSliceGUI("Red");
      vtkRenderWindowInteractor* rwi = sliceGUI->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor();

      int index = 0;
      while (style != rwi->GetInteractorStyle() && index < 2)
        {
        index++;
        if (index == 1)
          {
          sliceGUI = this->GetApplicationGUI()->GetMainSliceGUI("Yellow");
          }
        else
          {
          sliceGUI = this->GetApplicationGUI()->GetMainSliceGUI("Green");
          }
        rwi = sliceGUI->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor();
        }

      // second, transform XY mouse coordinates into RAS coordinates
      int xyPos[2];
      rwi->GetLastEventPosition(xyPos);
      double xyVec[4] = {xyPos[0], xyPos[1], 0, 1};
      double rasVec[4];
      vtkMatrix4x4* matrix = sliceGUI->GetLogic()->GetSliceNode()->GetXYToRAS();
      matrix->MultiplyPoint(xyVec, rasVec);

      // determine whether or not AbdoNav's fiducial list
      // already contains a fiducial corresponding to the
      // active check button
      //
      // if so, update it; otherwise, add a new fiducial
      bool fiducialExists = false;
      for (int i = 0; i < fiducialList->GetNumberOfFiducials(); i++)
        {
        if (!strcmp(activeCheckButton.c_str(), fiducialList->GetNthFiducialLabelText(i)))
          {
          // corresponding fiducial already exists, thus update it
          // (fiducial list implementation will invoke proper event
          // and thereby trigger vtkAbdoNavGUI::UpdateGUIFromMRML())
          fiducialList->SetNthFiducialXYZ(i, rasVec[0], rasVec[1], rasVec[2]);
          fiducialExists = true;
          }
        }
      if (fiducialExists == false)
        {
        // corresponding fiducial doesn't exist yet, thus create it
        // (fiducial list implementation will invoke proper event
        // and thereby trigger vtkAbdoNavGUI::UpdateGUIFromMRML())
        fiducialList->AddFiducialWithLabelXYZSelectedVisibility(activeCheckButton.c_str(), rasVec[0], rasVec[1], rasVec[2], 1, 1);
        }
      }
    }

  //----------------------------------------------------------------
  // Registration frame.
  //----------------------------------------------------------------
  else if (this->TrackerTransformSelector == vtkSlicerNodeSelectorWidget::SafeDownCast(caller) && event == vtkSlicerNodeSelectorWidget::NodeSelectedEvent)
    {
    vtkMRMLLinearTransformNode* tnode = vtkMRMLLinearTransformNode::SafeDownCast(this->TrackerTransformSelector->GetSelected());

    // only update MRML node if transform node was created by OpenIGTLinkIF
    if (tnode != NULL && tnode->GetDescription() != NULL && strcmp(tnode->GetDescription(), "Received by OpenIGTLink") == 0)
      {
      this->UpdateMRMLFromGUI();
      }
    else
      {
      vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                       this->GetApplicationGUI()->GetMainSlicerWindow(),
                                       "AbdoNav",
                                       "Selected transform node was not created by OpenIGTLinkIF!",
                                       vtkKWMessageDialog::ErrorIcon);
      this->TrackerTransformSelector->SetSelected(NULL);
      }
    }
  else if (this->GuidanceToolTypeMenuButton->GetMenu() == vtkKWMenu::SafeDownCast(caller) && event == vtkKWMenu::MenuItemInvokedEvent)
    {
    this->UpdateMRMLFromGUI();
    }
  else if (this->ToolBoxFileLoadButton->GetLoadSaveDialog() == vtkKWLoadSaveDialog::SafeDownCast(caller) && event == vtkKWTopLevel::WithdrawEvent)
    {
    // get path to selected file
    const char* path = this->ToolBoxFileLoadButton->GetLoadSaveDialog()->GetFileName();
    if (path)
      {
      // get filename (without the path)
      const vtksys_stl::string fname(path);
      vtksys_stl::string filename = vtksys::SystemTools::GetFilenameName(fname);

      // check if NDI ToolBox ".trackProperties" file was selected
      if (!strcmp(filename.c_str(), ".trackProperties"))
        {
        // set path
        this->CheckAndCreateAbdoNavNode();
        this->AbdoNavNode->SetToolBoxPropertiesFile(path);
        }
      else
        {
        // check if user previously provided the path to the NDI ToolBox ".trackProperties" file
        this->CheckAndCreateAbdoNavNode();
        if (this->AbdoNavNode->GetToolBoxPropertiesFile() != NULL)
          {
          // restore ToolBox file load button text and display an error message
          this->ToolBoxFileLoadButton->GetLoadSaveDialog()->SetFileName(this->AbdoNavNode->GetToolBoxPropertiesFile());
          vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                           this->GetApplicationGUI()->GetMainSlicerWindow(),
                                           "AbdoNav",
                                           "Wrong file selected! Will keep previously provided path to the NDI ToolBox \".trackProperties\" file.",
                                           vtkKWMessageDialog::ErrorIcon);
          }
        else
          {
          // reset ToolBox file load button text and display an error message
          this->ToolBoxFileLoadButton->SetText("Browse ...");
          vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                           this->GetApplicationGUI()->GetMainSlicerWindow(),
                                           "AbdoNav",
                                           "Provide path to the NDI ToolBox \".trackProperties\" file!",
                                           vtkKWMessageDialog::ErrorIcon);
          }
        }
      }
    }
  else if (this->Point1CheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    if (this->Point1CheckButton->GetSelectedState())
      {
      if (this->TimerLog == NULL)
        {
        // timer has not been started yet, thus start it now
        this->TimerLog = vtkTimerLog::New();
        this->TimerLog->StartTimer();
        }
      // mimic check button set behavior, i.e. only one check button allowed to be selected at a time
      this->Point2CheckButton->SelectedStateOff();
      this->Point3CheckButton->SelectedStateOff();
      this->Point4CheckButton->SelectedStateOff();
      this->Point5CheckButton->SelectedStateOff();
      }
    }
  else if (this->Point2CheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    if (this->Point2CheckButton->GetSelectedState())
      {
      if (this->TimerLog == NULL)
        {
        // timer has not been started yet, thus start it now
        this->TimerLog = vtkTimerLog::New();
        this->TimerLog->StartTimer();
        }
      // mimic check button set behavior, i.e. only one check button allowed to be selected at a time
      this->Point1CheckButton->SelectedStateOff();
      this->Point3CheckButton->SelectedStateOff();
      this->Point4CheckButton->SelectedStateOff();
      this->Point5CheckButton->SelectedStateOff();
      }
    }
  else if (this->Point3CheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    if (this->Point3CheckButton->GetSelectedState())
      {
      if (this->TimerLog == NULL)
        {
        // timer has not been started yet, thus start it now
        this->TimerLog = vtkTimerLog::New();
        this->TimerLog->StartTimer();
        }
      // mimic check button set behavior, i.e. only one check button allowed to be selected at a time
      this->Point1CheckButton->SelectedStateOff();
      this->Point2CheckButton->SelectedStateOff();
      this->Point4CheckButton->SelectedStateOff();
      this->Point5CheckButton->SelectedStateOff();
      }
    }
  else if (this->Point4CheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    if (this->Point4CheckButton->GetSelectedState())
      {
      if (this->TimerLog == NULL)
        {
        // timer has not been started yet, thus start it now
        this->TimerLog = vtkTimerLog::New();
        this->TimerLog->StartTimer();
        }
      // mimic check button set behavior, i.e. only one check button allowed to be selected at a time
      this->Point1CheckButton->SelectedStateOff();
      this->Point2CheckButton->SelectedStateOff();
      this->Point3CheckButton->SelectedStateOff();
      this->Point5CheckButton->SelectedStateOff();
      }
    }
  else if (this->Point5CheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    if (this->Point5CheckButton->GetSelectedState())
      {
      if (this->TimerLog == NULL)
        {
        // timer has not been started yet, thus start it now
        this->TimerLog = vtkTimerLog::New();
        this->TimerLog->StartTimer();
        }
      // mimic check button set behavior, i.e. only one check button allowed to be selected at a time
      this->Point1CheckButton->SelectedStateOff();
      this->Point2CheckButton->SelectedStateOff();
      this->Point3CheckButton->SelectedStateOff();
      this->Point4CheckButton->SelectedStateOff();
      }
    }
  else if (this->ResetRegistrationPushButton == vtkKWPushButton::SafeDownCast(caller) && event == vtkKWPushButton::InvokedEvent)
    {
    this->TrackerTransformSelector->SetEnabled(true);
    this->Point1CheckButton->SetEnabled(true);
    this->Point2CheckButton->SetEnabled(true);
    this->Point3CheckButton->SetEnabled(true);
    this->Point4CheckButton->SetEnabled(true);
    this->Point5CheckButton->SetEnabled(true);
    this->PerformRegistrationPushButton->SetEnabled(true);
    this->ResetRegistrationPushButton->SetEnabled(false);
    this->RecordLocatorPositionPushButton->SetEnabled(false);
    this->RecordLocatorPositionPushButton->SetBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIRed);
    this->RecordLocatorPositionPushButton->SetActiveBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIRed);
    // unlock fiducial list
    vtkMRMLFiducialListNode* fnode = vtkMRMLFiducialListNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->AbdoNavNode->GetRegistrationFiducialListID()));
    if (fnode)
      {
      fnode->SetLocked(0);
      }
    }
  else if (this->PerformRegistrationPushButton == vtkKWPushButton::SafeDownCast(caller) && event == vtkKWPushButton::InvokedEvent)
    {
    // verify registration input parameters
    this->CheckAndCreateAbdoNavNode();

    if (this->AbdoNavNode->GetTrackingTransformID() == NULL)
      {
      vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                       this->GetApplicationGUI()->GetMainSlicerWindow(),
                                       "AbdoNav",
                                       "Tracking transform node not specified!",
                                       vtkKWMessageDialog::ErrorIcon);
      }
    // need to check for "" as well due to UpdateMRMLFromGUI()
    else if (this->AbdoNavNode->GetGuidanceToolType() == NULL || (this->AbdoNavNode->GetGuidanceToolType() != NULL &&
             !strcmp(this->AbdoNavNode->GetGuidanceToolType(), "")))
      {
      vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                       this->GetApplicationGUI()->GetMainSlicerWindow(),
                                       "AbdoNav",
                                       "Guidance tool type not specified!",
                                       vtkKWMessageDialog::ErrorIcon);
      }
    else if (this->AbdoNavNode->GetToolBoxPropertiesFile() == NULL)
      {
      vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                       this->GetApplicationGUI()->GetMainSlicerWindow(),
                                       "AbdoNav",
                                       "Path to NDI ToolBox \".trackProperties\" file not specified!",
                                       vtkKWMessageDialog::ErrorIcon);
      }
    else if (this->AbdoNavNode->GetRegistrationFiducialListID() == NULL || (this->AbdoNavNode->GetRegistrationFiducialListID() != NULL &&
             vtkMRMLFiducialListNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->AbdoNavNode->GetRegistrationFiducialListID()))->GetNumberOfFiducials() < 3))
      {
      vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                       this->GetApplicationGUI()->GetMainSlicerWindow(),
                                       "AbdoNav",
                                       "Need to identify at least three fiducials in image space!",
                                       vtkKWMessageDialog::ErrorIcon);
      }
    else
      {
      if (this->AbdoNavLogic->ParseToolBoxProperties() == EXIT_SUCCESS)
        {
        if (this->AbdoNavLogic->PerformRegistration() == EXIT_SUCCESS)
          {
          if (this->TimerLog != NULL)
            {
            // stop timer, print time it took to perform the registration
            // and delete timer (resetting the registration and selecting
            // one of the three check buttons will create a new instance,
            // thereby resetting the timer)
            this->TimerLog->StopTimer();
            this->AbdoNavNode->SetElapsedTime(this->TimerLog->GetElapsedTime());
            std::cout << "===========================================================================" << std::endl;
            std::cout.setf(ios::scientific, ios::floatfield);
            std::cout.precision(8);
            std::cout << "time,," << this->AbdoNavNode->GetElapsedTime() << ",,[sec]" << std::endl;
            std::cout.unsetf(ios::floatfield);
            std::cout.precision(6);
            std::cout << "===========================================================================" << std::endl;
            this->TimerLog->Delete();
            this->TimerLog = NULL;
            }
          this->TrackerTransformSelector->SetEnabled(false);
          this->Point1CheckButton->SetEnabled(false);
          this->Point1CheckButton->SetSelectedState(false);
          this->Point2CheckButton->SetEnabled(false);
          this->Point2CheckButton->SetSelectedState(false);
          this->Point3CheckButton->SetEnabled(false);
          this->Point3CheckButton->SetSelectedState(false);
          this->Point4CheckButton->SetEnabled(false);
          this->Point4CheckButton->SetSelectedState(false);
          this->Point5CheckButton->SetEnabled(false);
          this->Point5CheckButton->SetSelectedState(false);
          this->PerformRegistrationPushButton->SetEnabled(false);
          this->ResetRegistrationPushButton->SetEnabled(true);
          this->RecordLocatorPositionPushButton->SetEnabled(true);
          this->RecordLocatorPositionPushButton->SetBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIGreen);
          this->RecordLocatorPositionPushButton->SetActiveBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIGreen);
          // lock fiducial list
          vtkMRMLFiducialListNode* fnode = vtkMRMLFiducialListNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->AbdoNavNode->GetRegistrationFiducialListID()));
          if (fnode)
            {
            fnode->SetLocked(1);
            }
          this->AbdoNavLogic->ObserveTrackingTransformNode();
          }
        else
          {
          vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                           this->GetApplicationGUI()->GetMainSlicerWindow(),
                                           "AbdoNav",
                                           "Registration failed, check input parameters!",
                                           vtkKWMessageDialog::ErrorIcon);
          }
        }
      else
        {
        vtkKWMessageDialog::PopupMessage(this->GetApplication(),
                                         this->GetApplicationGUI()->GetMainSlicerWindow(),
                                         "AbdoNav",
                                         "Parsing NDI ToolBox \".trackProperties\" file failed!",
                                         vtkKWMessageDialog::ErrorIcon);
        }
      }
    }

  //----------------------------------------------------------------
  // Navigation frame.
  //----------------------------------------------------------------
  else if (this->RecordLocatorPositionPushButton == vtkKWPushButton::SafeDownCast(caller) && event == vtkKWPushButton::InvokedEvent)
    {
    if (this->RecordLocatorPositionPushButton->GetEnabled())
      {
      this->RecordLocatorPositionPushButton->SetEnabled(false);
      this->RecordLocatorPositionPushButton->SetBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIYellow);
      this->RecordLocatorPositionPushButton->SetActiveBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIYellow);
      this->AbdoNavLogic->SetRecordLocatorPosition(true);
      }
    }
  else if (this->ShowLocatorCheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    int checked = this->ShowLocatorCheckButton->GetSelectedState();
    this->AbdoNavLogic->ToggleLocatorVisibility(checked);
    }
  else if (this->FreezeLocatorCheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    int checked = this->FreezeLocatorCheckButton->GetSelectedState();
    this->AbdoNavLogic->ToggleLocatorFreeze(checked);
    }
  else if (this->ShowCrosshairCheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    int checked = this->ShowCrosshairCheckButton->GetSelectedState();
    this->AbdoNavLogic->SetShowCrosshair(checked);

    // only set crosshair if not already set
    if (checked && this->AbdoNavLogic->GetCrosshair() == NULL)
      {
      vtkMRMLCrosshairNode* crosshair = this->GetApplicationGUI()->GetSlicesControlGUI()->GetCrosshairNode();
      if (crosshair)
        {
        crosshair->SetCrosshairName("AbdoNav-Crosshair");
        crosshair->SetCrosshairBehavior(vtkMRMLCrosshairNode::Normal);
        crosshair->SetCrosshairThickness(vtkMRMLCrosshairNode::Fine);
        crosshair->SetNavigation(1);
        this->AbdoNavLogic->SetCrosshair(crosshair);
        }
      }
    }
  else if (this->DrawNeedleProjectionCheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    int checked = this->DrawNeedleProjectionCheckButton->GetSelectedState();
    this->AbdoNavLogic->SetDrawNeedleProjection(checked);
    // set Slicer GUI if not already set
    if (checked && this->AbdoNavLogic->GetAppGUI() == NULL)
      {
      this->AbdoNavLogic->SetAppGUI(this->GetApplicationGUI());
      }
    }
  else if (this->RedSliceMenuButton->GetMenu() == vtkKWMenu::SafeDownCast(caller) && event == vtkKWMenu::MenuItemInvokedEvent)
    {
    this->AbdoNavLogic->SetSliceDriver(0, this->RedSliceMenuButton->GetValue());
    }
  else if (this->YellowSliceMenuButton->GetMenu() == vtkKWMenu::SafeDownCast(caller) && event == vtkKWMenu::MenuItemInvokedEvent)
    {
    this->AbdoNavLogic->SetSliceDriver(1, this->YellowSliceMenuButton->GetValue());
    }
  else if (this->GreenSliceMenuButton->GetMenu() == vtkKWMenu::SafeDownCast(caller) && event == vtkKWMenu::MenuItemInvokedEvent)
    {
    this->AbdoNavLogic->SetSliceDriver(2, this->GreenSliceMenuButton->GetValue());
    }
  else if (this->SetLocatorAllPushButton == vtkKWPushButton::SafeDownCast(caller) && event == vtkKWPushButton::InvokedEvent)
    {
    this->RedSliceMenuButton->SetValue("Locator");
    this->YellowSliceMenuButton->SetValue("Locator");
    this->GreenSliceMenuButton->SetValue("Locator");
    // setting a vtkKWMenuButton's value won't invoke
    // a MenuItemInvokedEvent; therefore, need to call
    // SetSliceDriver(...) manually
    this->AbdoNavLogic->SetSliceDriver(0, "Locator");
    this->AbdoNavLogic->SetSliceDriver(1, "Locator");
    this->AbdoNavLogic->SetSliceDriver(2, "Locator");
    }
  else if (this->SetUserAllPushButton == vtkKWPushButton::SafeDownCast(caller) && event == vtkKWPushButton::InvokedEvent)
    {
    this->RedSliceMenuButton->SetValue("User");
    this->YellowSliceMenuButton->SetValue("User");
    this->GreenSliceMenuButton->SetValue("User");
    // setting a vtkKWMenuButton's value won't invoke
    // a MenuItemInvokedEvent; therefore, need to call
    // SetSliceDriver(...) manually
    this->AbdoNavLogic->SetSliceDriver(0, "User");
    this->AbdoNavLogic->SetSliceDriver(1, "User");
    this->AbdoNavLogic->SetSliceDriver(2, "User");
    }
  else if (this->FreezeReslicingCheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    int checked = this->FreezeReslicingCheckButton->GetSelectedState();
    this->AbdoNavLogic->SetFreezeReslicing(checked);
    }
  else if (this->ObliqueReslicingCheckButton == vtkKWCheckButton::SafeDownCast(caller) && event == vtkKWCheckButton::SelectedStateChangedEvent)
    {
    int checked = this->ObliqueReslicingCheckButton->GetSelectedState();
    this->AbdoNavLogic->SetObliqueReslicing(checked);
    }
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::ProcessLogicEvents(vtkObject* caller, unsigned long event, void* vtkNotUsed(callData))
{
  if (this->AbdoNavLogic == vtkAbdoNavLogic::SafeDownCast(caller))
    {
    if (event == vtkAbdoNavLogic::RequestFitToBackground)
      {
      // logic requested to fit image data back into slice views
      this->GetApplicationGUI()->GetSlicesControlGUI()->FitSlicesToBackground();
      }
    else if (event == vtkAbdoNavLogic::LocatorPositionRecorded)
      {
      // done recording locator position
      this->RecordLocatorPositionPushButton->SetEnabled(true);
      this->RecordLocatorPositionPushButton->SetBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIGreen);
      this->RecordLocatorPositionPushButton->SetActiveBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIGreen);
      }
    }
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::ProcessMRMLEvents(vtkObject* caller, unsigned long event, void* callData)
{
  if (event == vtkMRMLScene::NodeAddedEvent)
    {
    // update the GUI if an AbdoNavNode was added; an AbdoNavNode is only added
    // when the user loads a previously saved scene that contains an AbdoNavNode
    vtkMRMLAbdoNavNode* anode = vtkMRMLAbdoNavNode::SafeDownCast((vtkObject*)callData);
    if (anode != NULL)
      {
      // a new AbdoNavNode was created and added
      if (this->AbdoNavNode == NULL)
        {
        // set and observe the new node in Logic
        this->AbdoNavLogic->SetAndObserveAbdoNavNode(anode);
        // set and observe the new node in GUI
        vtkSetAndObserveMRMLNodeMacro(this->AbdoNavNode, anode);
        // if an AbdoNav fiducial list is part of the loaded scene, observe it in order to update the GUI
        vtkMRMLFiducialListNode* fiducialList = vtkMRMLFiducialListNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(this->AbdoNavNode->GetRegistrationFiducialListID()));
        if (fiducialList != NULL)
          {
          // observe fiducial list in order to update the GUI whenever a fiducial is moved via drag & drop or renamed or renumbered externally via the Fiducials module
          fiducialList->AddObserver(vtkMRMLFiducialListNode::FiducialModifiedEvent, (vtkCommand*)this->MRMLCallbackCommand);
          // observe fiducial list in order to update the GUI whenever a fiducial is added externally via the Fiducials module
          fiducialList->AddObserver(vtkMRMLScene::NodeAddedEvent, (vtkCommand*)this->MRMLCallbackCommand);
          // observe fiducial list in order to update the GUI whenever a fiducial is removed externally via the Fiducials module
          fiducialList->AddObserver(vtkMRMLScene::NodeRemovedEvent, (vtkCommand*)this->MRMLCallbackCommand);
          // observe fiducial list in order to update the GUI whenever all fiducials are removed externally via the Fiducials module
          fiducialList->AddObserver(vtkCommand::ModifiedEvent, (vtkCommand*)this->MRMLCallbackCommand);
          // no need to observe vtkMRMLFiducialListNode::DisplayModifiedEvent or vtkMRMLFiducialListNode::FiducialIndexModifiedEvent
          }
        this->UpdateGUIFromMRML();
        }
      }

    // update the GUI if a fiducial was added externally via the Fiducials module
    vtkMRMLFiducialListNode* fnode = vtkMRMLFiducialListNode::SafeDownCast(caller);
    if (fnode != NULL && this->AbdoNavNode != NULL)
      {
      if (!strcmp(fnode->GetID(), this->AbdoNavNode->GetRegistrationFiducialListID()))
        {
        //std::cout << "fiducial added" << std::endl;
        this->UpdateGUIFromMRML();
        }
      }
    }
  else if (event == vtkMRMLScene::NodeRemovedEvent)
    {
    // update the GUI if a fiducial was removed externally via the Fiducials module
    vtkMRMLFiducialListNode* fnode = vtkMRMLFiducialListNode::SafeDownCast(caller);
    if (fnode != NULL && this->AbdoNavNode != NULL)
      {
      if (!strcmp(fnode->GetID(), this->AbdoNavNode->GetRegistrationFiducialListID()))
        {
        //std::cout << "fiducial removed" << std::endl;
        this->UpdateGUIFromMRML();
        }
      }
    }
  else if (event == vtkCommand::ModifiedEvent)
    {
    // update the GUI if an existing AbdoNavNode was modified
    vtkMRMLAbdoNavNode* anode = vtkMRMLAbdoNavNode::SafeDownCast(caller);
    if (anode != NULL && this->AbdoNavNode != NULL)
      {
      if (!strcmp(anode->GetID(), this->AbdoNavNode->GetID()))
        {
        this->UpdateGUIFromMRML();
        }
      }

    // update the GUI if all fiducials were removed externally via the Fiducials module
    vtkMRMLFiducialListNode* fnode = vtkMRMLFiducialListNode::SafeDownCast(caller);
    if (fnode != NULL && this->AbdoNavNode != NULL)
      {
      if (!strcmp(fnode->GetID(), this->AbdoNavNode->GetRegistrationFiducialListID()))
        {
        //std::cout << "all fiducials removed" << std::endl;
        this->UpdateGUIFromMRML();
        }
      }
    }
  else if (event == vtkMRMLFiducialListNode::FiducialModifiedEvent)
    {
    // update the GUI if a fiducial was moved via drag & drop or renamed or renumbered externally via the Fiducials module
    vtkMRMLFiducialListNode* fnode = vtkMRMLFiducialListNode::SafeDownCast(caller);
    if (fnode != NULL && this->AbdoNavNode != NULL)
      {
      if (!strcmp(fnode->GetID(), this->AbdoNavNode->GetRegistrationFiducialListID()))
        {
        //std::cout << "fiducial moved or renamed" << std::endl;
        this->UpdateGUIFromMRML();
        }
      }
    }
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::UpdateMRMLFromGUI()
{
  // create an AbdoNavNode if none exists yet
  vtkMRMLAbdoNavNode* node = this->CheckAndCreateAbdoNavNode();

  // save old node parameters for undo mechanism
  this->AbdoNavLogic->GetMRMLScene()->SaveStateForUndo(node);

  // set new node parameters from GUI widgets:
  // make sure that only ONE modified event is invoked (if
  // at all) instead of one modified event per changed value
  int modifiedFlag = node->StartModify();
  vtkMRMLLinearTransformNode* tnode = vtkMRMLLinearTransformNode::SafeDownCast(this->TrackerTransformSelector->GetSelected());
  if (tnode != NULL)
    {
    node->SetTrackingTransformID(tnode->GetID());
    }
  node->SetGuidanceToolType(this->GuidanceToolTypeMenuButton->GetValue());
  node->EndModify(modifiedFlag);
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::UpdateGUIFromMRML()
{
  vtkMRMLAbdoNavNode* node = this->AbdoNavNode;
  if (node != NULL)
    {
    // set GUI widgets from AbdoNav parameter node
    vtkMRMLNode* tnode = this->GetMRMLScene()->GetNodeByID(node->GetTrackingTransformID());
    this->TrackerTransformSelector->SetSelected(tnode);

    vtkMRMLFiducialListNode* fnode = vtkMRMLFiducialListNode::SafeDownCast(this->GetMRMLScene()->GetNodeByID(node->GetRegistrationFiducialListID()));
    if (fnode != NULL)
      {
      // need to set all values to NaN in case AbdoNav's fiducial list was modified,
      // i.e. in case a fiducial was removed or renamed (unsupported identifier) ex-
      // ternally via the Fiducials module
      this->Point1REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point1AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point1SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point2REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point2AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point2SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point3REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point3AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
      this->Point3SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());

      float* tmp = NULL;
      for (int i = 0; i < fnode->GetNumberOfFiducials(); i++)
        {
        if (!strcmp(tip, fnode->GetNthFiducialLabelText(i)))
          {
          tmp = fnode->GetNthFiducialXYZ(i);
          this->Point1REntry->SetValueAsDouble(tmp[0]);
          this->Point1AEntry->SetValueAsDouble(tmp[1]);
          this->Point1SEntry->SetValueAsDouble(tmp[2]);
          }
        else if (!strcmp(markerA, fnode->GetNthFiducialLabelText(i)))
          {
          tmp = fnode->GetNthFiducialXYZ(i);
          this->Point2REntry->SetValueAsDouble(tmp[0]);
          this->Point2AEntry->SetValueAsDouble(tmp[1]);
          this->Point2SEntry->SetValueAsDouble(tmp[2]);
          }
        else if (!strcmp(markerB, fnode->GetNthFiducialLabelText(i)))
          {
          tmp = fnode->GetNthFiducialXYZ(i);
          this->Point3REntry->SetValueAsDouble(tmp[0]);
          this->Point3AEntry->SetValueAsDouble(tmp[1]);
          this->Point3SEntry->SetValueAsDouble(tmp[2]);
          }
        else if (!strcmp(markerC, fnode->GetNthFiducialLabelText(i)))
          {
          tmp = fnode->GetNthFiducialXYZ(i);
          this->Point4REntry->SetValueAsDouble(tmp[0]);
          this->Point4AEntry->SetValueAsDouble(tmp[1]);
          this->Point4SEntry->SetValueAsDouble(tmp[2]);
          }
        else if (!strcmp(markerD, fnode->GetNthFiducialLabelText(i)))
          {
          tmp = fnode->GetNthFiducialXYZ(i);
          this->Point5REntry->SetValueAsDouble(tmp[0]);
          this->Point5AEntry->SetValueAsDouble(tmp[1]);
          this->Point5SEntry->SetValueAsDouble(tmp[2]);
          }
        }
      }
    }
}


//---------------------------------------------------------------------------
vtkMRMLAbdoNavNode* vtkAbdoNavGUI::CheckAndCreateAbdoNavNode()
{
  if (this->AbdoNavNode == NULL)
    {
    // no AbdoNav node present yet, thus create a new one
    vtkMRMLAbdoNavNode* node = vtkMRMLAbdoNavNode::New();
    // add the new node to this MRML scene but don't notify:
    // this way, it is known that a node added event is only
    // invoked when the user loads a previously saved scene
    // containing an AbdoNavNode
    this->GetMRMLScene()->AddNodeNoNotify(node);
    // set and observe the new node in Logic
    this->AbdoNavLogic->SetAndObserveAbdoNavNode(node);
    // set and observe the new node in GUI
    vtkSetAndObserveMRMLNodeMacro(this->AbdoNavNode, node);
    node->Delete();
   }

  return this->AbdoNavNode;
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::BuildGUI()
{
  // create a page
  this->UIPanel->AddPage("AbdoNav", "AbdoNav", NULL);

  // build the different GUI frames
  this->BuildGUIHelpFrame();
  this->BuildGUIRegistrationFrame();
  this->BuildGUINavigationFrame();
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::BuildGUIHelpFrame()
{
  // help text
  const char* help =
    "The **AbdoNav** module is meant to be used in CT-guided abdominal interventions which make use of a "
    "guidance needle (like e.g. cryosurgeries for liver and kidney tumor treatment)."
    "\n"
    "See <a>http://www.slicer.org/slicerWiki/index.php/Modules:AbdoNav-Documentation-3.6</a> for details "
    "about the module.";
  // about text
  const char* about =
    "The **AbdoNav** module was contributed by Christoph Ammann (Karlsruhe Institute of Technology, KIT) "
    "and Nobuhiko Hata, PhD (Surgical Navigation and Robotics Laboratory, SNR).";

  vtkKWWidget* page = this->UIPanel->GetPageWidget("AbdoNav");
  this->BuildHelpAndAboutFrame(page, help, about);
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::BuildGUIRegistrationFrame()
{
  vtkKWWidget* page = this->UIPanel->GetPageWidget("AbdoNav");

  // create collapsible registration frame
  vtkSlicerModuleCollapsibleFrame* registrationFrame = vtkSlicerModuleCollapsibleFrame::New();
  registrationFrame->SetParent(page);
  registrationFrame->Create();
  registrationFrame->SetLabelText("Registration");
  registrationFrame->CollapseFrame();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2 -in %s",
                registrationFrame->GetWidgetName(),
                page->GetWidgetName());

  // create labelled frame to hold widgets for specifying the tracking information
  vtkKWFrameWithLabel* trackerFrame = vtkKWFrameWithLabel::New();
  trackerFrame->SetParent(registrationFrame->GetFrame());
  trackerFrame->Create();
  trackerFrame->SetLabelText("Specify tracking information");
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", trackerFrame->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to specify the tracking information.
  //----------------------------------------------------------------
  // create selector to specify the input tracker transform node
  this->TrackerTransformSelector = vtkSlicerNodeSelectorWidget::New();
  this->TrackerTransformSelector->SetParent(trackerFrame->GetFrame());
  this->TrackerTransformSelector->Create();
  this->TrackerTransformSelector->SetLabelText("Tracker transform node:\t\t");
  this->TrackerTransformSelector->SetBalloonHelpString("Select the transform node created by OpenIGTLinkIF that holds the tracking data of the current cryoprobe relative to the guidance needle.");
  this->TrackerTransformSelector->SetNodeClass("vtkMRMLLinearTransformNode", NULL, NULL, "LinearTransform"); // filter: only show vtkMRMLLinearTransformNodes
  this->TrackerTransformSelector->SetMRMLScene(this->GetMRMLScene());
  this->TrackerTransformSelector->SetNewNodeEnabled(0); // turn off user option to create new linear transform nodes
  this->TrackerTransformSelector->SetDefaultEnabled(0); // turn off autoselecting nodes
  this->TrackerTransformSelector->GetWidget()->GetWidget()->IndicatorVisibilityOff(); // don't show indicator

  // add tracker transform selector
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", this->TrackerTransformSelector->GetWidgetName());

  // create frame required to display the label and tool type menu button on the left and right side respectively
  vtkKWFrame* toolFrame = vtkKWFrame::New();
  toolFrame->SetParent(trackerFrame->GetFrame());
  toolFrame->Create();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", toolFrame->GetWidgetName());

  // create label for the guidance tool type menu button
  this->GuidanceToolTypeLabel = vtkKWLabel::New();
  this->GuidanceToolTypeLabel->SetParent(toolFrame);
  this->GuidanceToolTypeLabel->Create();
  this->GuidanceToolTypeLabel->SetText("Guidance needle tool type:");

  // add label for the guidance tool type menu button
  this->Script ("pack %s -side left -anchor nw  -padx 2 -pady 2", this->GuidanceToolTypeLabel->GetWidgetName());

  // create menu button to set the guidance tool type being used
  this->GuidanceToolTypeMenuButton = vtkKWMenuButton::New();
  this->GuidanceToolTypeMenuButton->SetParent(toolFrame);
  this->GuidanceToolTypeMenuButton->Create();
  this->GuidanceToolTypeMenuButton->SetBalloonHelpString("Specify the guidance needle tool type being used.");
  this->GuidanceToolTypeMenuButton->SetWidth(13);
  this->GuidanceToolTypeMenuButton->GetMenu()->AddRadioButton("8700338");
  this->GuidanceToolTypeMenuButton->GetMenu()->AddRadioButton("8700339");
  this->GuidanceToolTypeMenuButton->GetMenu()->AddRadioButton("8700340");
  // The following line could be deleted. Note, however, that the current implementation demands that the
  // guidance tool type menu button *MUST NOT* be pre-initialized! If the user's choice matches the pre-
  // initialized value (i.e. the user doesn't need to change it), the vtkMRMLAbdoNavNode's corresponding
  // value won't be set. By not-pre-initializing the guidance tool type menu button, the user's forced to
  // set a value which in turn will update the vtkMRMLAbdoNavNode.
  this->GuidanceToolTypeMenuButton->SetValue("");

  // add guidance tool type menu button
  this->Script ("pack %s -side right -anchor ne -padx 2 -pady 2", this->GuidanceToolTypeMenuButton->GetWidgetName());

  // create frame required to display the label and NDI ToolBox ".trackProperties" file load button on the left and right side respectively
  vtkKWFrame* fileFrame = vtkKWFrame::New();
  fileFrame->SetParent(trackerFrame->GetFrame());
  fileFrame->Create();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", fileFrame->GetWidgetName());

  // create label for the NDI ToolBox ".trackProperties" file load button
  this->ToolBoxFileLoadLabel = vtkKWLabel::New();
  this->ToolBoxFileLoadLabel->SetParent(fileFrame);
  this->ToolBoxFileLoadLabel->Create();
  this->ToolBoxFileLoadLabel->SetText("ToolBox properties file:");

  // add label for the NDI ToolBox ".trackProperties" file load button
  this->Script ("pack %s -side left -anchor nw  -padx 2 -pady 2", this->ToolBoxFileLoadLabel->GetWidgetName());

  // create load button to specify the path to the NDI ToolBox ".trackProperties" file
  this->ToolBoxFileLoadButton = vtkKWLoadSaveButton::New();
  this->ToolBoxFileLoadButton->SetParent(fileFrame);
  this->ToolBoxFileLoadButton->Create();
  this->ToolBoxFileLoadButton->SetText("Browse ...");
  this->ToolBoxFileLoadButton->GetLoadSaveDialog()->SetMasterWindow(this->GetApplicationGUI()->GetMainSlicerWindow());
  this->ToolBoxFileLoadButton->GetLoadSaveDialog()->SetTitle("Open NDI ToolBox \".trackProperties\" file");

  // add NDI ToolBox ".trackProperties" file load button
  this->Script ("pack %s -side right -anchor ne -padx 2 -pady 2", this->ToolBoxFileLoadButton->GetWidgetName());

  // create labelled frame to hold widgets for identifying the guidance needle
  vtkKWFrameWithLabel* guidanceNeedleFrame = vtkKWFrameWithLabel::New();
  guidanceNeedleFrame->SetParent(registrationFrame->GetFrame());
  guidanceNeedleFrame->Create();
  guidanceNeedleFrame->SetLabelText("Identify guidance needle");
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", guidanceNeedleFrame->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to identify the guidance needle tip.
  //----------------------------------------------------------------
  // create frame required to display the check button and RAS coordinate entries on the left and right side respectively
  vtkKWFrame* point1Frame = vtkKWFrame::New();
  point1Frame->SetParent(guidanceNeedleFrame->GetFrame());
  point1Frame->Create();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", point1Frame->GetWidgetName());

  // create check button to select the guidance needle tip
  this->Point1CheckButton = vtkKWCheckButton::New();
  this->Point1CheckButton->SetParent(point1Frame);
  this->Point1CheckButton->Create();
  this->Point1CheckButton->SetText("Tip of guidance needle (RAS):");
  this->Point1CheckButton->SetBalloonHelpString("Identify the tip of the guidance needle in the CT/MR image.");
  this->Point1CheckButton->SetSelectedState(false);
  // create entry to hold the R coordinate of the guidance needle tip
  this->Point1REntry = vtkKWEntry::New();
  this->Point1REntry->SetParent(point1Frame);
  this->Point1REntry->Create();
  this->Point1REntry->SetBalloonHelpString("Guidance needle tip, R coordinate.");
  this->Point1REntry->SetWidth(8);
  this->Point1REntry->SetReadOnly(1);
  this->Point1REntry->SetRestrictValueToDouble();
  this->Point1REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the A coordinate of the guidance needle tip
  this->Point1AEntry = vtkKWEntry::New();
  this->Point1AEntry->SetParent(point1Frame);
  this->Point1AEntry->Create();
  this->Point1AEntry->SetBalloonHelpString("Guidance needle tip, A coordinate.");
  this->Point1AEntry->SetWidth(8);
  this->Point1AEntry->SetReadOnly(1);
  this->Point1AEntry->SetRestrictValueToDouble();
  this->Point1AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the S coordinate of the guidance needle tip
  this->Point1SEntry = vtkKWEntry::New();
  this->Point1SEntry->SetParent(point1Frame);
  this->Point1SEntry->Create();
  this->Point1SEntry->SetBalloonHelpString("Guidance needle tip, S coordinate.");
  this->Point1SEntry->SetWidth(8);
  this->Point1SEntry->SetReadOnly(1);
  this->Point1SEntry->SetRestrictValueToDouble();
  this->Point1SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());

  // add check button for the guidance needle tip
  this->Script ("pack %s -side left -anchor nw  -padx 2 -pady 2", this->Point1CheckButton->GetWidgetName());
  // add RAS coordinate entries for the guidance needle tip
  this->Script ("pack %s %s %s -side right -anchor ne -padx 2 -pady 2",
                 this->Point1SEntry->GetWidgetName(),
                 this->Point1AEntry->GetWidgetName(),
                 this->Point1REntry->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to identify marker A's center.
  //----------------------------------------------------------------
  // create frame required to display the check button and RAS coordinate entries on the left and right side respectively
  vtkKWFrame* point2Frame = vtkKWFrame::New();
  point2Frame->SetParent(guidanceNeedleFrame->GetFrame());
  point2Frame->Create();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", point2Frame->GetWidgetName());

  // create check button to select marker A's center
  this->Point2CheckButton = vtkKWCheckButton::New();
  this->Point2CheckButton->SetParent(point2Frame);
  this->Point2CheckButton->Create();
  this->Point2CheckButton->SetText("Center of marker A (RAS):");
  this->Point2CheckButton->SetBalloonHelpString("Identify the center of marker A in the CT/MR image.");
  this->Point2CheckButton->SetSelectedState(false);
  // create entry to hold the R coordinate of marker A's center
  this->Point2REntry = vtkKWEntry::New();
  this->Point2REntry->SetParent(point2Frame);
  this->Point2REntry->Create();
  this->Point2REntry->SetBalloonHelpString("Marker A's center, R coordinate.");
  this->Point2REntry->SetWidth(8);
  this->Point2REntry->SetReadOnly(1);
  this->Point2REntry->SetRestrictValueToDouble();
  this->Point2REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the A coordinate of marker A's center
  this->Point2AEntry = vtkKWEntry::New();
  this->Point2AEntry->SetParent(point2Frame);
  this->Point2AEntry->Create();
  this->Point2AEntry->SetBalloonHelpString("Marker A's center, A coordinate.");
  this->Point2AEntry->SetWidth(8);
  this->Point2AEntry->SetReadOnly(1);
  this->Point2AEntry->SetRestrictValueToDouble();
  this->Point2AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the S coordinate of marker A's center
  this->Point2SEntry = vtkKWEntry::New();
  this->Point2SEntry->SetParent(point2Frame);
  this->Point2SEntry->Create();
  this->Point2SEntry->SetBalloonHelpString("Marker A's center, S coordinate.");
  this->Point2SEntry->SetWidth(8);
  this->Point2SEntry->SetReadOnly(1);
  this->Point2SEntry->SetRestrictValueToDouble();
  this->Point2SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());

  // add check button for marker A's center
  this->Script ("pack %s -side left -anchor nw  -padx 2 -pady 2", this->Point2CheckButton->GetWidgetName());
  // add RAS coordinate entries for marker A's center
  this->Script ("pack %s %s %s -side right -anchor ne -padx 2 -pady 2",
                 this->Point2SEntry->GetWidgetName(),
                 this->Point2AEntry->GetWidgetName(),
                 this->Point2REntry->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to identify marker B's center.
  //----------------------------------------------------------------
  // create frame required to display the check button and RAS coordinate entries on the left and right side respectively
  vtkKWFrame* point3Frame = vtkKWFrame::New();
  point3Frame->SetParent(guidanceNeedleFrame->GetFrame());
  point3Frame->Create();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", point3Frame->GetWidgetName());

  // create check button to select marker B's center
  this->Point3CheckButton = vtkKWCheckButton::New();
  this->Point3CheckButton->SetParent(point3Frame);
  this->Point3CheckButton->Create();
  this->Point3CheckButton->SetText("Center of marker B (RAS):");
  this->Point3CheckButton->SetBalloonHelpString("Identify the center of marker B in the CT/MR image.");
  this->Point3CheckButton->SetSelectedState(false);
  // create entry to hold the R coordinate of marker B's center
  this->Point3REntry = vtkKWEntry::New();
  this->Point3REntry->SetParent(point3Frame);
  this->Point3REntry->Create();
  this->Point3REntry->SetBalloonHelpString("Marker B's center, R coordinate.");
  this->Point3REntry->SetWidth(8);
  this->Point3REntry->SetReadOnly(1);
  this->Point3REntry->SetRestrictValueToDouble();
  this->Point3REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the A coordinate of marker B's center
  this->Point3AEntry = vtkKWEntry::New();
  this->Point3AEntry->SetParent(point3Frame);
  this->Point3AEntry->Create();
  this->Point3AEntry->SetBalloonHelpString("Marker B's center, A coordinate.");
  this->Point3AEntry->SetWidth(8);
  this->Point3AEntry->SetReadOnly(1);
  this->Point3AEntry->SetRestrictValueToDouble();
  this->Point3AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the S coordinate of marker B's center
  this->Point3SEntry = vtkKWEntry::New();
  this->Point3SEntry->SetParent(point3Frame);
  this->Point3SEntry->Create();
  this->Point3SEntry->SetBalloonHelpString("Marker B's center, S coordinate.");
  this->Point3SEntry->SetWidth(8);
  this->Point3SEntry->SetReadOnly(1);
  this->Point3SEntry->SetRestrictValueToDouble();
  this->Point3SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());

  // add check button for marker B's center
  this->Script ("pack %s -side left -anchor nw  -padx 2 -pady 2", this->Point3CheckButton->GetWidgetName());
  // add RAS coordinate entries for marker B's center
  this->Script ("pack %s %s %s -side right -anchor ne -padx 2 -pady 2",
                 this->Point3SEntry->GetWidgetName(),
                 this->Point3AEntry->GetWidgetName(),
                 this->Point3REntry->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to identify marker C's center.
  //----------------------------------------------------------------
  // create frame required to display the check button and RAS coordinate entries on the left and right side respectively
  vtkKWFrame* point4Frame = vtkKWFrame::New();
  point4Frame->SetParent(guidanceNeedleFrame->GetFrame());
  point4Frame->Create();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", point4Frame->GetWidgetName());

  // create check button to select marker C's center
  this->Point4CheckButton = vtkKWCheckButton::New();
  this->Point4CheckButton->SetParent(point4Frame);
  this->Point4CheckButton->Create();
  this->Point4CheckButton->SetText("Center of marker C (RAS):");
  this->Point4CheckButton->SetBalloonHelpString("Identify the center of marker C in the CT/MR image.");
  this->Point4CheckButton->SetSelectedState(false);
  // create entry to hold the R coordinate of marker C's center
  this->Point4REntry = vtkKWEntry::New();
  this->Point4REntry->SetParent(point4Frame);
  this->Point4REntry->Create();
  this->Point4REntry->SetBalloonHelpString("Marker C's center, R coordinate.");
  this->Point4REntry->SetWidth(8);
  this->Point4REntry->SetReadOnly(1);
  this->Point4REntry->SetRestrictValueToDouble();
  this->Point4REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the A coordinate of marker C's center
  this->Point4AEntry = vtkKWEntry::New();
  this->Point4AEntry->SetParent(point4Frame);
  this->Point4AEntry->Create();
  this->Point4AEntry->SetBalloonHelpString("Marker C's center, A coordinate.");
  this->Point4AEntry->SetWidth(8);
  this->Point4AEntry->SetReadOnly(1);
  this->Point4AEntry->SetRestrictValueToDouble();
  this->Point4AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the S coordinate of marker C's center
  this->Point4SEntry = vtkKWEntry::New();
  this->Point4SEntry->SetParent(point4Frame);
  this->Point4SEntry->Create();
  this->Point4SEntry->SetBalloonHelpString("Marker C's center, S coordinate.");
  this->Point4SEntry->SetWidth(8);
  this->Point4SEntry->SetReadOnly(1);
  this->Point4SEntry->SetRestrictValueToDouble();
  this->Point4SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());

  // add check button for marker C's center
  this->Script ("pack %s -side left -anchor nw  -padx 2 -pady 2", this->Point4CheckButton->GetWidgetName());
  // add RAS coordinate entries for marker C's center
  this->Script ("pack %s %s %s -side right -anchor ne -padx 2 -pady 2",
                 this->Point4SEntry->GetWidgetName(),
                 this->Point4AEntry->GetWidgetName(),
                 this->Point4REntry->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to identify marker D's center.
  //----------------------------------------------------------------
  // create frame required to display the check button and RAS coordinate entries on the left and right side respectively
  vtkKWFrame* point5Frame = vtkKWFrame::New();
  point5Frame->SetParent(guidanceNeedleFrame->GetFrame());
  point5Frame->Create();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", point5Frame->GetWidgetName());

  // create check button to select marker D's center
  this->Point5CheckButton = vtkKWCheckButton::New();
  this->Point5CheckButton->SetParent(point5Frame);
  this->Point5CheckButton->Create();
  this->Point5CheckButton->SetText("Center of marker D (RAS):");
  this->Point5CheckButton->SetBalloonHelpString("Identify the center of marker D in the CT/MR image.");
  this->Point5CheckButton->SetSelectedState(false);
  // create entry to hold the R coordinate of marker D's center
  this->Point5REntry = vtkKWEntry::New();
  this->Point5REntry->SetParent(point5Frame);
  this->Point5REntry->Create();
  this->Point5REntry->SetBalloonHelpString("Marker D's center, R coordinate.");
  this->Point5REntry->SetWidth(8);
  this->Point5REntry->SetReadOnly(1);
  this->Point5REntry->SetRestrictValueToDouble();
  this->Point5REntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the A coordinate of marker D's center
  this->Point5AEntry = vtkKWEntry::New();
  this->Point5AEntry->SetParent(point5Frame);
  this->Point5AEntry->Create();
  this->Point5AEntry->SetBalloonHelpString("Marker D's center, A coordinate.");
  this->Point5AEntry->SetWidth(8);
  this->Point5AEntry->SetReadOnly(1);
  this->Point5AEntry->SetRestrictValueToDouble();
  this->Point5AEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());
  // create entry to hold the S coordinate of marker D's center
  this->Point5SEntry = vtkKWEntry::New();
  this->Point5SEntry->SetParent(point5Frame);
  this->Point5SEntry->Create();
  this->Point5SEntry->SetBalloonHelpString("Marker D's center, S coordinate.");
  this->Point5SEntry->SetWidth(8);
  this->Point5SEntry->SetReadOnly(1);
  this->Point5SEntry->SetRestrictValueToDouble();
  this->Point5SEntry->SetValueAsDouble(std::numeric_limits<double>::quiet_NaN());

  // add check button for marker D's center
  this->Script ("pack %s -side left -anchor nw  -padx 2 -pady 2", this->Point5CheckButton->GetWidgetName());
  // add RAS coordinate entries for marker D's center
  this->Script ("pack %s %s %s -side right -anchor ne -padx 2 -pady 2",
                 this->Point5SEntry->GetWidgetName(),
                 this->Point5AEntry->GetWidgetName(),
                 this->Point5REntry->GetWidgetName());

  //----------------------------------------------------------------
  // Create buttons to reset and perform the registration.
  //----------------------------------------------------------------
  // create button to reset the registration
  this->ResetRegistrationPushButton = vtkKWPushButton::New();
  this->ResetRegistrationPushButton->SetParent(registrationFrame->GetFrame());
  this->ResetRegistrationPushButton->Create();
  this->ResetRegistrationPushButton->SetText("Reset Registration");
  this->ResetRegistrationPushButton->SetBalloonHelpString("Redo identification of guidance needle.");
  this->ResetRegistrationPushButton->SetEnabled(false);

  // add reset registration button
  this->Script("pack %s -side left -anchor nw -padx 2 -pady 2", this->ResetRegistrationPushButton->GetWidgetName());

  // create button to perform the registration
  this->PerformRegistrationPushButton = vtkKWPushButton::New();
  this->PerformRegistrationPushButton->SetParent(registrationFrame->GetFrame());
  this->PerformRegistrationPushButton->Create();
  this->PerformRegistrationPushButton->SetText("Perform Registration");
  this->PerformRegistrationPushButton->SetBalloonHelpString("Perform registration based on current identification of guidance needle.");

  // add perform registration button
  this->Script("pack %s -side right -anchor ne -padx 2 -pady 2", this->PerformRegistrationPushButton->GetWidgetName());

  // clean up
  registrationFrame->Delete();
  trackerFrame->Delete();
  toolFrame->Delete();
  fileFrame->Delete();
  guidanceNeedleFrame->Delete();
  point1Frame->Delete();
  point2Frame->Delete();
  point3Frame->Delete();
  point4Frame->Delete();
  point5Frame->Delete();
}


//---------------------------------------------------------------------------
void vtkAbdoNavGUI::BuildGUINavigationFrame()
{
  vtkKWWidget* page = this->UIPanel->GetPageWidget("AbdoNav");

  // create collapsible navigation frame
  vtkSlicerModuleCollapsibleFrame* navigationFrame = vtkSlicerModuleCollapsibleFrame::New();
  navigationFrame->SetParent(page);
  navigationFrame->Create();
  navigationFrame->SetLabelText("Navigation");
  navigationFrame->CollapseFrame();
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2 -in %s",
                navigationFrame->GetWidgetName(),
                page->GetWidgetName());

  // create labelled frame to hold widgets for the evaluation
  vtkKWFrameWithLabel* evaluationFrame = vtkKWFrameWithLabel::New();
  evaluationFrame->SetParent(navigationFrame->GetFrame());
  evaluationFrame->Create();
  evaluationFrame->SetLabelText("Evaluation");
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", evaluationFrame->GetWidgetName());

  // create button to record the current locator position
  this->RecordLocatorPositionPushButton = vtkKWPushButton::New();
  this->RecordLocatorPositionPushButton->SetParent(evaluationFrame->GetFrame());
  this->RecordLocatorPositionPushButton->Create();
  this->RecordLocatorPositionPushButton->SetText("Record Locator Position");
  this->RecordLocatorPositionPushButton->SetBalloonHelpString("Record the current locator position in image space.");
  this->RecordLocatorPositionPushButton->SetEnabled(false);
  this->RecordLocatorPositionPushButton->SetBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIRed);
  this->RecordLocatorPositionPushButton->SetActiveBackgroundColor(((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors()->SliceGUIRed);

  // add record locator position button
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", this->RecordLocatorPositionPushButton->GetWidgetName());

  // create labelled frame to hold widgets for setting the locator display options
  vtkKWFrameWithLabel* locatorOptionsFrame = vtkKWFrameWithLabel::New();
  locatorOptionsFrame->SetParent(navigationFrame->GetFrame());
  locatorOptionsFrame->Create();
  locatorOptionsFrame->SetLabelText("Locator display options");
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", locatorOptionsFrame->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to set the locator display options.
  //----------------------------------------------------------------
  // create check button to show/hide the locator
  this->ShowLocatorCheckButton = vtkKWCheckButton::New();
  this->ShowLocatorCheckButton->SetParent(locatorOptionsFrame->GetFrame());
  this->ShowLocatorCheckButton->Create();
  this->ShowLocatorCheckButton->SetText("Show locator");
  this->ShowLocatorCheckButton->SetBalloonHelpString("Show/hide the locator.");
  this->ShowLocatorCheckButton->SetSelectedState(false);

  // add show locator check button
  this->Script("pack %s -side top -anchor nw -padx 2 -pady 2", this->ShowLocatorCheckButton->GetWidgetName());

  // create scale to set the locator projection length
  this->ProjectionLengthScale = vtkKWScaleWithEntry::New();
  this->ProjectionLengthScale->SetParent(locatorOptionsFrame->GetFrame());
  this->ProjectionLengthScale->Create();
  this->ProjectionLengthScale->SetLabelText("Projection length (cm):\t\t");
  this->ProjectionLengthScale->SetBalloonHelpString("Set the projection length of the locator tip (unit: cm, resolution: 0.5 cm). Set to zero to turn projection off.");
  this->ProjectionLengthScale->GetLabel()->SetPadX(16); // indentation: align scale text label with check button text labels
  this->ProjectionLengthScale->GetEntry()->SetWidth(8);
  this->ProjectionLengthScale->GetEntry()->SetReadOnly(1);
  this->ProjectionLengthScale->SetRange(0.0, 50.0);
  this->ProjectionLengthScale->SetResolution(0.5);
  this->ProjectionLengthScale->SetValue(0.0);

  // add projection length scale
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", this->ProjectionLengthScale->GetWidgetName());

  // create check button to freeze/unfreeze the locator
  this->FreezeLocatorCheckButton = vtkKWCheckButton::New();
  this->FreezeLocatorCheckButton->SetParent(locatorOptionsFrame->GetFrame());
  this->FreezeLocatorCheckButton->Create();
  this->FreezeLocatorCheckButton->SetText("Freeze locator");
  this->FreezeLocatorCheckButton->SetBalloonHelpString("Freeze/unfreeze the locator.");
  this->FreezeLocatorCheckButton->SetSelectedState(false);

  // create check button to show/hide the crosshair in the slice views (corresponding to the locator's tip position)
  this->ShowCrosshairCheckButton = vtkKWCheckButton::New();
  this->ShowCrosshairCheckButton->SetParent(locatorOptionsFrame->GetFrame());
  this->ShowCrosshairCheckButton->Create();
  this->ShowCrosshairCheckButton->SetText("Show crosshair");
  this->ShowCrosshairCheckButton->SetBalloonHelpString("Show/hide the crosshair.");
  this->ShowCrosshairCheckButton->SetSelectedState(false);

  // create check button to draw/hide the needle projection in the slice views
  this->DrawNeedleProjectionCheckButton = vtkKWCheckButton::New();
  this->DrawNeedleProjectionCheckButton->SetParent(locatorOptionsFrame->GetFrame());
  this->DrawNeedleProjectionCheckButton->Create();
  this->DrawNeedleProjectionCheckButton->SetText("Draw needle projection");
  this->DrawNeedleProjectionCheckButton->SetBalloonHelpString("Draw/hide the needle projection.");
  this->DrawNeedleProjectionCheckButton->SetSelectedState(false);

  // add freeze locator and show crosshair check buttons
  this->Script("pack %s %s %s -side top -anchor nw -padx 2 -pady 2",
                this->FreezeLocatorCheckButton->GetWidgetName(),
                this->ShowCrosshairCheckButton->GetWidgetName(),
                this->DrawNeedleProjectionCheckButton->GetWidgetName());

  // create labelled frame to hold widgets for setting the slice driver options
  vtkKWFrameWithLabel* sliceDriverFrame = vtkKWFrameWithLabel::New();
  sliceDriverFrame->SetParent(navigationFrame->GetFrame());
  sliceDriverFrame->Create();
  sliceDriverFrame->SetLabelText("Slice driver options");
  this->Script("pack %s -side top -anchor nw -fill x -padx 2 -pady 2", sliceDriverFrame->GetWidgetName());

  //----------------------------------------------------------------
  // Create menu buttons to set the slice driver for each slice orientation separately.
  //----------------------------------------------------------------
  // create frame required to center the menu buttons
  vtkKWFrame* sliceOrientationFrame = vtkKWFrame::New();
  sliceOrientationFrame->SetParent(sliceDriverFrame->GetFrame());
  sliceOrientationFrame->Create();
  this->Script("pack %s -side top -anchor c -padx 2 -pady 2", sliceOrientationFrame->GetWidgetName());

  // get slice orientation colors red, green and yellow
  vtkSlicerColor* color = ((vtkSlicerApplication*)this->GetApplication())->GetSlicerTheme()->GetSlicerColors();
  // create menu button to set the driver for the red (axial) slice orientation
  this->RedSliceMenuButton = vtkKWMenuButton::New();
  this->RedSliceMenuButton->SetParent(sliceOrientationFrame);
  this->RedSliceMenuButton->Create();
  this->RedSliceMenuButton->SetBalloonHelpString("Set driver for the axial slice orientation.");
  this->RedSliceMenuButton->SetWidth(13);
  this->RedSliceMenuButton->SetBackgroundColor(color->SliceGUIRed);
  this->RedSliceMenuButton->SetActiveBackgroundColor(color->SliceGUIRed);
  this->RedSliceMenuButton->GetMenu()->AddRadioButton("User");
  this->RedSliceMenuButton->GetMenu()->AddRadioButton("Locator");
  this->RedSliceMenuButton->SetValue("User");
  // create menu button to set the driver for the yellow (sagittal) slice orientation
  this->YellowSliceMenuButton = vtkKWMenuButton::New();
  this->YellowSliceMenuButton->SetParent(sliceOrientationFrame);
  this->YellowSliceMenuButton->Create();
  this->YellowSliceMenuButton->SetBalloonHelpString("Set driver for the sagittal slice orientation.");
  this->YellowSliceMenuButton->SetWidth(13);
  this->YellowSliceMenuButton->SetBackgroundColor(color->SliceGUIYellow);
  this->YellowSliceMenuButton->SetActiveBackgroundColor(color->SliceGUIYellow);
  this->YellowSliceMenuButton->GetMenu()->AddRadioButton("User");
  this->YellowSliceMenuButton->GetMenu()->AddRadioButton("Locator");
  this->YellowSliceMenuButton->SetValue("User");
  // create menu button to set the driver for the green (coronal) slice orientation
  this->GreenSliceMenuButton = vtkKWMenuButton::New();
  this->GreenSliceMenuButton->SetParent(sliceOrientationFrame);
  this->GreenSliceMenuButton->Create();
  this->GreenSliceMenuButton->SetBalloonHelpString("Set driver for the coronal slice orientation.");
  this->GreenSliceMenuButton->SetWidth(13);
  this->GreenSliceMenuButton->SetBackgroundColor(color->SliceGUIGreen);
  this->GreenSliceMenuButton->SetActiveBackgroundColor(color->SliceGUIGreen);
  this->GreenSliceMenuButton->GetMenu()->AddRadioButton("User");
  this->GreenSliceMenuButton->GetMenu()->AddRadioButton("Locator");
  this->GreenSliceMenuButton->SetValue("User");

  // add red, green and yellow slice orientation slice driver menu buttons
  this->Script("pack %s %s %s -side left -anchor nw -fill x -padx 2 -pady 2",
                this->RedSliceMenuButton->GetWidgetName(),
                this->YellowSliceMenuButton->GetWidgetName(),
                this->GreenSliceMenuButton->GetWidgetName());

  //----------------------------------------------------------------
  // Create widgets to set the slice driver for all slice orientations at once and to set the reslicing options.
  //----------------------------------------------------------------
  // create frame required to center the widgets
  vtkKWFrame* sliceOptionsFrame = vtkKWFrame::New();
  sliceOptionsFrame->SetParent(sliceDriverFrame->GetFrame());
  sliceOptionsFrame->Create();
  this->Script("pack %s -side top -anchor c -padx 2 -pady 2", sliceOptionsFrame->GetWidgetName());

  // create button to drive all slice orientations by the locator
  this->SetLocatorAllPushButton = vtkKWPushButton::New();
  this->SetLocatorAllPushButton->SetParent(sliceOptionsFrame);
  this->SetLocatorAllPushButton->Create();
  this->SetLocatorAllPushButton->SetText("Locator all");
  this->SetLocatorAllPushButton->SetBalloonHelpString("Drive all slice orientations by the locator.");
  this->SetLocatorAllPushButton->SetWidth(12);
  // create button to drive all slice orientations by the user
  this->SetUserAllPushButton = vtkKWPushButton::New();
  this->SetUserAllPushButton->SetParent(sliceOptionsFrame);
  this->SetUserAllPushButton->Create();
  this->SetUserAllPushButton->SetText("User all");
  this->SetUserAllPushButton->SetBalloonHelpString("Drive all slice orientations by the user.");
  this->SetUserAllPushButton->SetWidth(12);
  // create check button to freeze/unfreeze reslicing
  this->FreezeReslicingCheckButton = vtkKWCheckButton::New();
  this->FreezeReslicingCheckButton->SetParent(sliceOptionsFrame);
  this->FreezeReslicingCheckButton->Create();
  this->FreezeReslicingCheckButton->SetText("Freeze");
  this->FreezeReslicingCheckButton->SetBalloonHelpString("Freeze/unfreeze reslicing.");
  this->FreezeReslicingCheckButton->SetSelectedState(false);
  // create check button to enable/disable oblique reslicing
  this->ObliqueReslicingCheckButton = vtkKWCheckButton::New();
  this->ObliqueReslicingCheckButton->SetParent(sliceOptionsFrame);
  this->ObliqueReslicingCheckButton->Create();
  this->ObliqueReslicingCheckButton->SetText("Oblique");
  this->ObliqueReslicingCheckButton->SetBalloonHelpString("Enable/disable oblique reslicing.");
  this->ObliqueReslicingCheckButton->SetSelectedState(false);

  // add drive all slice orientations by locator/user buttons, freeze reslicing and oblique reslicing check buttons
  this->Script("pack %s %s %s %s -side left -anchor nw -fill x -padx 2 -pady 2",
                this->SetLocatorAllPushButton->GetWidgetName(),
                this->SetUserAllPushButton->GetWidgetName(),
                this->FreezeReslicingCheckButton->GetWidgetName(),
                this->ObliqueReslicingCheckButton->GetWidgetName());

  // clean up
  navigationFrame->Delete();
  evaluationFrame->Delete();
  locatorOptionsFrame->Delete();
  sliceDriverFrame->Delete();
  sliceOrientationFrame->Delete();
  sliceOptionsFrame->Delete();
}
