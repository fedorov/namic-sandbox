<?xml version="1.0" encoding="utf-8"?>
<executable>
  <category>Registration</category>
  <title>Slice to Volume registration</title>
  <description>Try to align an axial slice with the 3d volume.</description>
  <version>0.1.0.$Revision: 2104 $(alpha)</version>
  <documentation-url></documentation-url>
  <license></license>
  <contributor>Andras Lasso (Queen's), Andrey Fedorov (BWH)</contributor>
<acknowledgements>
This work is part of the National Alliance for Medical Image Computing (NAMIC), funded by the National Institutes of Health through the NIH Roadmap for Medical Research, Grant U54 EB005149.
</acknowledgements>

  <parameters>
    <label>IO</label>
    <description>Input/output parameters</description>

     <image>
      <name>SliceImageFilename</name>
      <longflag>sliceImage</longflag>
      <label>Slice image (fixed)</label>
      <channel>input</channel>      
      <description>The slice image</description>
    </image>

    <image>
      <name>VolumeImageFilename</name>
      <longflag>volumeImage</longflag>
      <label>Volume image (moving)</label>
      <channel>input</channel>      
      <description>The volume image</description>
    </image>

    <image>
      <name>VolumeMaskImageFilename</name>
      <longflag>volumeImageMask</longflag>
      <label>Fixed Image mask</label>
      <channel>input</channel>      
      <description>The fixed image mask</description>
    </image>

    <image>
      <name>ResampledRigidMovingImageFilename</name>
      <longflag>resampledRigidMovingImage</longflag>
      <label>Output rigid resampled volume</label>
      <channel>output</channel>
      <description>Resampled moving image to fixed image coordinate frame. Optional (specify an output transform or an output volume or both).</description>
    </image>

    <transform fileExtensions=".txt" reference="VolumeImageFilename">
      <name>RigidTransformFilename</name>
      <longflag>rigidTransform</longflag>
      <label>Output rigid transform</label>
      <channel>output</channel>      
      <description>The resulting transform</description>
    </transform>

    <image>
      <name>RigidFieldImageFilename</name>
      <longflag>rigidFieldImage</longflag>
      <label>Output rigid deformation field</label>
      <channel>output</channel>
      <description>Output of the deformation field after the rigid registration step. Optional.</description>
    </image>

  </parameters>

  <parameters>
    <label>Deformable registration</label>
    <description>Parameters used for deformable registration</description>

    <boolean>
      <name>EnableDeformable</name>
      <longflag>deformable</longflag>
      <label>Enable deformable registration</label>
      <description>Perform a deformable registration after the rigid registration is completed.</description>
      <default>false</default>
    </boolean>

    <double>
      <name>GridSpacing</name>
      <longflag>gridSpacing</longflag>
      <label>Grid spacing (mm)</label>
      <description>Distance between grid points of the deformable mesh.</description>
      <default>30</default>
      <constraints>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <step>0.1</step>
      </constraints>
    </double>

    <double>
      <name>MaxDeformation</name>
      <longflag>maxDeformation</longflag>
      <flag>d</flag>
      <description>Maximum allowed deformation in mm.</description>
      <label>Maximum deformation</label>
      <default>5.0</default>
      <constraints>
        <minimum>0.0</minimum>
        <maximum>20.0</maximum>
        <step>0.1</step>
      </constraints>
    </double>

    <image>
      <name>ResampledDefMovingImageFilename</name>
      <longflag>resampledDefMovingImage</longflag>
      <label>Output deformable resampled volume</label>
      <channel>output</channel>
      <description>Resampled moving image to fixed image coordinate frame. Optional (specify an output transform or an output volume or both).</description>
    </image>

    <transform fileExtensions=".txt" type="bspline">
      <name>DefTransformFilename</name>
      <longflag>defTransform</longflag>
      <label>Output deformable transform</label>
      <channel>output</channel>      
      <description>Transform calculated that aligns the fixed and moving image. Maps positions from the fixed coordinate frame to the moving coordinate frame. Optional (specify an output transform or an output volume or both).</description>
    </transform>
    
    <image>
      <name>DefFieldImageFilename</name>
      <longflag>defFieldImage</longflag>
      <label>Output deformable deformation field</label>
      <channel>output</channel>
      <description>Deformation field. Optional.</description>
    </image>

  </parameters>

  <parameters>
    <label>Metric testing</label>
    <description>Parameters used for metric testing</description>
    
    <boolean>
      <name>EnableMetricTesting</name>
      <longflag>metricTesting</longflag>
      <label>Enable metric testing</label>
      <description>Enable testing the metric by testing the effect of changing each parameter of the rigid transform and writing the metric result in a file.</description>
      <default>false</default>
    </boolean>

    <double-vector>
      <name>minimumStepLength</name>
      <longflag deprecatedalias="minimumStepSize">minimumStepLength</longflag>
      <label>Minimum Step Length</label>
      <description>Each step in the optimization takes steps at least this big.  When none are possible, registration is complete.</description>
      <default>0.005</default>
    </double-vector>

    <double-vector>
      <name>MetricTestTranslationRange</name>
      <longflag>metricTestTranslationRange</longflag>
      <label>Translation range (mm)</label>
      <description>The metric will be tested in the range x1, x2, y1, y2, z1, z2.</description>
      <default>-20,20,-20,20,-20,20</default>
    </double-vector>

    <integer-vector>
      <name>MetricTestTranslationResolution</name>
      <longflag>metricTestTranslationResolution</longflag>
      <description>Number of test samples taken along the translation range.</description>
      <label>Translation range resolution</label>
      <default>10,10,10</default>
    </integer-vector>

    <string>
      <name>MetricTestOutputTranslationFilename</name>
      <longflag>metricTestOutputTranslation</longflag>
      <label>Output translation image filename</label>
      <description>Name of the metric test output file for translation</description>
      <default>MetricTestOutputTranslation.mha</default>
    </string>

    <double-vector>
      <name>MetricTestRotationRange</name>
      <longflag>metricTestRotationRange</longflag>
      <label>Rotation range (deg)</label>
      <description>The metric will be tested in the range rx1, rx2, ry1, ry2, rz1, rz2.</description>
      <default>-10,10,-10,10,-10,10</default>
    </double-vector>

    <integer-vector>
      <name>MetricTestRotationResolution</name>
      <longflag>metricTestRotationResolution</longflag>
      <description>Number of test samples taken along the translation range.</description>
      <label>Rotation range resolution</label>
      <default>10,10,10</default>
    </integer-vector>
  
    <string>
      <name>MetricTestOutputRotationFilename</name>
      <longflag>metricTestOutputRotation</longflag>
      <label>Output rotation image filename</label>
      <description>Name of the metric test output file for rotation</description>
      <default>MetricTestOutputRotation.mha</default>
    </string>

  </parameters>


</executable>
