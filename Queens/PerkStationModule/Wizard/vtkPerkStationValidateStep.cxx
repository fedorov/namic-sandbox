
#include "vtkPerkStationValidateStep.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "PerkStationCommon.h"
#include "vtkPerkStationModuleGUI.h"
#include "vtkMRMLPerkStationModuleNode.h"
#include "vtkPerkStationSecondaryMonitor.h"

#include "vtkKWFrame.h"
#include "vtkKWLabel.h"
#include "vtkKWEntry.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWEntrySet.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"
#include "vtkKWMultiColumnList.h"
#include "vtkKWMultiColumnListWithScrollbars.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWWizardWidget.h"
#include "vtkKWWizardWorkflow.h"

#include "vtkLineSource.h"
#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkSmartPointer.h"



vtkStandardNewMacro( vtkPerkStationValidateStep );
vtkCxxRevisionMacro( vtkPerkStationValidateStep, "$Revision: 1.0 $" );



void
vtkPerkStationValidateStep
::AddGUIObservers()
{
  this->RemoveGUIObservers();
  
  
  if ( this->PlanList )
    {
    this->PlanList->GetWidget()->SetSelectionChangedCommand(
      this, "OnMultiColumnListSelectionChanged" );
    }
  
  
  if (this->ResetValidationButton)
    {
    this->ResetValidationButton->AddObserver( vtkKWPushButton::InvokedEvent,
                                              (vtkCommand *)this->WizardGUICallbackCommand );
    }
  
  if ( this->ValidationExportButton )
    {
    this->ValidationExportButton->GetLoadSaveDialog()->AddObserver(
      vtkKWLoadSaveDialog::WithdrawEvent, (vtkCommand*)this->WizardGUICallbackCommand );
    }
}



void
vtkPerkStationValidateStep
::InstallCallbacks()
{
    // Configure the OK button to start
  vtkKWWizardWidget *wizard_widget = this->GetGUI()->GetWizardWidget();
  
  this->AddGUIObservers();
}



vtkPerkStationValidateStep
::vtkPerkStationValidateStep()
{
  this->SetName("4/5. Validate");
  this->SetDescription("Mark actual entry point and target hit");  
  this->WizardGUICallbackCommand->SetCallback(vtkPerkStationValidateStep::WizardGUICallback);

  
  this->PlanListFrame = NULL;
  this->PlanListLabel = NULL;
  this->PlanList = NULL;
  
  this->ResetFrame = NULL;
  this->ResetValidationButton = NULL;
  
  this->ValidationListFrame = NULL;
  this->ValidationListLabel = NULL;
  this->ValidationList = NULL;
  this->ValidationExportButton = NULL;
  
  
  this->ValidationNeedleActor = vtkActor::New();
  this->PlanActor = vtkActor::New();
  
  
  this->ClickNumber = 0;
  this->ProcessingCallback = false;
}



vtkPerkStationValidateStep
::~vtkPerkStationValidateStep()
{
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->PlanListFrame );
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->PlanListLabel );
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->PlanList );
  
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->ResetFrame );
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->ResetValidationButton );
  
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->ValidationListFrame );
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->ValidationListLabel );
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->ValidationList );
  DELETE_IF_NULL_WITH_SETPARENT_NULL( this->ValidationExportButton );
  
  DELETE_IF_NOT_NULL( this->ValidationNeedleActor );
  DELETE_IF_NOT_NULL( this->PlanActor );
}



void
vtkPerkStationValidateStep
::ShowUserInterface()
{
  this->Superclass::ShowUserInterface();

  vtkKWWizardWidget *wizard_widget = this->GetGUI()->GetWizardWidget();
  wizard_widget->GetCancelButton()->SetEnabled( 0 );
  vtkKWWidget *parent = wizard_widget->GetClientArea();
  int enabled = parent->GetEnabled();
  
  
  this->ShowPlanListFrame();
  
  
    // clear controls
  
  FORGET( this->ResetFrame )
  FORGET( this->ResetValidationButton )
  
  
  this->SetName("4/4. Validate");
  this->GetGUI()->GetWizardWidget()->Update();

    // additional reset button
  
  
    // frame for reset button
  
  if ( ! this->ResetFrame )
    {
    this->ResetFrame = vtkKWFrame::New();
    }
  if ( ! this->ResetFrame->IsCreated() )
    {
    this->ResetFrame->SetParent( parent );
    this->ResetFrame->Create();     
    }
  this->Script( "pack %s -side top -anchor nw -fill x -padx 0 -pady 2", this->ResetFrame->GetWidgetName() );
  
  
  if ( ! this->ResetValidationButton )
    {
    this->ResetValidationButton = vtkKWPushButton::New();
    }
  if( ! this->ResetValidationButton->IsCreated() )
    {
    this->ResetValidationButton->SetParent( this->ResetFrame );
    this->ResetValidationButton->SetText( "Reset validation" );
    this->ResetValidationButton->SetBorderWidth( 2 );
    this->ResetValidationButton->SetReliefToRaised();      
    this->ResetValidationButton->SetHighlightThickness( 2 );
    this->ResetValidationButton->SetImageToPredefinedIcon( vtkKWIcon::IconTrashcan );
    this->ResetValidationButton->Create();
    }
  this->Script( "pack %s -side top -padx 2 -pady 4", this->ResetValidationButton->GetWidgetName() );
     
  
  this->ShowValidationListFrame();
  
  
  this->SetDescription( "Mark actual entry point and target hit" );  

  this->InstallCallbacks();
  
  this->UpdateGUI();
}



void
vtkPerkStationValidateStep
::ShowPlanListFrame()
{
  vtkKWWizardWidget* wizard_widget = this->GetGUI()->GetWizardWidget();
  vtkKWWidget* parent = wizard_widget->GetClientArea();
  
  
    // Create a frame for the plan list.
  
  if ( ! this->PlanListFrame )
    {
    this->PlanListFrame = vtkKWFrame::New();
    this->PlanListLabel = vtkKWLabel::New();
    }
  if ( ! this->PlanListFrame->IsCreated() )
    {
    this->PlanListFrame->SetParent( parent );
    this->PlanListFrame->Create();
    
    this->PlanListLabel->SetParent( this->PlanListFrame );
    this->PlanListLabel->Create();
    this->PlanListLabel->SetText( "Select plan to be validated. Then click on real \n"
                                  "needle entry point and real needle tip on the image." );
    }
  this->Script( "pack %s -side top -anchor nw -expand n -fill x -padx 2 -pady 2",
                this->PlanListFrame->GetWidgetName() );
  this->Script( "pack %s -side top -anchor nw -expand n -fill x -padx 2 -pady 2",
                this->PlanListLabel->GetWidgetName() );
  
  
    // Create the plan list.
  
  if ( ! this->PlanList )
    {
    this->PlanList = vtkKWMultiColumnListWithScrollbars::New();
    this->PlanList->SetParent( this->PlanListFrame );
    this->PlanList->Create();
    this->PlanList->SetHeight( 1 );
    this->PlanList->GetWidget()->SetSelectionTypeToRow();
    this->PlanList->GetWidget()->SetSelectionBackgroundColor( 1, 0, 0 );
    this->PlanList->GetWidget()->MovableRowsOff();
    this->PlanList->GetWidget()->MovableColumnsOff();
    
      // Create the columns.
    
    for ( int col = 0; col < PLAN_COL_COUNT; ++ col )
      {
      this->PlanList->GetWidget()->AddColumn( PLAN_COL_LABELS[ col ] );
      this->PlanList->GetWidget()->SetColumnWidth( col, PLAN_COL_WIDTHS[ col ] );
      this->PlanList->GetWidget()->SetColumnAlignmentToLeft( col );
      }
    }
  
  this->Script( "pack %s -side top -anchor nw -expand n -fill x -padx 2 -pady 2",
                this->PlanList->GetWidgetName() );
}



void
vtkPerkStationValidateStep
::ShowValidationListFrame()
{
  vtkKWWizardWidget* wizard_widget = this->GetGUI()->GetWizardWidget();
  vtkKWWidget* parent = wizard_widget->GetClientArea();
  
  if ( ! this->ValidationListFrame )
    {
    this->ValidationListFrame = vtkKWFrame::New();
    this->ValidationListLabel = vtkKWLabel::New();
    }
  if ( ! this->ValidationListFrame->IsCreated() )
    {
    this->ValidationListFrame->SetParent( parent );
    this->ValidationListFrame->Create();
    
    this->ValidationListLabel->SetParent( this->ValidationListFrame );
    this->ValidationListLabel->Create();
    this->ValidationListLabel->SetText( "Validation results" );
    }
  this->Script( "pack %s -side top -anchor nw -expand n -fill x -padx 2 -pady 2",
                this->ValidationListFrame->GetWidgetName() );
  this->Script( "pack %s -side top -anchor nw -expand n -fill x -padx 2 -pady 2",
                this->ValidationListLabel->GetWidgetName() );
  
    // Create the list.
  
  if ( ! this->ValidationList )
    {
    this->ValidationList = vtkKWMultiColumnListWithScrollbars::New();
    this->ValidationList->SetParent( this->ValidationListFrame );
    this->ValidationList->Create();
    this->ValidationList->SetHeight( 1 );
    this->ValidationList->GetWidget()->SetSelectionTypeToRow();
    this->ValidationList->GetWidget()->SetSelectionBackgroundColor( 1, 1, 1 );
    
      // Create the columns.
    
    for ( int col = 0; col < VALIDATION_COL_COUNT; ++ col )
      {
      this->ValidationList->GetWidget()->AddColumn( VALIDATION_COL_LABELS[ col ] );
      this->ValidationList->GetWidget()->SetColumnWidth( col, VALIDATION_COL_WIDTHS[ col ] );
      this->ValidationList->GetWidget()->SetColumnAlignmentToLeft( col );
      }
    }
  
  this->Script( "pack %s -side top -anchor nw -expand n -fill x -padx 2 -pady 2",
                this->ValidationList->GetWidgetName() );
  
  if ( ! this->ValidationExportButton )
    {
    this->ValidationExportButton = vtkKWLoadSaveButton::New();
    this->ValidationExportButton->SetParent( this->ValidationListFrame );
    this->ValidationExportButton->Create();
    this->ValidationExportButton->GetLoadSaveDialog()->SetSaveDialog( 1 );
    this->ValidationExportButton->SetInitialFileName( "results.csv" );
    this->ValidationExportButton->SetText( "Export validation results" );
    }
  
  this->Script( "pack %s -side top -anchor nw -expand n -fill x -padx 2 -pady 2",
                this->ValidationExportButton->GetWidgetName() );
}



void
vtkPerkStationValidateStep
::PrintSelf( ostream& os, vtkIndent indent )
{
  this->Superclass::PrintSelf( os,indent );
}



void
vtkPerkStationValidateStep
::RemoveGUIObservers()
{
  if ( this->PlanList )
    {
    this->PlanList->GetWidget()->SetSelectionChangedCommand( this, "" );
    }
  
  if (this->ResetValidationButton)
    {
    this->ResetValidationButton->RemoveObservers( vtkKWPushButton::InvokedEvent,
                                                  (vtkCommand *)this->WizardGUICallbackCommand );
    }
  
  if ( this->ValidationExportButton )
    {
    this->ValidationExportButton->GetLoadSaveDialog()->RemoveObservers(
      vtkKWLoadSaveDialog::WithdrawEvent, (vtkCommand*)this->WizardGUICallbackCommand );
    }
}



void vtkPerkStationValidateStep::ProcessImageClickEvents(
   vtkObject *caller, unsigned long event, void *callData )
{
  vtkKWWizardWidget *wizard_widget = this->GetGUI()->GetWizardWidget();
  
  if (    ! wizard_widget
       || wizard_widget->GetWizardWorkflow()->GetCurrentStep() !=  this
       || ! this->GetGUI()->GetPerkStationModuleNode()
       || ! this->GetGUI()->GetPerkStationModuleNode()->GetValidationVolumeNode()
       || strcmp( this->GetGUI()->GetPerkStationModuleNode()->GetVolumeInUse(),
                  "Validation" ) != 0 )
    {
    return;
    }
  vtkMRMLPerkStationModuleNode* moduleNode = this->GetGUI()->GetPerkStationModuleNode();
  
    
  if ( this->ClickNumber > 1 ) return;  // Don't do anything after two clicks.
  
  
  vtkSlicerInteractorStyle *s = vtkSlicerInteractorStyle::SafeDownCast( caller );
  
  vtkSlicerInteractorStyle *istyle0 = vtkSlicerInteractorStyle::SafeDownCast(
    this->GetGUI()->GetApplicationGUI()->GetMainSliceGUI( "Red" )->
    GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor()->
    GetInteractorStyle() );
  
  vtkRenderWindowInteractor *rwi;
  vtkRenderer *renderer = this->GetGUI()->GetApplicationGUI()->GetMainSliceGUI( "Red" )->GetSliceViewer()->
                                GetRenderWidget()->GetOverlayRenderer();

  if (    ( s == istyle0 )
       && ( event == vtkCommand::LeftButtonPressEvent ) )
    {
    ++ this->ClickNumber;
    
      // mouse click happened in the axial slice view      
    vtkSlicerSliceGUI *sliceGUI = vtkSlicerApplicationGUI::SafeDownCast(
         this->GetGUI()->GetApplicationGUI() )->GetMainSliceGUI( "Red" );
    rwi = sliceGUI->GetSliceViewer()->GetRenderWidget()->GetRenderWindowInteractor();    
    
    vtkMatrix4x4 *xyToRas;
      xyToRas = sliceGUI->GetLogic()->GetSliceNode()->GetXYToRAS();
    
    // capture the point
    int point[ 2 ];
    rwi->GetLastEventPosition( point );
    double inPt[ 4 ] = { point[ 0 ], point[ 1 ], 0, 1 };
    double outPt[ 4 ];
    
    xyToRas->MultiplyPoint( inPt, outPt ); 
    double ras[ 3 ] = { outPt[ 0 ], outPt[ 1 ], outPt[ 2 ] };
    
    
      // depending on click number, it is either Entry point or target point
    
    if ( this->ClickNumber == 1 )
      {
      moduleNode->SetValidationEntryPoint( ras );
      }
    else if ( this->ClickNumber == 2 )
      {
      moduleNode->SetValidationTargetPoint( ras );      
      moduleNode->SetValidated( true );
      
      this->OverlayValidationNeedleAxis();
      }
    
    this->UpdateGUI();
    }
}



void
vtkPerkStationValidateStep
::OverlayValidationNeedleAxis()
{
  vtkSlicerSliceGUI* sliceGUI = vtkSlicerApplicationGUI::SafeDownCast( 
                                  this->GetGUI()->GetApplicationGUI() )->GetMainSliceGUI( "Red" );
  
  vtkRenderer* renderer = sliceGUI->GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer();
  
    // get the world coordinates
  
  double rasEntry[ 3 ];
  this->GetGUI()->GetPerkStationModuleNode()->GetValidationEntryPoint( rasEntry );
  
  double rasTarget[ 3 ];
  this->GetGUI()->GetPerkStationModuleNode()->GetValidationTargetPoint( rasTarget );
  
  
  double wcEntry[ 3 ];
  this->RasToWorld( rasEntry, wcEntry );
  
  double wcTarget[ 3 ];
  this->RasToWorld( rasTarget, wcTarget );
  
  
    // set up the line
  vtkSmartPointer< vtkLineSource > line = vtkSmartPointer< vtkLineSource >::New();
    line->SetPoint1( wcEntry );
    line->SetPoint2( wcTarget );
  
    // set up the mapper
  vtkSmartPointer< vtkPolyDataMapper > lineMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    lineMapper->SetInputConnection( line->GetOutputPort() );
  
  
  this->ValidationNeedleActor->SetMapper( lineMapper );
  this->ValidationNeedleActor->GetProperty()->SetColor( 255, 0, 255 );


    // add to actor collection
  sliceGUI->GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer()->AddActor( this->ValidationNeedleActor );
  
  sliceGUI->GetSliceViewer()->RequestRender(); 
}



void
vtkPerkStationValidateStep
::OverlayPlan( bool visible )
{
  if ( ! visible )
    {
    this->PlanActor->SetVisibility( false );
    return;
    }
  
  
  vtkSlicerSliceGUI* sliceGUI = vtkSlicerApplicationGUI::SafeDownCast( 
                                  this->GetGUI()->GetApplicationGUI() )->GetMainSliceGUI( "Red" );
  
  vtkRenderer* renderer = sliceGUI->GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer();
  
  
    // Remove actor if already added.
  
  vtkActorCollection *collection = this->GetGUI()->GetApplicationGUI()->
    GetMainSliceGUI( "Red" )->GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer()->GetActors();
  
  if ( collection->IsItemPresent( this->PlanActor ) )
    {
    this->GetGUI()->GetApplicationGUI()->GetMainSliceGUI( "Red" )->
          GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer()->RemoveActor( this->PlanActor );
    }
  
  
    // Compute world coordinates.
  
  double rasEntry[ 3 ];
  this->GetGUI()->GetPerkStationModuleNode()->GetPlanEntryPoint( rasEntry );
  
  double rasTarget[ 3 ];
  this->GetGUI()->GetPerkStationModuleNode()->GetPlanTargetPoint( rasTarget );
  
  double wcEntry[ 3 ];
  this->RasToWorld( rasEntry, wcEntry );
  
  double wcTarget[ 3 ];
  this->RasToWorld( rasTarget, wcTarget );
  
  
    // set up the line
  vtkSmartPointer< vtkLineSource > line = vtkSmartPointer< vtkLineSource >::New();
    line->SetPoint1( wcEntry );
    line->SetPoint2( wcTarget );
    line->SetResolution( 100 );
  
    // set up the mapper
  vtkSmartPointer< vtkPolyDataMapper > lineMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    lineMapper->SetInputConnection( line->GetOutputPort() );
  
  
  this->PlanActor->SetMapper( lineMapper );
  this->PlanActor->GetProperty()->SetLineStipplePattern( 0xffff );
  this->PlanActor->GetProperty()->SetColor( 255, 50, 0 );
  this->PlanActor->SetVisibility( true );
  
  
    // add to actor collection and render
  sliceGUI->GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer()->AddActor( this->PlanActor );
  sliceGUI->GetSliceViewer()->RequestRender();
}



void
vtkPerkStationValidateStep
::RemoveValidationNeedleAxis()
{
    // should remove the overlay needle guide
  
  vtkActorCollection *collection = this->GetGUI()->GetApplicationGUI()->
    GetMainSliceGUI( "Red" )->GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer()->GetActors();
  
  
  if ( collection->IsItemPresent( this->ValidationNeedleActor ) )
    {
    this->GetGUI()->GetApplicationGUI()->GetMainSliceGUI( "Red" )->
          GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer()->RemoveActor( this->ValidationNeedleActor );
    this->GetGUI()->GetApplicationGUI()->GetMainSliceGUI( "Red" )->GetSliceViewer()->RequestRender();
    }
}



void
vtkPerkStationValidateStep
::OnMultiColumnListSelectionChanged()
{
  int numRows = this->PlanList->GetWidget()->GetNumberOfSelectedRows();
  if ( numRows != 1 ) return;
  
  vtkMRMLPerkStationModuleNode* moduleNode = this->GetGUI()->GetPerkStationModuleNode();
  
  int rowIndex = this->PlanList->GetWidget()->GetIndexOfFirstSelectedRow();
  moduleNode->SetCurrentPlanIndex( rowIndex );
  
  
  vtkPerkStationPlan* plan = moduleNode->GetPlanAtIndex( rowIndex );
  double point[ 3 ] = { 0, 0, 0 };
  plan->GetTargetPointRAS( point );
  moduleNode->SetCurrentSliceOffset( point[ 2 ] );
  this->GetGUI()->GetApplicationGUI()->GetMainSliceGUI( "Red" )->GetLogic()->SetSliceOffset(
    moduleNode->GetCurrentSliceOffset() );
  
  moduleNode->GetPlanFiducialsNode()->SetAllFiducialsVisibility( 0 );
  this->ClickNumber = 0;
  
  this->UpdateGUI();
}



void
vtkPerkStationValidateStep
::OnSliceOffsetChanged( double offset )
{
  vtkMRMLPerkStationModuleNode* node = this->GetGUI()->GetPerkStationModuleNode();
  if ( ! node  ||  node->GetCurrentPlanIndex() < 0 ) return;
  
  
  vtkPerkStationPlan* plan = node->GetPlanAtIndex( node->GetCurrentPlanIndex() );
  double* target = plan->GetTargetPointRAS();
  
  if ( target[ 2 ] >= ( offset - 0.5 )  &&  target[ 2 ] < ( offset + 0.5 ) )
    {
    this->PlanActor->SetVisibility( 1 );
    }
  else
    {
    this->PlanActor->SetVisibility( 0 );
    }
}



void
vtkPerkStationValidateStep
::Reset()
{
    // reset local member variables to defaults
  this->ClickNumber = 0;
  this->ProcessingCallback = false;
    // TODO: Would be better to put the fiducials in the corner of the image
    //       where no mouse clicks are ever expected.
  
  vtkMRMLPerkStationModuleNode* moduleNode = this->GetGUI()->GetPerkStationModuleNode();
  vtkMRMLFiducialListNode* planNode = moduleNode->GetPlanFiducialsNode();
  if ( planNode != NULL )
    {
    planNode->SetAllFiducialsVisibility( 0 );
    planNode->SetNthFiducialXYZ( 0, 0, 0, 0 );
    planNode->SetNthFiducialXYZ( 1, 0, 0, 0 );
    }
  
  this->GetGUI()->GetPerkStationModuleNode()->SetValidated( false );
}



void
vtkPerkStationValidateStep
::UpdateGUI()
{
  vtkMRMLPerkStationModuleNode* mrmlNode = this->GetGUI()->GetPerkStationModuleNode();
  if ( ! mrmlNode ) return;
  
  
    // Update plan list. ------------------------------------------------------
  
  const int PRECISION_DIGITS = 1;
  
  if ( this->PlanList == NULL || this->PlanList->GetWidget() == NULL ) return;
  
  int numPlans = mrmlNode->GetNumberOfPlans();
  
  bool deleteFlag = true;
  if ( numPlans != this->PlanList->GetWidget()->GetNumberOfRows() )
    {
    this->PlanList->GetWidget()->DeleteAllRows();
    }
  else
    {
    deleteFlag = false;
    }
  
  double planEntry[ 3 ];
  double planTarget[ 3 ];
  double validationEntry[ 3 ];
  double validationTarget[ 3 ];
  
  for ( int row = 0; row < numPlans; ++ row )
    {
    vtkPerkStationPlan* plan = mrmlNode->GetPlanAtIndex( row );
    
    if ( deleteFlag )
      {
      this->PlanList->GetWidget()->AddRow();
      }
    
    plan->GetEntryPointRAS( planEntry );
    plan->GetTargetPointRAS( planTarget );
    plan->GetValidationEntryPointRAS( validationEntry );
    plan->GetValidationTargetPointRAS( validationTarget );
    
    if ( planEntry == NULL || planTarget == NULL )
      {
      vtkErrorMacro( "ERROR: No plan points in plan" );
      }
    
    vtkKWMultiColumnList* colList = this->PlanList->GetWidget();
    
    this->PlanList->GetWidget()->SetCellText( row, PLAN_COL_NAME, plan->GetName().c_str() );
    for ( int i = 0; i < 3; ++ i )
      {
      std::ostrstream os;
      os << std::setiosflags( ios::fixed | ios::showpoint ) << std::setprecision( PRECISION_DIGITS );
      os << planEntry[ i ] << std::ends;
      colList->SetCellText( row, PLAN_COL_ER + i, os.str() );
      os.rdbuf()->freeze();
      }
    for ( int i = 0; i < 3; ++ i )
      {
      std::ostrstream os;
      os << std::setiosflags( ios::fixed | ios::showpoint ) << std::setprecision( PRECISION_DIGITS );
      os << planTarget[ i ] << std::ends;
      colList->SetCellText( row, PLAN_COL_ER + 3 + i, os.str() );
      os.rdbuf()->freeze();
      }
    
    if ( plan->GetValidated() )
      {
      for ( int i = 0; i < 3; ++ i )
        {
        std::ostrstream os;
        os << std::setiosflags( ios::fixed | ios::showpoint ) << std::setprecision( PRECISION_DIGITS );
        os << validationEntry[ i ] << std::ends;
        colList->SetCellText( row, PLAN_COL_ER + 6 + i, os.str() );
        os.rdbuf()->freeze();
        }
      for ( int i = 0; i < 3; ++ i )
        {
        std::ostrstream os;
        os << std::setiosflags( ios::fixed | ios::showpoint ) << std::setprecision( PRECISION_DIGITS );
        os << validationTarget[ i ] << std::ends;
        colList->SetCellText( row, PLAN_COL_ER + 9 + i, os.str() );
        os.rdbuf()->freeze();
        }
      } // if ( plan->GetValidated() )
    } // for ( int row = 0; row < numPlans; ++ row )
  
  this->PlanList->GetWidget()->SelectRow( mrmlNode->GetCurrentPlanIndex() );
  
  
    // Update error metric values. --------------------------------------------
  
  const int VPRECISION = 2;
  
  if ( this->ValidationList == NULL || this->ValidationList->GetWidget() == NULL ) return;
  
  int numValidations = mrmlNode->GetNumberOfValidations();
  int numRows = this->ValidationList->GetWidget()->GetNumberOfRows();
  
  bool deleteValidations = true;
  if ( numValidations != numRows )
    {
    this->ValidationList->GetWidget()->DeleteAllRows();
    }
  else
    {
    deleteValidations = false;
    }
  
  
  int validationRow = 0;
  for ( int row = 0; row < numPlans; ++ row )
    {
    vtkPerkStationPlan* plan = mrmlNode->GetPlanAtIndex( row );
    
    if ( plan->GetValidated() == false ) continue;
    
    if ( deleteValidations )
      {
      this->ValidationList->GetWidget()->AddRow();
      }
    
    vtkKWMultiColumnList* colList = this->ValidationList->GetWidget();
    
    colList->SetCellText( validationRow, VALIDATION_COL_NAME, plan->GetName().c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ENTRY, DoubleToString( mrmlNode->GetEntryPointError( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ENTRY_R, DoubleToString( mrmlNode->GetEntryPointErrorR( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ENTRY_A, DoubleToString( mrmlNode->GetEntryPointErrorA( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ENTRY_S, DoubleToString( mrmlNode->GetEntryPointErrorS( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_TARGET, DoubleToString( mrmlNode->GetTargetPointError( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_TARGET_R, DoubleToString( mrmlNode->GetTargetPointErrorR( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_TARGET_A, DoubleToString( mrmlNode->GetTargetPointErrorA( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_TARGET_S, DoubleToString( mrmlNode->GetTargetPointErrorS( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ABS_ANGLE_AXIAL,
                          DoubleToString( mrmlNode->GetPlanAngleAxial( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ABS_ANGLE_SAGITTAL,
                          DoubleToString( mrmlNode->GetPlanAngleSagittal( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ANGLE, DoubleToString( mrmlNode->GetAngleError( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ANGLE_AXIAL, DoubleToString( mrmlNode->GetAngleErrorAxial( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ANGLE_SAGITTAL, DoubleToString( mrmlNode->GetAngleErrorSagittal( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_ABS_DEPTH, DoubleToString( mrmlNode->GetPlanDepth( row ), VPRECISION ).c_str() );
    colList->SetCellText( validationRow, VALIDATION_COL_DEPTH_ERROR, DoubleToString( mrmlNode->GetDepthError( row ), VPRECISION ).c_str() );
    
    validationRow ++;
    }
  
  
    // Second monitor. --------------------------------------------------------
  
  if ( mrmlNode->GetCurrentPlanIndex() >= 0 )
    {
    this->OverlayPlan( true );
    }
  else
    {
    this->OverlayPlan( false );
    }
  
  
    // Fiducials on first monitor. --------------------------------------------
  
  this->RemoveValidationNeedleAxis();
  
  if ( this->ClickNumber > 0  ||  mrmlNode->GetValidated() )
    {
    double point[ 3 ];
    mrmlNode->GetValidationEntryPoint( point );
    vtkMRMLFiducialListNode* planNode = mrmlNode->GetPlanFiducialsNode();
      planNode->SetNthFiducialXYZ( 0, point[ 0 ], point[ 1 ], point[ 2 ] );
      planNode->SetNthFiducialVisibility( 0, 1 );
    }
  
  if ( this->ClickNumber > 1  ||  mrmlNode->GetValidated() )
    {
    double point[ 3 ];
    mrmlNode->GetValidationTargetPoint( point );
    vtkMRMLFiducialListNode* planNode = mrmlNode->GetPlanFiducialsNode();
      planNode->SetNthFiducialXYZ( 1, point[ 0 ], point[ 1 ], point[ 2 ] );
      planNode->SetNthFiducialVisibility( 1, 1 );
    this->OverlayValidationNeedleAxis();
    }
}



void
vtkPerkStationValidateStep
::HideOverlays()
{
  this->RemoveValidationNeedleAxis();
  this->OverlayPlan( false );
}



void
vtkPerkStationValidateStep
::WizardGUICallback(vtkObject *caller, unsigned long event, void *clientData, void *callData )
{
    vtkPerkStationValidateStep *self = reinterpret_cast<vtkPerkStationValidateStep *>(clientData);
    if (self) { self->ProcessGUIEvents(caller, event, callData); }
}



void
vtkPerkStationValidateStep
::ProcessGUIEvents( vtkObject *caller, unsigned long event, void *callData )
{
  vtkMRMLPerkStationModuleNode *mrmlNode = this->GetGUI()->GetPerkStationModuleNode();
  
  if(    ! mrmlNode
      || ! mrmlNode->GetPlanningVolumeNode()
      || strcmp( mrmlNode->GetVolumeInUse(), "Validation" ) != 0 )
    {
    return;
    }

  
  if ( this->ProcessingCallback ) return;
  this->ProcessingCallback = true;
  
  
    // reset plan button
  
  if (    this->ResetValidationButton
       && this->ResetValidationButton == vtkKWPushButton::SafeDownCast( caller )
       && ( event == vtkKWPushButton::InvokedEvent ) )
    {
    this->Reset();
    }
  
  
    // Export results.
  
  if (    this->ValidationExportButton
       && this->ValidationExportButton->GetLoadSaveDialog() == vtkKWLoadSaveDialog::SafeDownCast( caller ) )
    {
    if ( this->ValidationExportButton->GetLoadSaveDialog()->GetStatus() == vtkKWDialog::StatusOK )
      {
      const char* fileName = this->ValidationExportButton->GetLoadSaveDialog()->GetFileName();
  
      if ( fileName )
        {
        this->ExportResultsToFile( fileName );
        }
      }
    }
  
  this->UpdateGUI();
  
  this->ProcessingCallback = false;
}



void
vtkPerkStationValidateStep
::RasToWorld( const double ras[ 3 ], double wc[ 3 ] )
{
  vtkSlicerSliceGUI* sliceGUI =
    vtkSlicerApplicationGUI::SafeDownCast( this->GetGUI()->GetApplicationGUI() )->GetMainSliceGUI( "Red" );
  vtkRenderer* renderer = sliceGUI->GetSliceViewer()->GetRenderWidget()->GetOverlayRenderer();
  
  vtkMatrix4x4 *xyToRAS = sliceGUI->GetLogic()->GetSliceNode()->GetXYToRAS();
  vtkSmartPointer< vtkMatrix4x4 > rasToXY = vtkSmartPointer< vtkMatrix4x4 >::New();
  vtkMatrix4x4::Invert( xyToRAS, rasToXY );
  
  double inPt[ 4 ] = { ras[ 0 ], ras[ 1 ], ras[ 2 ], 1 };
  double outPt[ 4 ];  
  rasToXY->MultiplyPoint( inPt, outPt );
  
  double wc4[ 4 ];
  renderer->SetDisplayPoint( outPt[ 0 ], outPt[ 1 ], 0 );
  renderer->DisplayToWorld();
  renderer->GetWorldPoint( wc4 );
  
  for ( int i = 0; i < 3; ++ i ) wc[ i ] = wc4[ i ];
}



void
vtkPerkStationValidateStep
::ExportResultsToFile( const char* fileName )
{
  vtkMRMLPerkStationModuleNode* mrmlNode = this->GetGUI()->GetPerkStationModuleNode();
  if ( ! mrmlNode ) return;
  
  ofstream output( fileName );
  if ( ! output.is_open() )
    {
    vtkErrorMacro( "Save file couldn't be opened." );
    return;
    }
  
  
    // First row contains column names.
  
  for ( int i = 0; i < VALIDATION_COL_COUNT; ++ i )
    {
    output << VALIDATION_COL_LABELS[ i ] << ", ";
    }
  output << std::endl;
  
  
    // Rows containing results.
  
  const int VPRECISION = 2;
  
  int numValidations = mrmlNode->GetNumberOfValidations();
  int numPlans = mrmlNode->GetNumberOfPlans();
  
  int validationRow = 0;
  for ( int row = 0; row < numPlans; ++ row )
    {
    vtkPerkStationPlan* plan = mrmlNode->GetPlanAtIndex( row );
    
    if ( plan->GetValidated() == false ) continue;
    
    output << plan->GetName().c_str() << ", ";
    output << DoubleToString( mrmlNode->GetEntryPointError( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetEntryPointErrorR( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetEntryPointErrorA( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetEntryPointErrorS( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetTargetPointError( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetTargetPointErrorR( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetTargetPointErrorA( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetTargetPointErrorS( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetPlanAngleAxial( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetPlanAngleSagittal( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetAngleError( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetAngleErrorAxial( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetAngleErrorSagittal( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetPlanDepth( row ), VPRECISION ).c_str() << ", ";
    output << DoubleToString( mrmlNode->GetDepthError( row ), VPRECISION ).c_str() << ", ";
    output << std::endl;
    
    validationRow ++;
    }
  
  output.close();
}
