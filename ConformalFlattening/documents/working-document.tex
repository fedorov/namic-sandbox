\documentclass[letterpaper]{article}
\usepackage{graphics}
\usepackage{amsmath}
\usepackage{amssymb}

\begin{document}

  \title{Conformal Flattening Working Document}
  \date{\today}

  \maketitle

  \section{The processes of the paper}

  \subsection{The central goal of the paper}
  The original surface $\Sigma$, a 3-d point set, is given by $P_i(x,
  y, z)$.

  The goal is to map those points onto a plane, and then onto a
  sphere.  The mapping should have some properties, like angle
  preserving in this case.

  So:

  \begin{displaymath}
    \Sigma \qquad \rightarrow \qquad plane \rightarrow  \qquad sphere
  \end{displaymath}

  The first arrow above is achieved by the conformal mapping while the
  second is by the stereographic projection.

  It is shown in the appendix that the conformal mapping is the
  function $z$ defined by equation:

  \begin{eqnarray}
    \triangle z = (\frac{\partial}{\partial u} -
    i\frac{\partial}{\partial v})\delta_p \label{pde}
  \end{eqnarray}
  where $p$ is an (arbitrary) point on $\Sigma$. Through this mapping
  $z$, we map $\Sigma \backslash \{ p \}$ to complex plane \emph{C}.


  \subsection{How to get the mapping function $z$}
  So the problem turns out to be: how to solve the mapping function
  $z$ from the equation \ref{pde}.

  $z$ solve equation \ref{pde} iff $\forall$ smooth function f:

  \begin{eqnarray}
    \int\int_{\Sigma}\nabla z \cdot \nabla f \textrm{d}S =
    (\frac{\partial f}{\partial u} - i\frac{\partial f}{\partial
      v})|_p \label{inteqn}
  \end{eqnarray}  

  Next, we have to calculate both sides of equation \ref{inteqn} on
  the discrete mesh.

  \begin{enumerate}
  \item \emph{The right side} The right side is basically ZERO
    except on those THREE points in which the point $p$ (of the Dirac
    function) lies. For those three points, A, B, C, the right side of
    equation \ref{inteqn} is:
    \begin{eqnarray}
      \frac{f_A - f_B}{|| B - A ||} + i\frac{f_c-(f_A + \theta(f_B -
        f_A))}{||C - E||} \label{Rinteqn}
    \end{eqnarray}  
    where the point E is the point D in the derivation at the right
    bottom of page 701 of the paper. In the later part of the paper this
    point is refered to as E so I use E here.

    With this, the right side of equation \ref{inteqn} is solved.

  \item \emph{The left side} We use a piecewise linear function
    defined on $\Sigma$ to approximate the function f. Because the
    $\Sigma$ is a discrete mesh, let N be the total number of points
    in the mesh. Hence, the piecewise linear function space on the
    mesh is only N-dimensional. Define the basis to be, $\Phi_i$, as in the
    paper. Then we can write function z as $z = \sum_{k=1}^{N}z_k
    \Phi_k$

    So equation \ref{inteqn} becomes:
    \begin{eqnarray}
      \sum_{P}z_P \int\int (\nabla \Phi(P) \cdot \nabla \Phi(Q)) =
      \frac{\partial \Phi(Q)}{\partial u}(P) - i\frac{\partial
        \Phi(Q)}{\partial v} (P) \label{sumfunc}   
    \end{eqnarray}  
    
    $\int\int (\nabla \Phi(P) \cdot \nabla \Phi(Q))$ can be written in
    the matrix form as $\{ \mathbf{D} \}_{P,Q }$ and can be computed by:
    \begin{eqnarray}
      \{ \mathbf{D} \}_{P,Q } = -\frac{1}{2}(ctg\angle R + ctg\angle S)
      \label{ctg}
    \end{eqnarray}
    defined in the paper. Also, the matrix D has some good properties as:
    \begin{eqnarray}
      \mathbf{D}_{PP} = -\sum_{P \ne Q}D_PQ \label{prop}
    \end{eqnarray}


    Write each $z_p$ as $z_p=x_p + iy_p$, then
    equation \ref{sumfunc} can be written as:

    \begin{eqnarray}
      \sum_{P}x_p D_{PQ} &=& \frac{\partial \Phi(Q)}{\partial u}(P) \nonumber\\
      \sum_{Q}y_p D_{PQ} &=& -\frac{\partial \Phi(Q)}{\partial v} (P)
    \end{eqnarray}  

    denote:
    \begin{eqnarray}
      \vec{a} = (a_Q) &=& \frac{\partial \Phi(Q)}{\partial u}(P) \nonumber \\
      \vec{b} = (b_Q) &=& -\frac{\partial \Phi(Q)}{\partial v} (P)
    \end{eqnarray}  

    so we have:

    \begin{eqnarray}
      \mathbf{D}\vec{x} &=& \vec{a} \nonumber \\
      \mathbf{D}\vec{y} &=& \vec{b} \label{z}
    \end{eqnarray}  

    $\forall \quad point \quad Q$:
    \begin{eqnarray}
      a_Q - ib_Q := \left\{
      \begin{array}{lcl}
        0 & for & Q \notin {A, B, C} \\
        \frac{-1}{||B-A||} + i\frac{1-\theta}{||C-E||} & for & Q = A \\
        \frac{1}{||B-A||} + i\frac{\theta}{||C-E||} & for & Q = B \\
        i\frac{-1}{||C-E||} & for & Q = C
      \end{array} \right. \label{ab}
    \end{eqnarray}  

  \end{enumerate}
  
  \section{The implementation}
  \begin{enumerate}
    \item Calculate matrix D by equation \ref{ctg} and equation \ref{prop}.
    \item Calculate $a_Q$ and $b_Q$ by equation \ref{ab}.
    \item Compute $\vec{x}$ and $\vec{y}$ from equation \ref{z} and
    thus we get $z_P$ and thus $z$, the conformal mapping.
    \item Map the original surface to the plane by $z$.
    \item Map the plane to the sphere by the stereographic projection:
    \begin{eqnarray}
      (x, y) &\Rightarrow& (\frac{2x}{1+r^2}, \frac{2y}{1+r^2},
      \frac{2r^2}{1+r^2}-1) \nonumber \\
      r^2 &=& x^2 + y^2
    \end{eqnarray}  

    \item DONE!
  \end{enumerate}
\end{document}
